/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

var dateTimeFormatter = (function () {

    var module = {};

    module.formatTime = function (datetime) {
        return new Date(datetime).toLocaleTimeString();
    };

    module.formatDate = function (datetime) {
        return new Date(datetime).toLocaleDateString();
    };

    module.formatDateTime = function (datetime) {
        var dateTime = new Date(datetime);
        return dateTime.toLocaleDateString() + ' ' + dateTime.toLocaleTimeString();
    };

    return module;
})();

var dropdowns = (function () {

    // Update rate in milliseconds
    var updateRate = 10000;
    var module = {};

    // Register helper functions for the handlebars templating engine
    Handlebars.registerHelper('getNotificationSymbol', function (type) {
        return getNotificationSymbol(type);
    });
    Handlebars.registerHelper('formatDateTime', function (datetime) {
        return dateTimeFormatter.formatDateTime(datetime);
    });
    Handlebars.registerHelper('calculateJobStatusPercentage', function (status) {
        return calculateJobStatusPercentage(status);
    });
    Handlebars.registerHelper('getJobStatusClass', function (status) {
        return getJobStatusClass(status);
    });


    var statusTemplate;
    var notificationTemplate;

    function updateNotificationDropdown(notificationObjectArray) {
        $("#notification-dropdown-container").html(notificationTemplate({"objectArray": notificationObjectArray}));
    }

    function updateJobStatusDropdown(jobObjectArray) {
        $.each(jobObjectArray, function (index, jobObject) {
            if (jobObject.peregrineEnabled == true && jobHasCompleted(jobObject) == false ) {
                $.ajax({
                    url: '/data/job-status?id=' + jobObject.id,
                    type: 'get',
                    data: {
                        _csrf: token
                    },
                    dataType: 'json',
                    error: function () {
                        console.log("error");
                    },
                    success: function (status) {
                        // This might not work but if it doesnt it gets it on the second go.
                        jobObject.status = status;
                    }
                });
            }
        });

        $("#job-status-dropdown-container").html(statusTemplate({"objectArray" :jobObjectArray}));
    }

    module.compileTemplates = function () {
        statusTemplate = Handlebars.compile($("#job-status-dropdown-template").html());
        notificationTemplate = Handlebars.compile($("#notification-dropdown-template").html());
    };

    module.updateDropdowns = function () {

        // Check if the update rate is set somewhere, else sets to the default 10 seconds
        var newUpdateRate = $("#update-time-select").val();
        if (newUpdateRate != "" && newUpdateRate != null && newUpdateRate != 0) {
            updateRate = newUpdateRate;
        }

        getNotificationsLimit(updateNotificationDropdown, start, token, header, 5);
        basicGetLimit("/data/jobs", updateJobStatusDropdown, 5, token, header);
        console.log("polling@" + updateRate + "ms");

        setTimeout(module.updateDropdowns,
            updateRate);
    };

    return module;
})();

function jobHasCompleted(status) {
    return status == 'DONE' || status == 'COMPLETED_WITH_ERROR' || status == 'FATAL_ERROR'
}

function getNotificationsLimit(inputFunction, start, token, header, limit) {

    $.ajax({
        url: "/data/get-notifications",
        type: 'post',
        data: {
            _csrf: token,
            startDate: start,
            stopDate: new Date(),
            limit: limit
        },
        headers: {
            _csrf_header: header
        },
        dataType: 'json',
        success: function (data) {
            inputFunction(data);
        }
    });
}

function getNotifications(inputFunction, start, token, header) {
    $.ajax({
        url: "/data/get-notifications",
        type: 'post',
        data: {
            _csrf: token,
            startDate: start,
            stopDate: new Date()
        },
        headers: {
            _csrf_header: header
        },
        dataType: 'json',
        success: function (data) {
            inputFunction(data);
        }
    });

}

function basicGet(url, callback, token, header) {
    $.ajax({
        url: url,
        type: 'get',
        data: {
            _csrf: token,
        },
        headers: {
            _csrf_header: header
        },
        dataType: 'json',
        success: function (data) {
            callback(data);
        }
    });
}

function basicGetLimit(url, callback, limit, token, header) {
    $.ajax({
        url: url,
        type: 'get',
        data: {
            _csrf: token,
            limit: limit
        },
        headers: {
            _csrf_header: header
        },
        dataType: 'json',
        success: function (data) {
            callback(data);
        }
    });
}

function getNotificationSymbol(type) {

    if (type == "INFO") {
        return "fa fa-info-circle"
    } else if (type == "WARNING") {
        return "fa fa-exclamation-circle"
    } else if (type == "ERROR") {
        return "fa fa-exclamation-triangle"
    }

}

function calculateJobStatusPercentage(status) {

    if (status == 'PROCESSING') {
        return 20;
    } else if (status == 'UNKNOWN') {
        return 20;
    } else if (status == 'RUNNING') {
        return 50;
    } else if (status == 'DONE') {
        return 100;
    } else if (status == 'FATAL_ERROR') {
        return 100;
    } else if (status == 'COMPLETED_WITH_ERROR') {
        return 100;
    } else {
        return 0;
    }

}

function getJobStatusClass(status) {

    if (status == 'PROCESSING') {
        return "progress-bar-success active";
    } else if (status == 'UNKNOWN') {
        return "progress-bar-warning active";
    } else if (status == 'RUNNING') {
        return "progress-bar-success active";
    } else if (status == 'DONE') {
        return "progress-bar-success";
    } else if (status == 'FATAL_ERROR') {
        return "progress-bar-danger";
    } else if (status == 'COMPLETED_WITH_ERROR') {
        return "progress-bar-danger";
    } else {
        return "progress-bar-danger";
    }
}

function updateJobStatusBar(barId, status) {
    $(barId).each(function () {
        var barWidth = calculateJobStatusPercentage(status);
        $(this).width(barWidth + '%');
        $(this).text(status);
        $(this).removeClass("progress-bar-success progress-bar-warning progress-bar-danger");
        $(this).addClass(getJobStatusClass(status));
    });
}

function doFileDownload(jobId, paramId) {

    $.ajax({
        url: "/download?job-id=" + jobId + "&param-id=" + paramId,
        type: 'get',
        success: function (data) {
            window.location = "/download?job-id=" + jobId + "&param-id=" + paramId;
        },
        error: function (jqXHR, textStatus, errorThrown) {

            if (jqXHR.status == 401) {
                $("#peregrine-modal").modal();
            }
            if (jqXHR.status == 404) {
                $("#download-failed-alert").show();
            }
            if (jqXHR.status == 500) {
                console.log(jqXHR.responseText);
            }
        }
    })
}

var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");
var start = "2015-11-27 03:14:07";

$(function () {
    dropdowns.compileTemplates();
    dropdowns.updateDropdowns();
});



