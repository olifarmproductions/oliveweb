/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobDao;
import com.olifarm.olive.job.JobStatus;
import com.olifarm.olive.web.mail.MailService;
import com.olifarm.olive.web.notifications.notification.NotificationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Service used to configure queue listeners for the rabbit server.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Component
public class RabbitController {

    /**
     * Logger object used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * JobDao used for updating the database.
     */
    @Autowired
    private JobDao jobDao;

    /**
     * NotificationService used for generating notifications.
     */
    @Autowired
    private NotificationService notificationService;

    /**
     * MailService used for sending emails.
     */
    @Autowired
    private MailService mailService;

    /**
     * Queue listener for the jobResponses queue. This is used to indicate when a job has been received by a node.
     *
     * @param job the job (the job comes from the AMPQ queue)
     */
    @RabbitListener(queues = "jobResponses")
    public void receiveResponse(Job job) {

        logger.info("Job received by client: id: " + job.getId() + ", name: " + job.getName());
        job.setStatus(JobStatus.RUNNING);
        jobDao.updateStatus(job);

        notificationService.setNotification(NotificationService.createJobEventNotification(job, "received by client"));
    }

    /**
     * Queue listener for the jobFinished queue. This is used to indicate when a job has been completed by a node.
     *
     * @param job the job (the job comes from the AMQP queue)
     */
    @RabbitListener(queues = "jobFinished")
    public void receiveFinished(Job job) {
        if (job.getStatus() != JobStatus.COMPLETED_WITH_ERROR && job.getStatus() != JobStatus.FATAL_ERROR) {
            logger.info("Job completed: id: " + job.getId() + ", name: " + job.getName());
            job.setStatus(JobStatus.DONE);
            job.setStopTime(new Date());

            jobDao.updateStatus(job);
            jobDao.updateStopTime(job);

            notificationService.setNotification(NotificationService.createJobEventNotification(job, "job completed"));

            if (job.isMailNotification()) {
                mailService.jobFinished(job);
            }
        }
    }

    /**
     * Report error.
     *
     * @param job the job
     */
    @RabbitListener(queues = "errorQueue")
    public void reportError(Job job) {

        logger.warn("Job had an error: id: " + job.getId() + ", name: " + job.getName());
        job.setStatus(JobStatus.FATAL_ERROR);
        job.setStopTime(new Date());

        jobDao.updateStatus(job);
        jobDao.updateStopTime(job);

        notificationService.setNotification(NotificationService.createJobErrorNotification(job, "job crashed"));

        if (job.isMailNotification()) {
            mailService.jobFinished(job);
        }

    }


}
