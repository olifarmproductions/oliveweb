/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.notifications.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.List;


/**
 * The type Notification dao jdbc.
 */
@Component
public class NotificationDaoJdbc implements NotificationDao {

    /**
     * {@link JdbcTemplate} autowired by spring.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public List<Notification> getMessages(TimedNotificationRequest nfg) throws ParseException {

        if (nfg.getUserId() != 0) {

            if (nfg.getStartDate() != null && nfg.getStopDate() != null) {

                if (nfg.getLimit() != 0) {
                    return jdbcTemplate.query("SELECT * "
                                    + "FROM notifications "
                                    + "WHERE user_id = ? "
                                    + "AND date_time BETWEEN ? AND ? "
                                    + "ORDER BY date_time DESC "
                                    + "LIMIT ? ",
                            new Object[]{nfg.getUserId(), nfg.getStartDate(), nfg.getStopDate(), nfg.getLimit()},
                            new BeanPropertyRowMapper().newInstance(Notification.class));

                } else {
                    return jdbcTemplate.query("SELECT *  "
                                    + "FROM notifications "
                                    + "WHERE user_id = ? "
                                    + "AND date_time BETWEEN ? AND ? "
                                    + "ORDER BY date_time DESC",
                            new Object[]{nfg.getUserId(), nfg.getStartDate(), nfg.getStopDate()},
                            new BeanPropertyRowMapper().newInstance(Notification.class));
                }
            } else {

                if (nfg.getLimit() != 0) {
                    return jdbcTemplate.query("SELECT  * "
                                    + "FROM notifications WHERE user_id = ? "
                                    + "ORDER BY date_time DESC "
                                    + "LIMIT ?",
                            new Object[]{nfg.getUserId(), nfg.getLimit()},
                            new BeanPropertyRowMapper().newInstance(Notification.class));

                } else {
                    return jdbcTemplate.query("SELECT  * "
                                    + "FROM notifications WHERE user_id = ? "
                                    + "ORDER BY date_time DESC",
                            new Object[]{nfg.getUserId()},
                            new BeanPropertyRowMapper().newInstance(Notification.class));
                }
            }

        } else {
            throw new IllegalArgumentException("NotifcicationRequest invalid");
        }

    }

    @Override
    public void setNotification(Notification notification) {

        jdbcTemplate.update("INSERT INTO notifications (user_id, type, title , date_time, notification) "
                        + "VALUES (?,?,?,?,?)",
                new Object[]{notification.getUserId(),
                        notification.getType().toString(),
                        notification.getTitle(),
                        notification.getDateTime(),
                        notification.getNotification()});
    }

}
