package com.olifarm.olive.web.job;

/**
 * The enum Run type.
 */
public enum RunType {

    /**
     * Basic run type.
     */
    BASIC,
    /**
     * Peregrine run type.
     */
    PEREGRINE
}
