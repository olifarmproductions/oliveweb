/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

package com.olifarm.olive.web.job.project;

import com.olifarm.olive.job.*;
import com.olifarm.olive.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * The type Job group service.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Component
public class JobGroupService {

    @Autowired
    private JobDao jobDao;

    @Autowired
    private JobGroupDao jobGroupDao;

    /**
     * Gets all job groups for user.
     *
     * @param user the user
     * @return the all job groups for user
     */
    public List<JobGroup> getAllJobGroupsForUser(User user) {
        List<JobGroup> jobGroups = jobGroupDao.getJobGroups(user);

        addJobsToGroup(user, jobGroups);

        return jobGroups;
    }

    /**
     * Gets all job groups for user as list.
     *
     * @param user the user
     * @return the all job groups for user as list
     */
    public List<JobGroup> getAllJobGroupsForUserAsList(User user) {
        List<JobGroup> jobGroups = jobGroupDao.getJobGroupsAsList(user);

        return jobGroups;
    }

    /**
     * Gets job groups for project.
     *
     * @param user    the user
     * @param project the project
     * @return the job groups for project
     */
    public List<JobGroup> getJobGroupsForProject(User user, Project project) {
        List<JobGroup> jobGroups = jobGroupDao.getJobGroups(project);
        addJobsToGroup(user, jobGroups);

        return jobGroups;
    }

    /**
     * Gets job group.
     *
     * @param user the user
     * @param id   the id
     * @return the job group
     */
    public JobGroup getJobGroup(User user, int id) {
        return jobGroupDao.getJobGroup(user, id);
    }

    /**
     * Add job group.
     *
     * @param user     the user
     * @param jobGroup the job group
     */
    public void addJobGroup(User user, JobGroup jobGroup) {
        jobGroup.setUserId(user.getId());
        jobGroupDao.addJobGroup(jobGroup);
    }

    /**
     * Update job group.
     *
     * @param user     the user
     * @param jobGroup the job group
     */
    public void updateJobGroup(User user, JobGroup jobGroup) {
        jobGroup.setUserId(user.getId());
        jobGroupDao.updateJobGroup(jobGroup);
    }

    /**
     * Remove job group.
     *
     * @param user     the user
     * @param jobGroup the job group
     */
    public void removeJobGroup(User user, JobGroup jobGroup) {
        jobGroup.setUserId(user.getId());
        jobGroupDao.removeJobGroup(jobGroup);
    }

    /**
     * Sets job group on job.
     *
     * @param user     the user
     * @param jobGroup the job group
     * @param job      the job
     */
    public void setJobGroupOnJob(User user, JobGroup jobGroup, Job job) {
        job.setJobGroupId(jobGroup.getId());
        jobDao.setJobGroup(job);
    }


    private void addJobsToGroup(User user, List<JobGroup> jobGroups) {
        for (JobGroup jobGroup: jobGroups) {
            this.addJobsToGroup(user, jobGroup);
        }
    }

    private void addJobsToGroup(User user, JobGroup jobGroup) {
        jobGroup.setJobs(jobDao.getJobsByGroupId(user, jobGroup.getId()));
    }



}
