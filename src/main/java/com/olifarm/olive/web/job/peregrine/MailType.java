/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.peregrine;

/**
 * Enum with different mail notification types available for a Peregrine job.
 *
 * @author Harm Brugge
 */
public enum MailType {

    /**
     * Equivalent to: BEGIN, END, FAIL and REQUEUE
     */
    ALL,

    /**
     * Job started running.
     */
    BEGIN,

    /**
     * Job completed.
     */
    END,

    /**
     * Job failed.
     */
    FAIL,

    /**
     * Job is requeued.
     */
    REQUEUE,

    /**
     * Job exceeded time limit.
     */
    TIME_LIMIT,

    /**
     * Job reached 50/80/90 percent of time limit
     */
    TIME_LIMIT_50,
    /**
     * Time limit 80 mail type.
     */
    TIME_LIMIT_80,
    /**
     * Time limit 90 mail type.
     */
    TIME_LIMIT_90,

}
