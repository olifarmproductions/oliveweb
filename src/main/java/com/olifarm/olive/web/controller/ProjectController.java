/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobGroup;
import com.olifarm.olive.job.Project;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.Utils;
import com.olifarm.olive.web.job.JobService;
import com.olifarm.olive.web.job.application.CliApplicationService;
import com.olifarm.olive.web.job.project.JobGroupService;
import com.olifarm.olive.web.job.project.ProjectService;
import com.olifarm.olive.web.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * The type Project controller.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Controller
public class ProjectController {

    /**
     * Logger class used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private JobGroupService jobGroupService;

    @Autowired
    private JobService jobService;

    @Autowired
    private CliApplicationService applicationService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;


    /**
     * Resolves the view jobs view.
     *
     * @param principal the principal
     * @param model     the model
     * @return the view
     */
    @RequestMapping("/view-jobs")
    @PreAuthorize("authenticated")
    public String showJobs(Principal principal, Model model) {

        User user = userService.getUser(principal.getName());
        model.addAttribute("apps", applicationService.getApps());

        return "authenticated/project/view_jobs";
    }

    /**
     * Show job group string.
     *
     * @param id        the id
     * @param principal the principal
     * @param model     the model
     * @return the string
     */
    @RequestMapping("/view-job-group")
    @PreAuthorize("authenticated")
    public String showJobGroup(@RequestParam("group-id") int id,  Principal principal, Model model) {

        User user = userService.getUser(principal.getName());
        JobGroup jobGroup = jobGroupService.getJobGroup(user, id);
        List<Job> jobs = jobService.getJobsWithApps(user, jobGroup);
        model.addAttribute("apps", applicationService.getApps());
        model.addAttribute("jobGroup", jobGroup);
        model.addAttribute("jobs", jobs);
        model.addAttribute("jobBatchInfo", Utils.getJobInfo(jobs));

        return "authenticated/project/view_group";
    }

    /**
     * View projects
     *
     * @param model the model
     * @return the view
     */
    @RequestMapping("/view-projects")
    @PreAuthorize("authenticated")
    public String testTree(Model model)  {
        model.addAttribute("apps", applicationService.getApps());

        return "authenticated/project/view_projects";
    }


    /**
     * Gets projects.
     *
     * @param principal the principal
     * @return the projects
     */
    @RequestMapping("/data/projects")
    @PreAuthorize("authenticated")
    @ResponseBody
    public List<Project> getProjects(Principal principal) {
        User user = userService.getUser(principal.getName());
        return projectService.getAllProjectsForUser(user);
    }

    /**
     * Add project boolean.
     *
     * @param project       the project
     * @param bindingResult the binding result
     * @param principal     the principal
     * @return the boolean
     */
    @RequestMapping(value = "/data/add-project", method = RequestMethod.POST)
    @PreAuthorize("authenticated")
    @ResponseBody
    public boolean addProject(@Valid Project project, BindingResult bindingResult, Principal principal) {
        if (bindingResult.hasErrors()) {
            return false;
        }
        User user = userService.getUser(principal.getName());
        projectService.addProject(user, project);
        return true;
    }

    /**
     * Update project boolean.
     *
     * @param project       the project
     * @param bindingResult the binding result
     * @param principal     the principal
     * @return the boolean
     */
    @RequestMapping(value = "/data/update-project", method = RequestMethod.POST)
    @PreAuthorize("authenticated")
    @ResponseBody
    public boolean updateProject(@Valid Project project, BindingResult bindingResult, Principal principal) {
        if (bindingResult.hasErrors()) {
            return false;
        }
        User user = userService.getUser(principal.getName());
        projectService.updateProject(user, project);
        return true;
    }

    /**
     * Remove project boolean.
     *
     * @param project       the project
     * @param bindingResult the binding result
     * @param principal     the principal
     * @return the boolean
     */
    @RequestMapping(value = "/data/remove-project", method = RequestMethod.POST)
    @PreAuthorize("authenticated")
    @ResponseBody
    public boolean removeProject(@Valid Project project, BindingResult bindingResult, Principal principal) {
        if (bindingResult.hasErrors()) {
            return false;
        }
        User user = userService.getUser(principal.getName());
        projectService.removeProject(user, project);
        return true;
    }





}
