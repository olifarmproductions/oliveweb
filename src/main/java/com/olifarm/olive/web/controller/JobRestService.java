/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.jcraft.jsch.JSchException;
import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobStatus;
import com.olifarm.olive.job.JobStatusPeregrine;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.Utils;
import com.olifarm.olive.web.io.PeregrineSession;
import com.olifarm.olive.web.io.PeregrineSessionAbsentException;
import com.olifarm.olive.web.io.SshService;
import com.olifarm.olive.web.job.JobService;
import com.olifarm.olive.web.job.application.CliApplicationService;
import com.olifarm.olive.web.user.UserService;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Properties;

/**
 * Author(s):     Olivier Bakker
 * Student id(s): 330595
 * Email(s):      o.b.bakker@st.hanze.nl
 * Institute:     Hanze University of Applied Sciences, bioinformatics department
 * Project:       olive
 * Package:       com.olifarm.notifications
 * Created on:    23-12-2015
 * All rights reserved.
 */
@RestController
public class JobRestService {

    @Autowired
    private JobService jobService;

    @Autowired
    private UserService userService;

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Autowired
    private SshService sshService;

    /**
     * Gets jobs limit.
     *
     * @param limit     the limit
     * @param principal the principal
     * @return jobs limit
     * @throws IOException the io exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/data/jobs", method = RequestMethod.GET)
    public Object getJobsLimit(@RequestParam(value = "limit", defaultValue = "-1") int limit, Principal principal)
            throws IOException {

        User user = userService.getUser(principal.getName());
        List<Job> jobs;

        if (limit == -1) {
            jobs = jobService.getJobsWithApps(user);
        } else {
            jobs = jobService.getJobs(user, limit);
        }

        return jobs;
    }

    /**
     * Gets queue count.
     *
     * @return the queue count
     * @throws IOException the io exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/data/queue-size", method = RequestMethod.GET)
    public Object getQueueCount() throws IOException {

        Properties properties = amqpAdmin.getQueueProperties("jobRequests");

        return properties.get("QUEUE_MESSAGE_COUNT").toString();
    }

    /**
     * Gets job rest.
     *
     * @param id        the id
     * @param principal the principal
     * @return job rest
     * @throws IOException the io exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/data/job", method = RequestMethod.GET)
    public Job getJobRest(@RequestParam(value = "id") int id, Principal principal) throws IOException {
        User user = userService.getUser(principal.getName());

        return jobService.getJob(id, user);
    }

    /**
     * Gets status.
     *
     * @param id        the id
     * @param principal the principal
     * @param session   the session
     * @param response  the response
     * @return status status
     * @throws JSchException the j sch exception
     * @throws IOException   the io exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/data/job-status", method = RequestMethod.GET)
    public JobStatus getStatus(@RequestParam(value = "id") int id,
                               Principal principal,
                               HttpSession session,
                               HttpServletResponse response) throws JSchException, IOException {

        User user = userService.getUser(principal.getName());
        Job job = jobService.getJob(id, user);
        PeregrineSession peregrineSession = (PeregrineSession) session.getAttribute("peregrineSession");

        if (!Utils.jobHasCompleted(job) && job.isPeregrineEnabled()) {
            try {
                JobStatusPeregrine status = sshService.getStatus(SshService.makeSshSession(peregrineSession), job.getPeregrineJobId());
                job.setPeregrineJobStatus(status);
                job.setStatus(JobStatus.convertPeregrineStatus(status.toString()));
                jobService.updateJobStatus(job);

            } catch (PeregrineSessionAbsentException | IllegalStateException e) {
                response.sendError(HttpStatus.UNAUTHORIZED.value(), "please log in to peregrine");
                return job.getStatus();
            }
        }
        return job.getStatus();
    }
}
