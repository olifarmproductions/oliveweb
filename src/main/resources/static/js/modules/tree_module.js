/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

/**
 * Created by Olivier on 27-5-2016.
 */
var treeModule = (function () {

    var module = {};

    var margin = {top: 20, right: 120, bottom: 20, left: 120};
    var width;
    var height;
    calculateSize();

    var i = 0;
    var originalData;
    var rootNode;
    var rootNodeId;
    var activeNode;
    var animationDuration = 1000;
    var numberOfNodes = 1;

    var tree = d3.layout.tree().size([height, width]);

    // Determines the distance between nodes
    tree.separation(function separation(a, b) {
        return a.parent == b.parent ? 1 : 2;
    });

    // Determines the curve of the lines
    var diagonal = d3.svg.diagonal().projection(function (d) {
        return [d.y, d.x];
    });

    var svg = d3.select("#tree-container")
        .append("svg")
        .attr("width", width + margin.right + margin.left)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    d3.select("svg").attr(":xmlns", "http://www.w3.org/2000/svg");
    d3.select("svg").attr(":xmlns:xlink", "http://www.w3.org/1999/xlink");
    window.addEventListener("resize", resizeTree);

    basicGet("/data/projects", projectCallback, token, header);

    function resizeTree(event) {

        calculateSize();
        tree.size([height, width]);
        updateTree(rootNode);

    }

    function getProjectFromOriginalData(projectId) {

        for (var i = 0; i < originalData.children.length; i++) {
            if (originalData.children[i].projectId == projectId) {
                return originalData.children[i];
            }
        }
    }

    function convertJobGroupToNode(jobGroup) {
        var treeNode = {};
        treeNode.id = numberOfNodes;
        treeNode.name = jobGroup.name;
        treeNode.jobGroupId = jobGroup.id;
        treeNode.children = [];
        numberOfNodes++;

        if (jobGroup.children) {
            var arrayLen = jobGroup.children.length;
            for (var i = 0; i < arrayLen; i++) {
                treeNode.children[i] = convertJobGroupToNode(jobGroup.children[i]);
            }
        }

        return treeNode;

    }

    function convertProjectToNode(project) {
        var treeNode = {};
        treeNode.id = numberOfNodes;
        treeNode.name = project.name;
        treeNode.projectId = project.id;
        treeNode.children = [];
        numberOfNodes++;

        var arrayLen = project.jobGroups.length;

        for (var i = 0; i < arrayLen; i++) {
            treeNode.children[i] = convertJobGroupToNode(project.jobGroups[i]);
        }

        return treeNode;
    }

    function convertProjectArrayToRoot(projects) {
        var rootNode = {};
        rootNode.name = "Projects";
        rootNode.children = [];

        var arrayLen = projects.length;

        for (var i = 0; i < arrayLen; i++) {
            rootNode.children[i] = convertProjectToNode(projects[i]);
        }
        return rootNode;
    }

    function setupData(projects) {
        rootNode = projects;

        rootNode.x0 = height / 2;
        rootNode.y0 = 0;

        updateTree(rootNode);
    }

    function updateTree(source) {

        // Compute the new tree layout.
        var nodes = tree.nodes(rootNode).reverse();
        var links = tree.links(nodes);

        // Update the nodes…
        var node = svg.selectAll("g.node")
            .data(nodes, function (node) {
                return node.id || (node.id = ++i);
            });

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter()
            .append("g")
            .attr("class", "node")
            .attr("transform", function (node) {
                return "translate(" + source.y0 + "," + source.x0 + ")";
            });

        nodeEnter.append("circle")
            .attr("r", 8)
            .style("fill", function (node) {
                return node.hiddenChildren ? "darkgrey" : "#fff";
            })
            .on("click", nodeClicked);

        nodeEnter.append("text")
            .attr("x", -45)
            .attr("y", 10)
            .text(function (d) {
                return '\uf055';
            })
            .attr("style", "font: 20px FontAwesome")
            .attr("fill", "green")
            .on("click", showAddGroup);

        nodeEnter.append("text")
            .attr("x", -30)
            .attr("y", 10)
            .text(function (d) {
                return '\uf056';
            })
            .attr("style", "font: 20px FontAwesome")
            .attr("fill", "red")
            .on("click", showRemoveGroup);

        nodeEnter.append("a")
            .attr("xlink:href", function (node) {
                return "/view-job-group?group-id=" + node.jobGroupId;
            })
            .append("text")
            .attr("x", 15)
            .attr("dy", ".50em")
            .text(function (node) {
                return node.name;
            });

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(animationDuration)
            .attr("transform", function (node) {
                return "translate(" + node.y + "," + node.x + ")";
            });

        nodeUpdate.select("circle")
            .attr("r", 8)
            .style("fill", function (node) {
                return node.hiddenChildren ? "darkgrey" : "#fff";
            });

        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(animationDuration)
            .attr("transform", function (d) {
                return "translate(" + source.y + "," + source.x + ")";
            })
            .remove();

        nodeExit.select("circle")
            .attr("r", 1e-6);

        nodeExit.select("text")
            .style("fill-opacity", 1e-6);

        // Update the links…
        var link = svg.selectAll("path.link")
            .data(links, function (d) {
                return d.target.id;
            });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function (d) {
                var o = {x: source.x0, y: source.y0};
                return diagonal({source: o, target: o});
            });

        // Transition links to their new position.
        link.transition()
            .duration(animationDuration)
            .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(animationDuration)
            .attr("d", function (d) {
                var o = {x: source.x, y: source.y};
                return diagonal({source: o, target: o});
            })
            .remove();

        // Stash the old positions for transition.
        nodes.forEach(function (d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    function nodeClicked(node) {
        if (node.children) {
            node.hiddenChildren = node.children;
            node.children = null;
        } else {
            node.children = node.hiddenChildren;
            node.hiddenChildren = null;
        }
        updateTree(node);
    }

    function collapseNode(node) {
        if (node.children) {
            node.hiddenChildren = node.children;
            node.hiddenChildren.forEach(collapseNode);
            node.children = null;
        }
    }

    function expandNode(node) {
        if (node.children) {
            node.hiddenChildren = node.children;
            node.hiddenChildren.forEach(expandNode);
        }
    }

    function setProjectsTable(data) {
        if (data.length > 0) {
            $("#project-table").DataTable({
                autoWidth: false,
                data: data,
                columns: [{
                    data: "id", render: function (data) {
                        return "<a onclick='treeModule.setProjectAsRoot(" + data + ")'><i class='fa fa-link fa-lg'></i></a>";
                    }
                }, {data: "id"}, {data: "name"}, {data: "description"}]
            });

        }
    }

    function projectCallback(data) {
        originalData = convertProjectArrayToRoot(data);
        rootNodeId = originalData.children[0].projectId;

        setupData(getProjectFromOriginalData(rootNodeId));
        setProjectsTable(data);

    }

    function calculateSize() {
        width = $("#tree-container").width() - margin.right - margin.left;
        height = width * 0.3 - margin.top - margin.bottom;
    }

    function showRemoveGroup(node) {
        activeNode = node;
        $("#remove-group-modal").modal("show");
    }

    function showAddGroup(node) {
        activeNode = node;
        $("#add-group-modal").modal("show");
    }

    function newProjectCallback(data) {

        if ($.fn.DataTable.isDataTable("#project-table")) {
            $("#project-table").DataTable().clear();
            $("#project-table").DataTable().rows.add(data);
            $("#project-table").DataTable().draw();
        } else {
            setProjectsTable(data);
        }

        rootNodeId = data[data.length - 1].id;
        treeModule.setTreeData(data);
        treeModule.setProjectAsRoot(rootNodeId);
    }

    module.doAddProjectPost = function () {

        var formData = {
            name: $("#project-name")[0].value,
            description: $("#project-description")[0].value,
            _csrf: token
        };

        $.ajax({
            url: "/data/add-project",
            type: 'post',
            data: formData,
            headers: {
                _csrf_header: header
            },
            dataType: 'json',
            success: function (data) {
                basicGet("/data/projects", newProjectCallback, token, header);
            }
        });
    };

    module.setProjectAsRoot = function (id) {
        rootNodeId = id;
        setupData(getProjectFromOriginalData(id));
    };

    module.doAddGroupPost = function () {

        var formData = {
            projectId: rootNode.projectId,
            parentId: activeNode.jobGroupId,
            name: $("#group-name")[0].value,
            description: $("#group-description")[0].value,
            _csrf: token
        };

        $.ajax({
            url: "/data/add-job-group",
            type: 'post',
            data: formData,
            headers: {
                _csrf_header: header
            },
            dataType: 'json',
            success: function (data) {
                basicGet("/data/projects", module.setTreeData, token, header);
                $("#add-group-modal").modal("hide");

            }
        });

    };

    module.doRemoveGroupPost = function () {

        var formData = {
            projectId: rootNode.projectId,
            id: activeNode.jobGroupId,
            _csrf: token
        };

        $.ajax({
            url: "/data/remove-job-group",
            type: 'post',
            data: formData,
            headers: {
                _csrf_header: header
            },
            dataType: 'json',
            success: function (data) {
                basicGet("/data/projects", module.setTreeData, token, header);
                $("#remove-group-modal").modal("hide");
            }
        });

    };

    module.setTreeData = function (data) {
        originalData = convertProjectArrayToRoot(data);
        setupData(getProjectFromOriginalData(rootNodeId));
    };

    return module;

})();