/*
 *  OliveWeb is the webinterface for the OliveProject.
 *  Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.web.job;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobStatus;
import com.olifarm.olive.job.JobStatusPeregrine;
import org.springframework.beans.TypeMismatchException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * The type Job row mapper.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public class JobRowMapper implements RowMapper<Job> {


    @Override
    public Job mapRow(ResultSet rs, int rowNum) throws SQLException {

        Job job = new Job();

        job.setId(rs.getInt("id"));

        try {
            job.setPeregrineJobId(rs.getInt("peregrine_job_id"));
        } catch (TypeMismatchException exception) {
            job.setPeregrineJobId(0);
        }
        String peregrineStatus = rs.getString("peregrine_job_status");
        if (peregrineStatus != null) {
            job.setPeregrineJobStatus(JobStatusPeregrine.valueOf(peregrineStatus));
        } else {
            job.setPeregrineJobStatus(JobStatusPeregrine.UNKNOWN);
        }
        job.setApplicationId(rs.getInt("application_id"));
        job.setUserId(rs.getInt("user_id"));
        job.setName(rs.getString("name"));
        job.setUsersJobId(rs.getInt("users_job_id"));
        Timestamp startTime = rs.getTimestamp("start_time");
        if (startTime != null) {
            job.setStartTime(new Date(startTime.getTime()));
        }
        Timestamp stopTime = rs.getTimestamp("stop_time");
        if (stopTime != null) {
            job.setStopTime(new Date(stopTime.getTime()));
        }
        job.setStatus(JobStatus.valueOf(rs.getString("status")));
        job.setPeregrineEnabled(rs.getBoolean("peregrine_enabled"));
        try {
            job.setJobGroupId(rs.getInt("job_group_id"));
        } catch (TypeMismatchException exception) {
            job.setJobGroupId(0);
        }

        return job;
    }
}
