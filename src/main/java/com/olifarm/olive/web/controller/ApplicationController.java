/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.Module;
import com.olifarm.olive.web.job.application.CliApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * Application controller.
 * <p>
 * API's are only accessible by admins
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Controller
public class ApplicationController {

    /**
     * The application service.
     */
    @Autowired
    private CliApplicationService applicationService;

    /**
     * API for creating a new application.
     *
     * @param model the model
     * @return the add app view
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/add-app", method = RequestMethod.GET)
    public String getNewApp(Model model) {

        model.addAttribute("apps", applicationService.getApps());
        model.addAttribute("app", new CliApplication());
        model.addAttribute("modules", applicationService.getPeregrineModules());

        return "authenticated/admin/add_app";
    }

    /**
     * API for resolving the view to edit an application.
     *
     * @param model the model
     * @param id    the id of the application to be edited
     * @return the view with application form
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/change-app", method = RequestMethod.GET)
    public String changeApp(Model model, @RequestParam(value = "id") int id) {

        model.addAttribute("apps", applicationService.getApps());
        model.addAttribute("app", applicationService.getApp(id));
        model.addAttribute("modules", applicationService.getPeregrineModules());

        return "authenticated/admin/add_app";
    }

    /**
     * API for generating the view for the list of applications that can be edited.
     *
     * @param model the model
     * @return the edit app view
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/edit-app", method = RequestMethod.GET)
    public String getEditApp(Model model) {

        model.addAttribute("apps", applicationService.getApps());

        return "authenticated/admin/edit_app";
    }

    /**
     * API for posting a new application.
     * Will validate the App object
     *
     * @param app    the application
     * @param result results of the validation
     * @param model  the model
     * @return a redirect to main view
     * @throws IOException the io exception
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/set-app", method = RequestMethod.POST)
    public String setNewApp(@Valid CliApplication app, BindingResult result, Model model) throws IOException {

        model.addAttribute("apps", applicationService.getApps());

        if (result.hasErrors()) {
            model.addAttribute("app", app);
            return "authenticated/admin/add_app";
        }

        applicationService.setApp(app);

        return "redirect:/olive";
    }


    /**
     * Rest API for getting all the application without parameters
     *
     * @return List of applications as JSON
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseBody
    @RequestMapping(value = "/data/apps", method = RequestMethod.GET)
    public List<CliApplication> getApps() {

        return applicationService.getAppsNoParams();
    }

    /**
     * API for changing Peregrine configurations
     *
     * @param model the model
     * @return the view
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/peregrine-config", method = RequestMethod.GET)
    public String showPeregrineConfig(Model model) {
        model.addAttribute("apps", applicationService.getApps());
        return "authenticated/admin/peregrine";
    }

    /**
     * Gets all the peregrine modules
     *
     * @return the view
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/get-modules", method = RequestMethod.GET)
    @ResponseBody
    public List<Module> getModules() {
        return applicationService.getPeregrineModules();

    }

    /**
     * API for changing Peregrine configurations
     *
     * @param module the module
     * @return the view
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/add-module", method = RequestMethod.POST)
    @ResponseBody
    public boolean addModule(Module module) {
        applicationService.setPeregrineModule(module);
        return true;
    }

    /**
     * API for changing Peregrine configurations
     *
     * @param module the module
     * @return the view
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/remove-module", method = RequestMethod.POST)
    @ResponseBody
    public boolean removeModule(Module module) {
        applicationService.removePeregrineModule(module);
        return true;
    }

    /**
     * Databinder for mapping a list of module id's on module objects.
     *
     * @param binder data binder
     */
    @InitBinder
    protected void initBinder(WebDataBinder binder) {

        binder.registerCustomEditor(List.class, "modules", new CustomCollectionEditor(List.class) {
            @Override
            protected Object convertElement(Object element) {
                String idString = (String) element;
                int id = Integer.parseInt(idString);

                Module module = new Module();
                module.setId(id);

                return module;
            }
        });
    }

}
