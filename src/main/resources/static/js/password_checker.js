/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

$(function () {
    
    function canSubmit() {
        var password = $("#password").val();
        var confirmPass = $("#confirm-pass").val();

        if (password != confirmPass) {
            $("#match-pass-message").text("Passwords don't match");
            $("#match-pass-input").addClass("has-error").removeClass("has-success");
            return false;
        }
        else {
            return true;
        }
    }

    var shouldValidate = false;

    function checkPasswordsMatch() {
        var password = $("#password").val();
        var confirmPass = $("#confirm-pass").val();

        shouldValidate = password.length > 0 && confirmPass.length > 0;

        if (shouldValidate) {
            if (password == confirmPass) {
                $("#match-pass-message").text("Passwords match");
                $("#match-pass-input").addClass("has-success").removeClass("has-error");
            } else {
                $("#match-pass-message").text("Password don't match");
                $("#match-pass-input").addClass("has-error").removeClass("has-success");
            }
        }
    }

    $("#password").keyup(checkPasswordsMatch);
    $("#confirm-pass").keyup(checkPasswordsMatch);
    $("#account-form").submit(canSubmit);
});



