/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.olifarm.olive.io.RemoteFileService;
import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobDao;
import com.olifarm.olive.job.JobGroup;
import com.olifarm.olive.job.JobStatus;
import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.CliApplicationDao;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.job.cliapplication.ParameterType;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.Utils;
import com.olifarm.olive.web.controller.JobRestService;
import com.olifarm.olive.web.io.NameServiceImpl;
import com.olifarm.olive.web.io.PeregrineSession;
import com.olifarm.olive.web.io.PeregrineSessionAbsentException;
import com.olifarm.olive.web.io.SshService;
import com.olifarm.olive.web.job.application.CliApplicationForm;
import com.olifarm.olive.web.job.application.FileParameter;
import com.olifarm.olive.web.job.peregrine.JobFormPeregrine;
import com.olifarm.olive.web.notifications.notification.NotificationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Service used for creating an editing jobs.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Service
public class JobService {

    /**
     * Logger object used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * RabbitTemplate used for sending and receiving messages from the RabbitMq server.
     */
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * FtpService used for interacting with the ftp server.
     */
    @Autowired
    private RemoteFileService remoteFileService;

    /**
     * The job request queue used for sending new jobs to the RabbitMq server
     */
    @Autowired
    private Queue jobRequests;

    /**
     * The JobDao object used for updating the database.
     */
    @Autowired
    private JobDao jobDao;

    /**
     * ApplicationDao used for updating the database.
     */
    @Autowired
    private CliApplicationDao applicationDao;

    /**
     * NameService used for generating file and folder names.
     */
    @Autowired
    @Qualifier("nameServiceImpl")
    private NameServiceImpl nameService;

    /**
     * NotificationService used for creating new notifications.
     */
    @Autowired
    private NotificationService notificationService;

    /**
     * SshService used for connecting to the peregrine cluster.
     */
    @Autowired
    private SshService sshService;


    /**
     * Create and exectute a new job.
     *
     * @param job              the job to create
     * @param peregrineSession the peregrine session
     * @return boolean indicating success
     * @throws IOException                     the io exception
     * @throws JSchException                   the j sch exception
     * @throws SftpException                   the sftp exception
     * @throws PeregrineSessionAbsentException the peregrine session absent exception
     */
    public boolean createNewJob(JobFormPeregrine job, PeregrineSession peregrineSession)
            throws IOException, JSchException, SftpException, PeregrineSessionAbsentException {

        job.setStatus(JobStatus.PROCESSING);
        job.setStartTime(new Date());

        // Calculate the job id for that specific user.
        int userJobId = jobDao.getMaxUserJobId(job.getUser());
        job.setUsersJobId(userJobId <= 0 ? 1 : userJobId + 1);
        boolean returnVal = false;

        switch (job.getRunType()) {
            case BASIC:
                job.setPeregrineEnabled(false);
                if (!jobDao.create(job)) {
                    return false;
                }
                job = this.removeEmptyParams(job);
                addOutputParams(job);
                returnVal = runBasic(job);
                break;

            case PEREGRINE:
                job.setPeregrineEnabled(true);
                if (!jobDao.create(job)) {
                    return false;
                }
                job = this.removeEmptyParams(job);
                returnVal = runPeregrine(job, peregrineSession);
                addOutputParams(job);
                break;
        }

        return returnVal;
    }

    /**
     * Adds the output params to a job and stores the reference into the db.
     *
     * @param job the job
     */
    private void addOutputParams(Job job) {
        List<Parameter> outputParams = applicationDao.getOutputParams(job.getApp());
        job.getApp().getParameters().addAll(outputParams);
        nameService.addRemotePathToOutputParams(job);
        // Add the parameters with values to DB
        jobDao.setOutputParams(job);
    }

    /**
     * Get a job from the database.
     *
     * @param id   the jobs id
     * @param user the user
     * @return the job
     */
    public Job getJob(int id, User user) {
        Job job = jobDao.getJob(id, user);

        CliApplication app = applicationDao.getApplication(job.getApplicationId());
        app.setParameters(jobDao.getParameters(job));

        job.setApp(app);

        return job;
    }

    /**
     * Get a list of jobs for a user.
     *
     * @param user the user
     * @return a list of jobs
     */
    public List<Job> getJobs(User user) {
        return jobDao.getJobs(user);
    }

    /**
     * Gets jobs.
     *
     * @param user     the user
     * @param jobGroup the job group
     * @return the jobs
     */
    public List<Job> getJobs(User user, JobGroup jobGroup) {
        return jobDao.getJobsByGroupId(user, jobGroup.getId());
    }

    /**
     * Get a list of jobs for a user with a limited ammount.
     *
     * @param user  the user
     * @param limit the limit
     * @return a list of jobs
     */
    public List<Job> getJobs(User user, int limit) {
        return jobDao.getJobsLimit(user, limit);
    }

    /**
     * Get a list of job objects with their applications.
     *
     * @param user the user
     * @return a list of job objects
     */
    public List<Job> getJobsWithApps(User user) {
        List<Job> jobs = jobDao.getJobs(user);
        addApplicationsToJobs(jobs);

        return jobs;
    }

    /**
     * Gets jobs.
     *
     * @param user     the user
     * @param jobGroup the job group
     * @return the jobs
     */
    public List<Job> getJobsWithApps(User user, JobGroup jobGroup) {
        List<Job> jobs = jobDao.getJobsByGroupId(user, jobGroup.getId());
        addApplicationsToJobs(jobs);

        return jobs;
    }

    /**
     * Get a list of jobs for a user with a limited ammount.
     *
     * @param user  the user
     * @param limit the limit
     * @return a list of jobs
     */
    public List<Job> getJobsWithApps(User user, int limit) {
        List<Job> jobs =  jobDao.getJobsLimit(user, limit);
        addApplicationsToJobs(jobs);

        return jobs;
    }

    /**
     * Get a job from the database with empty parameters.
     *
     * @param id   the jobs id.
     * @param user the user
     * @return the job
     */
    public JobForm getJobWithEmptyParams(int id, User user) {

        Job job = this.getJob(id, user);
        List<Parameter> emptyParams = applicationDao.getInputParameters(job.getApp());
        List<Parameter> finalParams = new ArrayList<>(job.getApp().getParameters());
        
        for (Parameter emptyParam : emptyParams) {

            boolean present = false;
            for (Parameter param : job.getApp().getParameters()) {
                if (param.equals(emptyParam)) {
                    present = true;
                    break;
                }
            }
            if (!present && !emptyParam.getType().isSystemParam()) {
                finalParams.add(emptyParam);
            }
        }

        job.getApp().setParameters(finalParams);

        CliApplicationForm app = new CliApplicationForm(job.getApp());
        app.moveInputFileParameters();
        app.emptyFileParameters();

        app.setFileParameters(getInputFileParametersWithValue(job));

        JobForm jobForm = new JobForm();
        BeanUtils.copyProperties(job, jobForm);

        jobForm.setApp(app);

        return jobForm;
    }

    private List<FileParameter> getInputFileParametersWithValue(Job job) {
        List<FileParameter> fileParameters = new ArrayList<>();

        for (Parameter parameter :applicationDao.getInputFileParameters(job.getApp())) {
            Parameter parameter1;
            try {
                parameter1 = jobDao.getParameter(job.getUser(), job.getId(), parameter.getId());
                fileParameters.add(new FileParameter(parameter1));

            } catch (EmptyResultDataAccessException e) {
                fileParameters.add(new FileParameter(parameter));
            }
        }

        return fileParameters;
    }

    /**
     * Update job status.
     *
     * @param job the job
     */
    public void updateJobStatus(Job job) {
        if (job.getStatus() == JobStatus.DONE
                || job.getStatus() == JobStatus.COMPLETED_WITH_ERROR
                || job.getStatus() == JobStatus.FATAL_ERROR) {

            job.setStopTime(new Date());
            jobDao.updateStopTime(job);
        }
        jobDao.updateStatus(job);
    }

    /**
     * Create a new job with a specific file.
     *
     * @param user    the user
     * @param appId   the applications id
     * @param jobId   the jobs id
     * @param paramId the parameters id
     * @return the job with the file set
     */
    public Job createJobWithFile(User user, int appId, int jobId, int paramId) {
        JobForm job = new JobForm();

        CliApplicationForm app = new CliApplicationForm(applicationDao.getApplication(appId));

        // TODO: See if this is nessacary after update to getInputFileParameters.
        List<Parameter> parameters = applicationDao.getInputFileParameters(app);
        parameters.addAll(applicationDao.getInputParameters(app));

        app.setParameters(parameters);
        Parameter fileParam = jobDao.getParameter(user, jobId, paramId);

        for (Parameter parameter : app.getParameters()) {
            if (parameter.getType() == ParameterType.INPUT_FILE) {
                parameter.setArguments(fileParam.getArguments());
                break;
            }
        }

        app.moveInputFileParameters();
        job.setApp(app);

        return job;
    }

    /**
     * Uploads a jobs files to the ftp server.
     *
     * @param job the job
     * @throws IOException the io exception
     */
    public void uploadFiles(JobForm job) throws IOException {

        List<FileParameter> fileParameters = job.getApp().getFileParameters();
        if (fileParameters == null || fileParameters.isEmpty()) {
            return;
        }
        String remoteInputDir = nameService.makeRemoteInputDir(job);

        remoteFileService.open();
        remoteFileService.mkdir(remoteInputDir);

        int fileNr = 0;
        //TODO: cleanup duplicate code
        // Do the file uploads
        for (FileParameter fileParameter : job.getApp().getFileParameters()) {

            boolean fileSet = false;
            String fileName = null;
            String filePath = null;
            MultipartFile file = fileParameter.getFile();

            if (file != null && file.getSize() != 0) {
                fileName = nameService.createFilename(job, fileParameter.getExtensionFromFilename(), fileNr);
                filePath = remoteInputDir + "/" + fileName;
                remoteFileService.setFile(file.getInputStream(), filePath);
                fileSet = true;

            } else if (fileParameter.getArguments() != null) {
                fileName = nameService.createFilename(job, Utils.getExtensionFromFilename(fileParameter.getFileNameFromPath()), fileNr);
                filePath = remoteInputDir + "/" + fileName;
                remoteFileService.copy(fileParameter.getArguments().get(0), filePath);
                fileSet = true;
            }

            if (fileSet) {
                fileParameter.setArguments(Collections.singletonList(filePath));
                fileParameter.setRemoteFilePath(filePath);
                fileParameter.setFilename(fileName);
                fileParameter.setFile(null);
                fileNr++;
                jobDao.addParameter(job, fileParameter);

                // Add the file parameter to parameters
                job.getApp().getParameters().add(convertFileParameterToParameter(fileParameter));
            }

        }
        remoteFileService.close();
    }

    /**
     * Removes parameters that do not have values.
     *
     * @param job the job
     * @return the job without empty parameters
     */
    private JobFormPeregrine removeEmptyParams(JobFormPeregrine job) {

        List<Parameter> filteredParams = new ArrayList<>();
        if (job.getApp().getParameters() != null) {
            for (Parameter parameter : job.getApp().getParameters()) {
                if (parameter.getArguments() != null && parameter.getArguments().size() > 0) {
                    filteredParams.add(parameter);
                }
            }
            job.getApp().setParameters(filteredParams);
        }

        return job;
    }

    private Job convertJobFormToJob(JobForm jobForm) {
        CliApplication cliApplication = new CliApplication();
        BeanUtils.copyProperties(jobForm.getApp(), cliApplication);

        Job job = new Job();
        BeanUtils.copyProperties(jobForm, job);
        job.setApp(cliApplication);

        return job;
    }

    private Parameter convertFileParameterToParameter(FileParameter fileParameter) {
        Parameter parameter = new Parameter();
        BeanUtils.copyProperties(fileParameter, parameter);

        return parameter;
    }

    private void addApplicationsToJobs(List<Job> jobs) {
        for (Job job : jobs) {
            CliApplication app = applicationDao.getApplication(job.getApplicationId());
            app.setParameters(jobDao.getParameters(job));

            job.setApp(app);
        }
    }

    private boolean runBasic(JobForm job) throws IOException {
        // Upload the files
        this.uploadFiles(job);

        // Convert web object to parent type objects & send it to broker
        Job jobToSend = convertJobFormToJob(job);
        rabbitTemplate.convertAndSend(jobRequests.getName(), jobToSend);

        // Create user notifications
        notificationService.setNotification(NotificationService.createJobEventNotification(job, "sent to queue"));

        return true;
    }

    private boolean runPeregrine(JobFormPeregrine job, PeregrineSession peregrineSession) throws PeregrineSessionAbsentException, JSchException, IOException, SftpException {

        Session session = SshService.makeSshSession(peregrineSession);

        User user = job.getUser();
        user.setPeregrineUsername(session.getUserName());
        job.setUser(user);

        int peregrineId = sshService.executePeregrineJob(job, session);

        if (peregrineId > 0) {
            job.setPeregrineJobId(peregrineId);
            jobDao.setPeregrineJobId(job);
            return true;
        } else {
            return false;
        }

    }

}
