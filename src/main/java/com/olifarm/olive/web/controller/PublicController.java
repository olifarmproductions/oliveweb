/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.olifarm.olive.user.User;
import com.olifarm.olive.user.UserDao;
import com.olifarm.olive.web.user.UserService;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Controller class for resolving the public views.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Controller
public class PublicController {


    /**
     * Resolves the landing page view.
     *
     * @return the view
     */
    @RequestMapping("/")
    public String showLandingPage() {
        return "public/landing_page";
    }

    /**
     * Resolves the landing page view.
     *
     * @return the view
     */
    @RequestMapping("/landing-page")
    public String landingPage() {
        return "public/landing_page";
    }

    /**
     * Resolves the documentation page view.
     *
     * @return the view
     */
    @RequestMapping("/docs/home")
    public String docsMain() {
        return "public/docs/docs_home";
    }

}
