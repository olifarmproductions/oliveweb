![Flow](/meta/logo/olive_logo_banner.png)

-----------
# Olive Web

## Contents
+ [About](#markdown-header-about)
+ [Installation](#markdown-header-installation)
    - [JAR](#markdown-header-jar)
    - [WAR](#markdown-header-war)
    - [Dependencies](#markdown-header-dependencies)
    - [Build from sources](#markdown-header-build-from-sources)
        * [Intellij](#markdown-header-intellij)
        * [Gradle](#markdown-header-gradle)
        * [Netbeans](#markdown-header-netbeans)
+ [Configuration](#markdown-header-configuring-the-webapp)
    - [Datasource](#markdown-header-configuring-an-external-datasource)
    - [Rabbit Server](#markdown-header-configuring-a-rabbit-server)
    - [Sftp Server](#markdown-header-configuring-a-sftp-server)
    - [Peregrine](#markdown-header-configuring-peregrine)
    - [Server](#markdown-header-server-settings)
    - [Email](#markdown-header-email-settings)
    - [Miscellaneous](#markdown-header-miscellaneous)
+ [Known issues and troubleshooting](#markdown-header-known-issues-and-troubleshooting)
---------------
## About

Installing, configuring and running course grained simulation software can be a complex process which can be very intimidating for non tech-savvy users. So much so in fact that most of these users wouldn’t even consider using these packages.  This is a shame because researchers could benefit from being able to run relatively complex protein simulations without having to resort to expert help.
Our solution to this problem is a software package called Olive. Olive includes a web interface for running these kinds of simulations and managing the results.  This web interface significantly cuts down on the expertise needed to run course grained simulations because the configuration and setup of the software has been done by and administrator. This way the end user does not have to concern him or herself with installing and configuring all the needed packages and dependencies. This way the user can focus on just running the simulations and processing the results.

This repo contains the web application of Olive


![Flow](/meta/uml/olive_file_flow.png)

---------------
## Installation
This tutorial will inform you about how to install the webserver part of the application,
not the receiving end. Howto install the receiver and its dependencies is detailed [here](https://bitbucket.org/olifarmproductions/olivejobreceiver)

The application is also reliant on a rabbitmq server for the communication between the webserver and the receiver.
Information on how to install a rabbitmq server can be found [here](http://www.rabbitmq.com/download.html) 
RabbitMQ is also depandent on erlang which also needs to be installed and can be found [here](http://www.erlang.org/doc.html).By default the application searches for a rabbitmq server on localhost. To change this see the section [below](#markdown-header-configuring-a-rabbit-server).
OliveWeb is written in java 8, so java 8 must be installed on the system you wish to host OliveWeb on.

### JAR
This method is not recomended for actual deployment. To deploy OliveWeb we reccomend the WAR method. 
Download the latest version of [OliveWeb](https://bitbucket.org/olifarmproductions/oliveweb/downloads) and make sure the system has Java 8 installed and then run the following command:

    java -jar OliveWeb<version>.jar
### WAR
To install OliveWeb using WAR download the latest version of [OliveWeb](https://bitbucket.org/olifarmproductions/oliveweb/downloads). Make sure a valid Tomcat 8 instance using java 8 is availible. Then simply place the jar into the tomcat webapps folder. For more information on how to install a WAR click [here](https://tomcat.apache.org/tomcat-8.0-doc/deployer-howto.html) 

### Dependencies
The application is written in Java 8 using the Java Spring framework to facilitate a lot of the features. When building from sources Gradle 2.9 can be used to donwload and install all the dependancies automaticly. As stated previously Olive is built the [Spring framework](https://spring.io/). Spring Boot version 1.3.0 was used to build the core of the application. Spring Integration Core and Sftp version 4.2.4 where used to facilitate the connection between the web server and the ftp server. To build the project OliveCore must be added as a jar to the libs folder if it is not yet present. OliveCore can be found [here](https://bitbucket.org/olifarmproductions/olivecore). OliveCore contains the core data beans and top level interfaces for the backend.

#### Full list of dependecies
##### Backend 
+ spring-boot-starter-jdbc
+ spring-boot-starter-security
+ spring-boot-starter-thymeleaf
+ spring-boot-starter-web
+ spring-boot-starter-mail
+ spring-boot-devtools
+ spring-boot-starter-amqp
+ thymeleaf-extras-springsecurity 4:2.1.2.RELEASE
+ nekohtml:1.9.21
+ h2database
+ spring-integration-core:4.2.4.RELEASE
+ spring-integration-sftp:4.2.4.RELEASE
+ [OliveCore](https://bitbucket.org/olifarmproductions/olivecore) 
##### Frontend
+ bootstrap:3.3.5
+ font-awesome:4.5.0
+ jquery:2.1.4
+ metisMenu:1.1.3
+ startbootstrap-sb-admin-2:1.0.7
+ org.webjars:datatables:1.10.9
+ org.webjars:handlebars:4.0.2
+ org.webjars:d3js:3.5.17


### Build from sources

##### Intellij

Simply clone the project repo and import it into Intellij. Then link the gradle root and set it to use the 
'default gradle wrapper'. For Intellij usage see the [manual](https://www.jetbrains.com/idea/help/working-with-gradle-projects.html).
Then open the gradle tab and build the project by pressing the blue refresh button. If the global SDK is not set then
this must be [set](https://www.jetbrains.com/idea/help/configuring-global-project-and-module-sdks.html) to the java 8 JDK.
If these things have been configured the project can be run.

##### Netbeans 

For running the project in Netbeans the Gradle plugin must first be installed. The plugin can be found [here](http://plugins.netbeans.org/plugin/44510/gradle-support).
Once installed go to Team > Git > Clone and clone the repo. For more information see [this](https://netbeans.org/kb/docs/ide/git.html). 
Then let gradle finish building the project and start the app by running "OliveApplication"

##### Gradle Command line 

To build the project using the commandline you can use gradle. Spring has an excelent [tutorial](https://spring.io/guides/gs/gradle/) on how to do this.Since the gradle file and sources have already been made you can skip straight to the executing part.

---------------
## Configuring the webapp ##
To configure the application the file application.properties is used. The repo contains a template applications
properties files named application.template. Simply create a file called application.properties in the resources folder
and fill in the gaps. To override the default properties file (which is highly reccomended) simply create a folder
called config in the folder where the jar/war is located and make a file called 'application.properties' in the config folder. Add all
the settings you want to change in this file. Then (re) start Olive Web. 

We reccomend to thouroughly read this section since many errors are caused by faulty configuration. 

>NOTE: <> indacates that something is a placeholder. 

#### Configuring an external datasource

By default the application is configured with an internal H2 database server. This method is not recommended and should 
only be used for "demoing" purposes only. This is because upon reboot the internal database is reset because this 
database runs in-memory. We recommend configuring an external database server for the application.
To change this remove the following line from the application.properties. 
 
     spring.datasource.url=jdbc:h2:mem:testdb;Mode=Oracle
     
 And than add the following lines instead.
 
     spring.datasource.url=jdbc:mysql://<host>:3306/<database>
     spring.datasource.username=<user>
     spring.datasource.password=<password>

#### Configuring a Rabbit server

    # Enable if server is not on localhost
    spring.rabbitmq.host=<host>
    spring.rabbitmq.username=<user>
    spring.rabbitmq.password=<password>
    spring.rabbitmq.port=5672

#### Configuring a SFTP server
    # Set the SSH module only to log warnings
    logging.level.com.jcraft.jsch=WARN
    
    ftp.host=<host>
    ftp.username=<user>
    ftp.password=<password>
    ftp.external_storage_root=<Olive's working directory on the ftp server>


#### Configuring Peregrine
The connection settings for peregrine can be set as follows:

    # Set the perigrine host
    perigrine.ssh.host= <peregrine host>
    perigrine.ssh.port= 22
    # Timeout in milliseconds
    perigrine.ssh.timeout= 6000

To confiugre any custom applications not coverd by the module system in peregrine they must be added to a centeral folder. They can then be added to the interface by typing the name and extenstion of the application into the custom.tools option.

    # Set the folder containing custom applications (such as martinitools) not inculed in the module system
    peregrine.custom.tools.home = <path/to/folder>
    # Set the names and extesions of the tools in the folder seperated by a "," NO SPACE IN BETWEEN!
    peregrine.custom.tools = <martinate.sh,insane.py>

#### Server settings
    # Tomcat server port, defaults to 8080
    #server.port=8085
    # Tomcat session timeout
    server.session.timeout=36000

#### Email settings
These settings are used to set the email server Olive uses to send emails to users.

    # Settings for the email server
    spring.mail.host = <email.host>
    spring.mail.port = <email port>
    
    # Settings for the email account
    mail.address = <email@adress>
    spring.mail.username = <email.username>
    spring.mail.password = <email.password>
    
    # Settings for the email backend
    spring.mail.default-encoding=UTF-8
    spring.mail.properties.mail.smtp.starttls.enable=true
    spring.mail.properties.mail.smtp.starttls.required=true
    spring.mail.properties.mail.smtp.auth = true;
    spring.mail.properties.mail.socketFactory.port=587
    spring.mail.properties.mail.socketFactory.class=javax.net.ssl.SSLSocketFactory
    spring.mail.properties.mail.socketFactory.fallback=false

#### Miscellaneous settings
OliveWeb also has some miscellaneous settings that need to be set. 

    # Set the HTMl mode to HTML 5
    spring.thymeleaf.mode=LEGACYHTML5
    # Set the whitelabel error page to false
    server.error.whitelabel.enabled=false
    # Set the caching period for resources to -1
    spring.resources.cache-period=-1
    
    # Set the text encoding to UTF-8
    spring.http.encoding.charset=UTF-8
    spring.http.encoding.enabled=true
    spring.http.encoding.force=true
    
    # Configure the maximum filesizes for uploads
    multipart.maxFileSize=250000MB
    multipart.maxRequestSize=250000MB
    
    # Set the global logging level for spring modules to INFO
    log4j.logger.org.springframework=INFO

------------
## Known issues and troubleshooting

> Olive web does start but doesn't work properly
    
Make sure the webserver is running with Java 8


> Olive throws an AMQP connect exception

Make sure the AMQP broker is correctly configured and running. How to configure a broker can be found [here](#markdown-header-configuring-a-rabbit-server)

> The status of a job is stuck on 'Unknown' 


This means the status can not be retreived. This is usually due to the Peregrine tool jobinfo not being able to retreive the status.


> I cannot download a file.
 
This is usually due to the file not being present on the ftp server. Maybe the selected tool did not generate the file due to some faulty parameters. Check the stdout to see what is wrong.


> There is no stderr file.

When running jobs on Peregrine the stdout and stderr get merged into the stdout file.


> A configured tool won't excute, it throws an exception

Thouroughly check if everything has been configured properly. Maybe a misplaced space or typo in the configuration file?.
Some good places to check would be in the [Peregrine settings](#markdown-header-configuring-peregrine) or when using the node
the node configuration.