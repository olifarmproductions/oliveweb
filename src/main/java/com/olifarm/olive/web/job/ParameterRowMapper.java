/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job;



import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.job.cliapplication.ParameterType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * RowMapper used to convert the result of the database query to a parameter object.
 *
 * @author Harm Brugge
 */
public class ParameterRowMapper implements RowMapper<Parameter> {
    @Override
    public Parameter mapRow(ResultSet rs, int rowNum) throws SQLException {
        Parameter param = new Parameter();

        param.setId(rs.getInt("parameter_id"));
        param.setFlag(rs.getString("flag"));
        param.setType(ParameterType.valueOf(rs.getString("type")));
        param.setArgNum(rs.getInt("arg_num"));
        param.setDescription(rs.getString("description"));
        param.setLevel(rs.getInt("level"));
        param.setName(rs.getString("name"));
        param.setAddition(rs.getString("addition"));
        String[] args;

        if (rs.getString("value") != null) {
            args = rs.getString("value").replaceAll("\\[", "").replaceAll("\\]", "").split(",");
        } else {
            args = new String[0];
        }
        param.setArguments(Arrays.asList(args));
        param.setArgSep(rs.getString("arg_sep"));
        param.setLongFlag(rs.getBoolean("long_flag"));
        param.setAllowMultiple(rs.getBoolean("allow_multiple"));

        return param;
    }
}
