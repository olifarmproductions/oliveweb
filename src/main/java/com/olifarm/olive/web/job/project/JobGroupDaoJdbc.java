/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

package com.olifarm.olive.web.job.project;

import com.olifarm.olive.job.JobGroup;
import com.olifarm.olive.job.JobGroupDao;
import com.olifarm.olive.job.Project;
import com.olifarm.olive.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * The type Job group dao jdbc.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Component
public class JobGroupDaoJdbc implements JobGroupDao {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Override
    public int addJobGroup(JobGroup jobGroup) {

        GeneratedKeyHolder key = new GeneratedKeyHolder();
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(jobGroup);

        if (jobGroup.getParentId() > 0) {

            if (jobGroup.getProjectId() > 0) {
                jdbc.update("INSERT INTO job_group (user_id, parent_id, project_id, name,description) " +
                        "VALUES (:userId, :parentId,  :projectId, :name, :description)",
                        params,
                        key);
            } else {
                jdbc.update("INSERT INTO job_group (user_id, parent_id, name,description) " +
                        "VALUES (:userId, :parentId, :name, :description)",
                        params,
                        key);
            }

        } else {

            if (jobGroup.getProjectId() > 0) {
                jdbc.update("INSERT INTO job_group (user_id, project_id, name, description) " +
                        "VALUES (:userId, :projectId, :name, :description)",
                        params,
                        key);
            } else {
                jdbc.update("INSERT INTO job_group (user_id, name, description) VALUES (:userId, :name, :description)",
                        params,
                        key);
            }
        }

        if (jobGroup.hasChildren()) {
            for (JobGroup jobGroup1 : jobGroup.getChildren()) {
                jobGroup1.setParentId(key.getKey().intValue());
                this.addJobGroup(jobGroup1);
            }
        }

        return key.getKey().intValue();
    }

    @Override
    public void removeJobGroup(JobGroup jobGroup) {
        jdbc.update("DELETE from job_group where id = :id and user_id = :userId",
                new BeanPropertySqlParameterSource(jobGroup));
    }

    @Override
    public void updateJobGroup(JobGroup jobGroup) {

        if (jobGroup.getProjectId() != 0) {
            jdbc.update("UPDATE job_group SET " +
                    "user_id =:userId," +
                    "project_id =:projectId, " +
                    "name =:name," +
                    "description =:description " +
                    "WHERE id =:id",
                    new BeanPropertySqlParameterSource(jobGroup));
        } else {
            jdbc.update("UPDATE job_group SET " +
                    "user_id =:userId, " +
                    "name =:name," +
                    "description =:description " +
                    "WHERE id =:id",
                    new BeanPropertySqlParameterSource(jobGroup));
        }
    }

    @Override
    public JobGroup getJobGroup(User user, int id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", user.getId());
        params.addValue("id", id);

        return addChildren(jdbc.queryForObject("SELECT * FROM job_group WHERE user_id = :userId AND id = :id",
                params,
                new JobGroupRowmapper()));
    }

    private JobGroup addChildren(JobGroup parent) {

        List<JobGroup> children = jdbc.query("SELECT * FROM job_group WHERE parent_id =:id AND user_id =:userId",
                new BeanPropertySqlParameterSource(parent),
                new JobGroupRowmapper());

        if (children.size() > 0) {
            // is not end node so run for all children
            for (JobGroup child : children) {
                this.addChildren(child);
                parent.addChild(child);
            }
        }

        return parent;
    }

    private List<JobGroup> addChildren(List<JobGroup> rootGroups) {
        for (JobGroup rootGroup : rootGroups) {
            addChildren(rootGroup);
        }
        return rootGroups;
    }

    private List<JobGroup> getRootGroups(User user) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", user.getId());

        return jdbc.query("SELECT * FROM job_group WHERE user_id =:userId AND parent_id IS NULL",
                params,
                new JobGroupRowmapper());
    }

    private List<JobGroup> getRootGroups(Project project) {
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(project);

        return jdbc.query("SELECT * FROM job_group WHERE user_id =:userId AND project_id =:id AND parent_id IS NULL ",
                params,
                new JobGroupRowmapper());
    }

    @Override
    public List<JobGroup> getJobGroups(User user) {
        return addChildren(getRootGroups(user));
    }

    @Override
    public List<JobGroup> getJobGroupsAsList(User user) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", user.getId());

        return jdbc.query("SELECT * FROM job_group WHERE user_id = :userId",
                params,
                new JobGroupRowmapper());
    }

    @Override
    public List<JobGroup> getJobGroups(Project project) {
        return addChildren(getRootGroups(project));
    }
}
