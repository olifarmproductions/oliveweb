/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.application;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.CliApplicationDao;
import com.olifarm.olive.job.cliapplication.Module;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.web.job.ParameterRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Implementation of ApplicationDao for JDBC
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
@Component
public class CliApplicationDaoJdbc implements CliApplicationDao {

    /**
     * The Jdbc.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Override
    public CliApplication getApplication(int id) {
        CliApplication app = jdbc.queryForObject("SELECT * FROM applications WHERE id=:id",
                new MapSqlParameterSource("id", id), BeanPropertyRowMapper.newInstance(CliApplication.class));
        return app;
    }

    @Override
    public List<Parameter> getInputParameters(CliApplication app) {
        return jdbc.query("SELECT * FROM parameters WHERE application_id =:id "
                        + "AND level != 0 "
                        + "AND type != 'INPUT_FILE' "
                        + "ORDER BY form_group, level, id ASC",
                new MapSqlParameterSource("id", app.getId()),
                new FormParameterRowMapper());
    }

    @Override
    public List<Parameter> getInputFileParameters(CliApplication app) {
        return jdbc.query("SELECT * FROM parameters WHERE application_id =:id "
                        + "AND level != 0 "
                        + "AND type = 'INPUT_FILE' "
                        + "ORDER BY form_group, level, id ASC",
                new MapSqlParameterSource("id", app.getId()),
                BeanPropertyRowMapper.newInstance(Parameter.class));
    }

    /**
     * Gets input file parameter value.
     *
     * @param job the job
     * @param i   the
     * @return the input file parameter value
     */
    @Deprecated
    public Parameter getInputFileParameterValue(Job job, int i) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("jobId", job.getId());
        params.addValue("paramId", i);

        return jdbc.queryForObject("SELECT * FROM job_parameters WHERE parameter_id = :paramId AND job_id = :jobId",
                params,
                new FormParameterRowMapper());
    }

    @Override
    public List<CliApplication> getApplications() {
        return jdbc.query("SELECT * FROM applications",
                new MapSqlParameterSource(),
                BeanPropertyRowMapper.newInstance(CliApplication.class));
    }

    @Override
    public List<Parameter> getOutputParams(CliApplication app) {
        return jdbc.query("SELECT * FROM parameters WHERE application_id =:id AND level = 0",
                new MapSqlParameterSource("id", app.getId()),
                new FormParameterRowMapper());
    }

    @Override
    public List<Module> getPeregrineModules() {

        return jdbc.query("SELECT * FROM peregrine_modules",
                new MapSqlParameterSource(),
                BeanPropertyRowMapper.newInstance(Module.class));
    }

    @Override
    public List<Module> getPeregrineModulesByAppId(int applicationId) {

        return jdbc.query("SELECT pm.* "
                        + "FROM peregrine_modules pm "
                        + "JOIN application_modules am "
                        + "ON pm.id = am.peregrine_module_id "
                        + "WHERE am.application_id=:appId",
                new MapSqlParameterSource("appId", applicationId),
                BeanPropertyRowMapper.newInstance(Module.class));
    }

    @Override
    public int setApp(CliApplication app) {

        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(app);
        GeneratedKeyHolder key = new GeneratedKeyHolder();

        jdbc.update("INSERT INTO applications "
                        + "(name, zip_output, list_separator, custom_view, peregrine_enabled) "
                        + "VALUES (:name, :zipOutput, :listSeparator, :customView, :peregrineEnabled)",
                params,
                key);

        return key.getKey().intValue();
    }

    @Override
    public void setAppParams(CliApplication app) {

        for (Parameter param : app.getParameters()) {
            this.addParameter(app, param);
        }
    }

    @Override
    public void setStdOut(CliApplication app) {
        jdbc.update("INSERT INTO parameters "
                        + "(application_id, long_flag, flag, type, arg_num, description, level, name, extension) "
                        + "VALUES (:id, FALSE, '', 'STD_OUT', 1, 'The standard output', 0, 'stdout', 'stdout.log')",
                new MapSqlParameterSource("id", app.getId()));
    }

    @Override
    public void setStdErr(CliApplication app) {
        jdbc.update("INSERT INTO parameters "
                        + "(application_id, long_flag, flag, type, arg_num, description, level, name, extension) "
                        + "VALUES (:id, FALSE, '', 'STD_ERR', 1, 'The standard error output', 0, 'stderr', 'stderr.log')",
                new MapSqlParameterSource("id", app.getId()));
    }

    @Override
    public void updateApp(CliApplication app) {
        jdbc.update("UPDATE applications "
                        + "SET name =:name, "
                        + "    zip_output =:zipOutput, "
                        + "    list_separator =:listSeparator, "
                        + "    custom_view =:customView, "
                        + "    peregrine_enabled =:peregrineEnabled "
                        + "WHERE id =:id",
                new BeanPropertySqlParameterSource(app));
    }

    @Override
    public boolean existsParameter(Parameter parameter) {

        boolean result = jdbc.queryForObject("SELECT EXISTS("
                        + "SELECT 1 FROM parameters "
                        + "WHERE id =:id "
                        + "LIMIT 1)",
                new MapSqlParameterSource("id", parameter.getId()),
                Boolean.class);

        return result;
    }

    @Override
    public void updateParameter(Parameter parameter) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", parameter.getId());
        params.addValue("name", parameter.getName());
        params.addValue("flag", parameter.getFlag());
        params.addValue("longFlag", parameter.isLongFlag());
        params.addValue("argNum", parameter.getArgNum());
        params.addValue("argSep", parameter.getArgSep());
        params.addValue("description", parameter.getDescription());
        params.addValue("level", parameter.getLevel());
        params.addValue("extension", parameter.getExtension());
        params.addValue("formGroup", parameter.getFormGroup());
        params.addValue("allowMultiple", parameter.isAllowMultiple());
        params.addValue("addition", parameter.getAddition());
        params.addValue("type", parameter.getType().toString());

        if (parameter.getArguments() != null) {
            params.addValue("value", parameter.getArguments().toString());
        } else {
            params.addValue("value", null);
        }

        jdbc.update("UPDATE parameters "
                        + "SET "
                        + "name = :name, "
                        + "flag = :flag, "
                        + "long_flag = :longFlag, "
                        + "arg_num = :argNum, "
                        + "arg_sep = :argSep, "
                        + "description = :description, "
                        + "level = :level, "
                        + "extension = :extension, "
                        + "form_group = :formGroup,"
                        + "allow_multiple = :allowMultiple, "
                        + "addition = :addition, "
                        + "type = :type, "
                        + "value = :value "
                        + "WHERE id = :id",
                params
        );

    }

    @Override
    public void addParameter(CliApplication app, Parameter parameter) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("appId", app.getId());
        params.addValue("name", parameter.getName());
        params.addValue("argNum", parameter.getArgNum());
        params.addValue("flag", parameter.getFlag());
        params.addValue("longFlag", parameter.isLongFlag());
        params.addValue("type", parameter.getType().toString());
        params.addValue("description", parameter.getDescription());
        params.addValue("level", parameter.getLevel());
        params.addValue("extension", parameter.getExtension());
        params.addValue("argSep", parameter.getArgSep());
        params.addValue("formGroup", parameter.getFormGroup());
        params.addValue("allowMultiple", parameter.isAllowMultiple());
        params.addValue("addition", parameter.getAddition());

        if (parameter.getArguments() != null) {
            params.addValue("value", parameter.getArguments().toString());
        } else {
            params.addValue("value", null);
        }

        jdbc.update("INSERT INTO parameters "
                        + "(application_id, name, flag, long_flag, type,arg_num, description, "
                        + "level, extension, arg_sep, form_group, allow_multiple, addition, value) "
                        + "VALUES (:appId, :name, :flag, :longFlag, :type, :argNum, :description, "
                        + ":level, :extension, :argSep, :formGroup, :allowMultiple, :addition, :value)",
                params);

    }

    @Override
    public void removeParameter(Parameter parameter) {

        jdbc.update("DELETE FROM parameters WHERE id=:id",
                new MapSqlParameterSource("id", parameter.getId()));
    }

    @Override
    public void addModules(CliApplication cliApplication, List<Module> modules) {

        MapSqlParameterSource params = new MapSqlParameterSource("applicationId", cliApplication.getId());

        for (Module module : modules) {
            params.addValue("moduleId", module.getId());
            jdbc.update("INSERT INTO application_modules (application_id, peregrine_module_id) "
                    + "VALUES (:applicationId, :moduleId)", params);
        }

    }

    @Override
    public void removeModules(CliApplication cliApplication, List<Module> modules) {
        MapSqlParameterSource params = new MapSqlParameterSource("applicationId", cliApplication.getId());

        for (Module module : modules) {
            params.addValue("moduleId", module.getId());
            jdbc.update("DELETE FROM application_modules "
                    + "WHERE application_id =:applicationId "
                    + "AND peregrine_module_id =:moduleId", params);

        }

    }

    @Override
    public void setPeregrineModule(Module module) {
        jdbc.update("INSERT INTO peregrine_modules (name, version) VALUES (:name, :version)",
                new BeanPropertySqlParameterSource(module));
    }

    @Override
    public void removePeregrineModule(Module module) {
        jdbc.update("DELETE FROM peregrine_modules where id = :id",
                new BeanPropertySqlParameterSource(module));
    }

}
