/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

/**
 * Created by Olivier on 27-5-2016.
 */
var jobModule = (function () {

    var module = {};

    var helpActive = true;
    var size = $("meta[name='params_size']").attr("content");
    var paramId = 0;

    var runWith = $("#run-with");

    if (size != null) {
        paramId = size;
    }

    module.renderHelpTable = function () {
        $('#help-table').DataTable({
            "bLengthChange": true,
            "iDisplayLength": 4,
            "lengthMenu": [1, 2, 3, 4, 5],
            "bFilter": true,
            "bSort": false,
            "bInfo": false,
            "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>'
        });
    };

    module.makeParameterGroupingHeaders = function () {

        var curGroup = "";

        $(".parameter-grouping").each(function (index) {

            if (curGroup == "") {
                if (this.getAttribute("parameter-group") != null) {
                    curGroup = this.getAttribute("parameter-group");

                    $(this).replaceWith(function () {
                        return "<h3>" + curGroup + "</h3><hr/>"
                    });
                }

            } else if (curGroup != this.getAttribute("parameter-group")) {

                curGroup = this.getAttribute("parameter-group");
                $(this).replaceWith(function () {
                    return "<h3>" + curGroup + "</h3><hr/>"

                });
            }
        });
    };

    module.addTextParameter = function (groupParamId) {

        var tmplHtml = $("#multiplicate-parameter-template-text").html();
        var tmpl = Handlebars.compile(tmplHtml);

        var id = groupParamId;
        var flag = $("#parameter-" + groupParamId + "-flag").val();
        var type = $("#parameter-" + groupParamId + "-type").val();
        var longFlag = $("#parameter-" + groupParamId + "-long-flag").val();
        var argSep = $("#parameter-" + groupParamId + "-arg-sep").val();

        var model = {
            "paramId": paramId,
            "id": id,
            "flag": flag,
            "type": type,
            "longFlag": longFlag,
            "argSep": argSep
        };

        $("#parameter-" + groupParamId).append(tmpl(model));
        paramId++;
    };

    module.removeTextParameter = function (paramId) {
        $("#param-" + paramId).remove();
    };

    module.togglePeregrine = function () {

        runWith = $("#run-with");

        if (runWith.val() == "BASIC") {
            $("#peregrine-settings").toggleClass("hidden");
        } else {
            runWith.val("BASIC");

            $.ajax({
                url: '/check-session',
                type: 'get',
                data: {
                    _csrf: token
                },
                error: function () {
                    $('#peregrine-modal').modal('show')
                },
                success: function (data) {
                    $("#peregrine-settings").removeClass("hidden");
                    runWith.val("PEREGRINE");
                }
            });
        }
    };

    module.checkPeregrine = function () {
        if (runWith.val() == "PEREGRINE") {
            $("#peregrine-settings").removeClass("hidden");
        }
    };

    module.toggleHelp = function () {
        $("#help-panel").toggleClass("hidden");

        if (helpActive) {
            $("#job-panel").removeClass();
            $("#job-panel").toggleClass("col-lg-12");
            helpActive = false;
        } else {
            $("#job-panel").removeClass();
            $("#job-panel").toggleClass("col-lg-7");
            helpActive = true;
        }
    };

    module.toggleAdvancedOptions = function () {
        $(".advanced").toggleClass("hidden", "easeInOutQuint");
    };
    
    return module;
})();
