package com.olifarm.olive.web.io;

import com.olifarm.olive.job.Job;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * The type Name service peregrine.
 */
@Service("nameServicePeregrine")
public class PeregrineNameService extends NameServiceImpl {

    /**
     * Logger class used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    @Value("${ftp.peregrine.external_storage_root}")
    private String REMOTE_STORAGE_ROOT;

    @Value("${ftp.peregrine.external_storage_dir}")
    private String REMOTE_STORAGE_DIR;

    /**
     * Post construct.
     */
    @PostConstruct
    public void postConstruct(){
        logger.info("REMOTE PEREGRINE DIR: " + REMOTE_STORAGE_DIR);

    }

    @Override
    public String makeRemoteInputDir(Job job) {
        String externalJobRoot = REMOTE_STORAGE_ROOT + "/"
                + job.getUser().getPeregrineUsername().toLowerCase()
                + REMOTE_STORAGE_DIR + "/"
                + job.getId();
        return externalJobRoot + "/input";
    }

    @Override
    public String makeRemoteOutputDir(Job job) {
        String externalJobRoot = REMOTE_STORAGE_ROOT + "/"
                + job.getUser().getPeregrineUsername().toLowerCase()
                + REMOTE_STORAGE_DIR + "/"
                + job.getId()
                + "/output";
        return externalJobRoot;
    }

}
