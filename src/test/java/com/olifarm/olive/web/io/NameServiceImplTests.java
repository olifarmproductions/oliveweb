/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.io;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.Module;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.web.TestBase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Name service impl tests.
 */
public class NameServiceImplTests extends TestBase {


    @Autowired
    private NameServiceImpl nameServiceImpl;


    private List<Parameter> validParams;

    private List<Module> validModules;

    private CliApplication application;

    private Job validJob;

    /**
     * Setup.
     */
    @Before
    public void setup(){
        validParams = new ArrayList<>();
        validParams.add(new Parameter(){{setId(1);}});
        validParams.add(new Parameter(){{setId(2);}});
        validParams.add(new Parameter(){{setId(3);}});

        validModules = new ArrayList<>();
        validModules.add(new Module(){{setId(1);}});
        validModules.add(new Module(){{setId(2);}});
        validModules.add(new Module(){{setId(3);}});

        application = new CliApplication();
        application.setId(1);
        application.setName("testApp");
        application.setParameters(validParams);
        application.setModules(validModules);

        validJob = new Job();

        validJob.setApp(application);
        validJob.setId(1);

    }


    /**
     * Create filename test sunny file nr one.
     */
    @Test
    public void createFilenameTestSunnyFileNrOne(){

        String result = nameServiceImpl.createFilename(validJob, "txt", 1);
        String expected = "testapp_job-1[1].txt";

        Assert.assertEquals(result, expected);

    }

    /**
     * Create filename test sunny file nr zero.
     */
    @Test
    public void createFilenameTestSunnyFileNrZero(){

        String result = nameServiceImpl.createFilename(validJob, "txt", 0);
        String expected = "testapp_job-1.txt";

        Assert.assertEquals(result, expected);

    }

    /**
     * Create filename test no extension.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createFilenameTestNoExtension(){
        String result = nameServiceImpl.createFilename(validJob, "", 0);
    }

    /**
     * Create filename test extension null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createFilenameTestExtensionNull(){
        String result = nameServiceImpl.createFilename(validJob, null, 0);
    }

    /**
     * Create filename test file number negative.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createFilenameTestFileNumberNegative(){
        String result = nameServiceImpl.createFilename(validJob, "txt", Integer.MIN_VALUE);
    }

    /**
     * Create filename test job invalid.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createFilenameTestJobInvalid(){

        Job invalidJob = validJob;
        CliApplication invalidApp = application;

        invalidApp.setName(null);
        invalidApp.setId(Integer.MIN_VALUE);

        invalidJob.setApp(invalidApp);

        String result = nameServiceImpl.createFilename(invalidJob, "txt", Integer.MIN_VALUE);
    }



}
