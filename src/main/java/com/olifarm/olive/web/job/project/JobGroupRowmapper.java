/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.project;

import com.olifarm.olive.job.JobGroup;
import org.springframework.beans.TypeMismatchException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by obbakker on 26-4-16.
 */
public class JobGroupRowmapper implements RowMapper<JobGroup> {

    @Override
    public JobGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
        JobGroup jb = new JobGroup();


        jb.setId(rs.getInt("id"));
        jb.setUserId(rs.getInt("user_id"));

        try {
            jb.setProjectId(rs.getInt("project_id"));
        } catch (TypeMismatchException exception) {
            jb.setProjectId(0);
        }

        try {
            jb.setParentId(rs.getInt("parent_id"));
        } catch (TypeMismatchException exception) {
            jb.setParentId(0);
        }

        jb.setName(rs.getString("name"));

        return jb;
    }
}
