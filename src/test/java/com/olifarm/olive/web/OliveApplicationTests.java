package com.olifarm.olive.web;

import com.olifarm.olive.web.controller.JobControllerTests;
import com.olifarm.olive.web.io.NameServiceImplTests;
import com.olifarm.olive.web.job.JobServiceTests;
import com.olifarm.olive.web.job.JobServiceUploadTests;
import com.olifarm.olive.web.job.project.JobGroupDaoTests;
import com.olifarm.olive.web.notifications.notification.NotificationServiceTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The type Olive application tests.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(value = {
        JobGroupDaoTests.class,
        JobServiceTests.class,
        NameServiceImplTests.class,
        NotificationServiceTests.class,
        JobControllerTests.class,
        JobServiceUploadTests.class
})
public class OliveApplicationTests {


}
