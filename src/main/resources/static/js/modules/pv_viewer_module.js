/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

var pvViewerModule = (function () {

    var module = {};

    module.rotationX90 = [
        1, 0, 0,
        0, 0, -1,
        0, 1, 0
    ];

    module.rotationY90 = [
        0, 0, 1,
        0, 1, 0,
        -1, 1, 0
    ];

    module.rotationZ90 = [
        0, -1, 0,
        1, 0, 0,
        0, 0, 1
    ];

    function gpcrViewPreset(viewer, structure) {

        var ligand = structure.select({anames: ['NC3', 'PO4']});
        var protein = structure.select({anames: ['BB', 'SC1', 'SC2', 'SC3', 'SC4']});
        var cholesterol = structure.select({anames: ['ROH', 'C1']});

        viewer.spheres('ligand', ligand, {color: pv.color.uniform("grey")});
        viewer.spheres('protein', protein, {color: pv.color.uniform("cyan")});
        viewer.spheres('cholesterol', cholesterol, {color: pv.color.uniform("yellow")});
    }

    module.basicGpcrPresetTop = function (viewer, structure) {
        viewer.clear();
        gpcrViewPreset(viewer, structure);
        viewer.setRotation(module.rotationZ90);
        viewer.autoZoom();
    };

    module.basicGpcrPresetFront = function (viewer, structure) {
        viewer.clear();
        gpcrViewPreset(viewer, structure);
        viewer.setRotation(module.rotationX90);
        viewer.autoZoom();
        viewer.spin(true);
    };

    module.basicPreset = function (viewer, structure) {
        viewer.clear();
        var ligand = structure.select("ligand");
        var protein = structure.select("protein");

        viewer.ballsAndSticks('ligand', ligand);
        viewer.cartoon('protein', structure);
        viewer.spin(true);
    };

    module.loadPdbForMartinateViz = function (jobId, callback) {
        $.ajax({
            url: "/download?job-id=" + jobId + "&param-id=38",
            type: "GET",
            success: callback
        });
    };

    module.loadPdb = function (pdbId, callback) {
        $.ajax({
            url: "pdb/" + pdbId,
            type: "GET",
            success: callback
        });
    };

    return module;
})();




