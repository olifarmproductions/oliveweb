/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

/**
 * Created by Olivier on 28-5-2016.
 */
var peregrineModule = (function () {

    var module = {};
    
    function addModule(name, version) {
        $.ajax({
            type: "POST",
            url: '/admin/add-module',
            data: {
                name: name,
                version: version,
                _csrf: token
            },
            error: function () {
                console.log("error");
            },
            success: function () {
                $("#modules").DataTable().ajax.reload();
            }
        });
    };

    module.doPeregrineLogin = function (form, callback) {
        $("#login-indicator").removeClass("hidden");
        $.ajax({
            type: "POST",
            url: '/do-auth',
            data: $("#peregrine-login").serialize(),
            error: function () {
                $("#login-message").removeClass("hidden");
                $("#login-indicator").addClass("hidden");
            },
            success: function () {
                $("#login-indicator").addClass("hidden");
                $('#peregrine-modal').modal("hide");
                $("#peregrine-settings").removeClass("hidden");
                $("#run-with").val("PEREGRINE");

                if (callback) {
                    callback()
                }
            }
        });

        form.preventDefault();
    };

    module.addModulePost = function () {
        var name = $("#peregrine-module-name").val();
        var version = $("#peregrine-module-version").val();
        
        if (name != null && name != "" && version != null && version != "") {
            addModule(name, version);
            $("#peregrine-module-name").val("");
            $("#peregrine-module-version").val("");
            return true;
        } 
        
        return false;
    };
    
    module.removeModule = function (id) {
        $.ajax({
            type: "POST",
            url: '/admin/remove-module',
            data: {
                id: id,
                _csrf: token
            },
            error: function () {
                console.log("error");
            },
            success: function () {
                $("#modules").DataTable().ajax.reload();
            }
        });
    };
    
    module.cancelJob = function (id) {
        basicGet("/cancel-job?id=" + id, function (data) {
            console.log(data);
        }, token, header);
        
    };

    return module;
})();
