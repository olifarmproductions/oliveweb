/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */
package com.olifarm.olive.web.controller;

import com.olifarm.olive.user.User;
import com.olifarm.olive.web.job.application.CliApplicationService;
import com.olifarm.olive.web.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;

/**
 * User controller class.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
@Controller
public class UserController {

    /**
     * User service.
     */
    @Autowired
    private UserService userService;

    /**
     * Logger.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * Handle for message source.
     */
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CliApplicationService applicationService;

    /**
     * Resolves the login view.
     *
     * @return the login view
     */
    @RequestMapping("/login")
    public String login() {
        return "public/login";
    }

    /**
     * Resolves the generate account form.
     *
     * @param user User on the view
     * @return generate account view
     */
    @RequestMapping(value = "/generate-account", method = RequestMethod.GET)
    public String showGenerateAccount(User user) {
        return "public/generate_account";
    }

    /**
     * Post API for a new account.
     * Validates the account and return the form if validation fails.
     * Generates the account when validation succeeds
     *
     * @param user   User object to be stored in datasource
     * @param result Validation results
     * @param attr   redirect attribute with status message
     * @return the appropriate view
     */
    @RequestMapping(value = "/generate-account", method = RequestMethod.POST)
    public String doGenerateAccount(@Validated({User.ValidationBasic.class, User.ValidationPassword.class}) User user,
                                    BindingResult result,
                                    RedirectAttributes attr) {

        if (result.hasErrors()) {
            return "public/generate_account";
        }

        if (userService.exists(user.getUsername())) {
            result.rejectValue("username", "DuplicateKey.user.username");
            return "public/generate_account";
        }

        if (userService.existsEmail(user.getEmail())) {
            result.rejectValue("email", "DuplicateKey.user.email");
            return "public/generate_account";
        }

        userService.createUser(user);
        attr.addFlashAttribute("message", "Account created, please log in");

        return "redirect:/login";
    }

    /**
     * Resolves the view for the user settings.
     *
     * @param principal user object on session
     * @param model     the model
     * @return the setting view
     */
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    @PreAuthorize("authenticated")
    public String showSettings(Principal principal, Model model) {

        model.addAttribute("apps", applicationService.getApps());
        model.addAttribute("user", userService.getUser(principal.getName()));

        return "authenticated/settings";
    }

    /**
     * Post API for settings form.
     * Validates the settings
     *
     * @param user      user to be changed
     * @param result    validation results
     * @param principal user object on session
     * @param attr      redirect attributes with status message
     * @return the settings view
     */
    @RequestMapping(value = "/settings", method = RequestMethod.POST)
    public String changeSettings(@Validated(User.ValidationBasic.class) User user,
                                 BindingResult result, Principal principal,
                                 RedirectAttributes attr) {

        if (result.hasErrors()) {
            return "authenticated/settings";
        }

        // Get username form principal for security
        user.setUsername(principal.getName());

        if (userService.updateUser(user)) {
            attr.addFlashAttribute("message", "Account updated");
        }

        return "redirect:/settings";
    }

    /**
     * Post API for the change password form
     *
     * @param user      user object with password to be changed
     * @param result    validation results
     * @param principal user object on session
     * @param attr      redirect attributes with status message
     * @return the settings view
     */
    @RequestMapping(value = "/change-password", method = RequestMethod.POST)
    public String changePasswordPost(@Validated(User.ValidationPassword.class) User user,
                                 BindingResult result, Principal principal,
                                 RedirectAttributes attr) {

        if (result.hasErrors()) {
            return "authenticated/settings";
        }

        // Get username form principal for security
        user.setUsername(principal.getName());

        if (userService.changePassword(user)) {
            attr.addFlashAttribute("message", "Password changed");
        }

        return "redirect:/settings";
    }

    /**
     * Change password post public string.
     *
     * @param user      the user
     * @param result    the result
     * @param principal the principal
     * @param attr      the attr
     * @param model     the model
     * @return the string
     */
    @RequestMapping(value = "/change-password-public", method = RequestMethod.POST)
    public String changePasswordPostPublic(@Validated({User.ValidationPassword.class, User.ValidationRecover.class}) User user,
                                     BindingResult result, Principal principal,
                                     RedirectAttributes attr, Model model) {

        if (result.hasErrors()) {
            return "public/reset_password";
        }

        User userFromDb = userService.getUserById(user.getId());

        if (validateToken(userFromDb, user)) {
            userFromDb.setPassword(user.getPassword());
            if (userService.changePassword(userFromDb)) {
                attr.addFlashAttribute("message", "Password changed");
                userService.resetToken(user);
            }
            return "redirect:/login";
        } else {
            model.addAttribute("resetInvalid", true);
            return "public/reset_password";
        }
    }

    /**
     * Change password string.
     *
     * @param model  the model
     * @param user   the user
     * @param result the result
     * @return the string
     */
    @RequestMapping(value = "/change-password", method = RequestMethod.GET)
    public String changePassword(Model model, @Validated(User.ValidationRecover.class) User user, BindingResult result) {

        try {
            if (!result.hasErrors()) {
                User userFromDb = userService.getUserById(user.getId());

                if (validateToken(userFromDb, user)) {
                    model.addAttribute("passwordToken", user.getPasswordToken());
                    model.addAttribute("id", user.getId());
                    return "public/reset_password";

                } else {
                    model.addAttribute("resetInvalid", true);
                    return "public/recover_password";
                }

            } else {
                model.addAttribute("resetInvalid", true);
                return "public/recover_password";
            }

        } catch (EmptyResultDataAccessException e) {
            model.addAttribute("noEmail", true);
            return "public/recover_password";

        } catch (NullPointerException e) {
            model.addAttribute("resetInvalid", true);
            return "public/recover_password";
        }
    }

    private static boolean validateToken(User one, User two) {
        // Adds one day to the timestamp
        Date timestamp = one.getTimestamp();

        boolean timeMatch = dateBeforeCurrentPlusOneDay(timestamp);
        boolean tokenMatch = two.getPasswordToken().equals(one.getPasswordToken());

        if (timeMatch && tokenMatch) {
            return true;
        }
        return false;
    }

    private static boolean dateBeforeCurrentPlusOneDay(Date date) {
        Calendar calender = Calendar.getInstance();
        calender.setTime(date);
        calender.add(Calendar.DATE, 1);

        return new Date().before(calender.getTime());
    }

    /**
     * Change Peregrine credentials
     *
     * @param user      user object with pregrine credentials to be changed
     * @param principal user object on session
     * @param attr      redirect attributes with status message
     * @return the settings view
     */
    @RequestMapping(value = "/change-peregrine", method = RequestMethod.POST)
    public String changePeregrine(User user, Principal principal, RedirectAttributes attr) {

        // Get username form principal for security
        user.setUsername(principal.getName());

        if (userService.changePeregrineUsername(user)) {
            Object[] properties = new Object[]{ user.getPeregrineUsername() };

            String message = messageSource.getMessage("UserController.changedPeregrine", properties, null);
            attr.addFlashAttribute("message", message);
            logger.info(message);

        } else {
            attr.addFlashAttribute("message", messageSource.getMessage("UserController.changedPeregrine.fail", null, null));
        }

        return "redirect:/settings";
    }

    /**
     * Recover password string.
     *
     * @return the string
     */
    @RequestMapping("/recover-password")
    public String recoverPassword() {
        return "public/recover_password";
    }

    /**
     * Recover password post string.
     *
     * @param model         the model
     * @param email         the email
     * @param bindingResult the binding result
     * @param request       the request
     * @return the string
     */
    @RequestMapping(value = "/recover-password", method = RequestMethod.POST)
    public String recoverPasswordPost(Model model,
                                      @Validated({User.ValidationEmail.class}) User email,
                                      BindingResult bindingResult,
                                      HttpServletRequest request) {

        if (!bindingResult.hasErrors()) {
            try {
                User user = userService.getUserByEmail(email.getEmail());
                if (user == null) {
                    model.addAttribute("noEmail", true);
                    return "public/recover_password";
                }
                String baseUrl = "http://" + request.getServerName()
                        + ":" + request.getServerPort()
                        + request.getContextPath();

                userService.sendToken(user, baseUrl);
                model.addAttribute("recoveryEmailSent", true);
                return "public/login";

            } catch (EmptyResultDataAccessException e) {
                model.addAttribute("noEmail", true);
                return "public/recover_password";
            }
        } else {
            model.addAttribute("invalidEmail", true);
            return "public/recover_password";
        }
    }
}
