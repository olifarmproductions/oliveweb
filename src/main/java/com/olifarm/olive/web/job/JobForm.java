/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobGroup;
import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.web.job.application.CliApplicationForm;

/**
 * Created by hbrugge on 24-2-16.
 */
public class JobForm extends Job {

    private CliApplicationForm app;

    private RunType runType;


    @Override
    public CliApplicationForm getApp() {
        return app;
    }


    /**
     * Sets app.
     *
     * @param app the app
     */
    public void setApp(CliApplicationForm app) {
        this.app = app;
    }

    public int getApplicationId() {
        return this.app.getId();
    }

    /**
     * Gets run type.
     *
     * @return the run type
     */
    public RunType getRunType() {
        return runType;
    }

    /**
     * Sets run type.
     *
     * @param runType the run type
     */
    public void setRunType(RunType runType) {
        this.runType = runType;
    }


}
