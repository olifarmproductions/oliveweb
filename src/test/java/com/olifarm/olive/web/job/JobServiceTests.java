/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job;


import com.olifarm.olive.job.Job;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.TestBase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.Repeat;

/**
 * The type Job service tests.
 */
public class JobServiceTests extends TestBase {


    @Autowired
    private JobService jobService;


    private User validUser;

    /**
     * Setup.
     */
    @Before
    public void setup(){
        User user = new User();
        user.setUsername("finch");
        user.setId(2);
        validUser = user;
    }

    /**
     * Gets job sunny.
     */
    @Test
    @Repeat(10)
    public void getJobSunny() {

        Job result = jobService.getJob(1, validUser);

        Job expected = new Job();
        expected.setId(1);

        Assert.assertEquals(result, expected);

    }

    /**
     * Get job not in database.
     */
    @Test(expected = EmptyResultDataAccessException.class)
    public void getJobNotInDatabase(){
        // Since the data is stricly controlled we know Integer.MAX_VALUE is not in the db
        Job result = jobService.getJob(Integer.MAX_VALUE, validUser);

    }

    /**
     * Gets job id null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void getJobIdNull()  {
        Job result = jobService.getJob(0, validUser);
    }

    /**
     * Gets job id negative.
     */
    @Test(expected = IllegalArgumentException.class)
    public void getJobIdNegative()  {
        Job result = jobService.getJob(Integer.MIN_VALUE, validUser);
    }


    /**
     * Get job user null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void getJobUserNull(){

        Job result = jobService.getJob(0, null);

    }

    /**
     * Get job user empty.
     */
    @Test(expected = IllegalArgumentException.class)
    public void getJobUserEmpty(){

        Job result = jobService.getJob(0, new User());

    }





}
