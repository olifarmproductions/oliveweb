/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */
package com.olifarm.olive.web.user;

import com.olifarm.olive.user.User;
import com.olifarm.olive.user.UserDao;
import com.olifarm.olive.web.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.UUID;

/**
 * User service.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
@Service
public class UserService {

    /**
     * User data access object.
     */
    @Autowired
    private UserDao userDao;

    @Autowired
    private MailService mailService;

    /**
     * The password encoder.
     */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Encodes the password before creating the user.
     *
     * @param user the user object
     * @return true if succeeded
     */
    public boolean createUser(User user) {
        // encode the password
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        // enable user by default
        user.setEnabled(true);
        user.setAuthority("ROLE_USER");

        return userDao.create(user);
    }

    /**
     * Updates the user credentials,
     * Username comes from session object en user id is gotten from database
     *
     * @param user user to be updated
     * @return true if succeeded
     */
    public boolean updateUser(User user) {
        // id retrieved from db for security
        user.setId(userDao.getId(user.getUsername()));

        return userDao.updateUser(user);
    }

    /**
     * Encoded and changes the password in datasource.
     *
     * @param user user object with changed password
     * @return true if succeeded
     */
    public boolean changePassword(User user) {

        // id retrieved from db for security
        user.setId(userDao.getId(user.getUsername()));
        // encode the password
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return userDao.changePassword(user);
    }

    /**
     * Get user by username.
     *
     * @param username the username
     * @return the user
     */
    public User getUser(String username) {
        return userDao.getUser(username);
    }

    /**
     * Gets user by id.
     *
     * @param id the id
     * @return the user by id
     */
    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    /**
     * Gets user by email.
     *
     * @param email the email
     * @return the user by email
     */
    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    /**
     * Checks if the user exists in the datasource.
     *
     * @param username the username
     * @return true if exists
     */
    public boolean exists(String username) {
        return userDao.exists(username);
    }

    /**
     * Check is an email address exists in the datasource.
     *
     * @param email the email address
     * @return true if exists
     */
    public boolean existsEmail(String email) {
        return userDao.existsEmail(email);
    }

    /**
     * Change peregrine username boolean.
     *
     * @param user the user
     * @return the boolean
     */
    public boolean changePeregrineUsername(User user) {
        // id retrieved from db for security
        user.setId(userDao.getId(user.getUsername()));

        return userDao.changePeregrineUsername(user);
    }

    /**
     * Send token boolean.
     *
     * @param user    the user
     * @param baseUrl the base url
     * @return the boolean
     */
    public boolean sendToken(User user, String baseUrl) {

        String passwordToken = UUID.randomUUID().toString();
        user.setPasswordToken(passwordToken);
        user.setTimestamp(new Date());

        userDao.setToken(user);
        userDao.setTimestamp(user);

        String link = baseUrl + "/change-password?id=" + user.getId() + "&passwordToken=" + passwordToken;
        return mailService.sendPasswordResetLink(user, link);
    }

    /**
     * Reset token boolean.
     *
     * @param user the user
     * @return the boolean
     */
    public boolean resetToken(User user) {
        user.setPasswordToken(null);
        user.setTimestamp(null);
        userDao.setToken(user);
        userDao.setTimestamp(user);

        return true;
    }

}
