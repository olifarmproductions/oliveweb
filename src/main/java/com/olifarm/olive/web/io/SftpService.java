/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.io;

import com.jcraft.jsch.ChannelSftp;
import com.olifarm.olive.io.RemoteFileService;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import java.io.*;

/**
 * Implementation of the ftp service interface.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Service
public class SftpService implements RemoteFileService {

    /**
     * Logger object used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * SftpSession factory used for getting new ftp sessions.
     */
    @Autowired
    private DefaultSftpSessionFactory defaultSftpSessionFactory;

    /**
     * SftpSession object used for interacting with the ftp server.
     */
    private Session<ChannelSftp.LsEntry> ftpSession;

    @Override
    public void open() {
        ftpSession = defaultSftpSessionFactory.getSession();
        logger.info("Opened ftp session: " + ftpSession);
    }

    @Override
    public void close() {
        ftpSession.close();
        logger.info("Closed ftp session: " + ftpSession);
        ftpSession = null;
    }

    @Override
    public void read(String inputUrl, OutputStream outputStream) throws IOException {
        ftpSession.read(inputUrl, outputStream);
    }

    @Override
    public OutputStream getFile(String inputUrl, String outputPath) throws IOException {
        OutputStream outputStream = new FileOutputStream(outputPath);
        ftpSession.read(inputUrl, outputStream);
        return outputStream;
    }

    @Override
    public void mkdir(String path) throws IOException {
        if (!this.hasDir(path)) {
            this.recursiveMkdir(path);
        }
    }

    @Override
    public boolean hasDir(String path) throws IOException {
        return ftpSession.exists(path);
    }

    @Override
    public void setFile(InputStream inputStream, String path) throws IOException {
        ftpSession.write(inputStream, path);
        inputStream.close();
    }

    /**
     * Copy a file from to the ftp server. Not the most ideal implementation
     * but the best we could achieve since the ftp standard does not support
     * copy operations.
     *
     * @param source the source path
     * @param dest the destination path
     * @throws IOException the IOException
     */
    @Override
    public void copy(String source, String dest) throws IOException {

        File tempFile = File.createTempFile("temp-copy-file", ".tmp");
        OutputStream outputStream = new FileOutputStream(tempFile);

        ftpSession.read(source, outputStream);
        outputStream.flush();
        outputStream.close();

        InputStream inputStream = new FileInputStream(tempFile);
        ftpSession.write(inputStream, dest);
        inputStream.close();
    }

    /**
     * Gets ftp session.
     *
     * @return the ftp session
     */
    public Session<ChannelSftp.LsEntry> getFtpSession() {
        return ftpSession;
    }

    /**
     * Sets ftp session.
     *
     * @param ftpSession the ftp session
     */
    public void setFtpSession(Session<ChannelSftp.LsEntry> ftpSession) {
        this.ftpSession = ftpSession;
    }

    private void recursiveMkdir(String path) throws IOException {

        String[] pathElements = path.split("/");
        String curDir = "";

        if (pathElements.length > 0) {

            for (String singleDir : pathElements) {

                // check for home root
                if (singleDir.equals("~")) {
                    curDir = "~";
                } else {
                    curDir = curDir + "/" + singleDir;

                    if (!ftpSession.exists(curDir)) {
                        if (ftpSession.mkdir(curDir)) {
                            logger.info("Created directory: " + curDir);
                        } else {
                            logger.warn("Could not create directory: " + curDir);
                            break;
                        }
                    }
                }
            }
        }
    }
}
