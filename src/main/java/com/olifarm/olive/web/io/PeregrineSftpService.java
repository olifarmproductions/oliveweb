/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.io;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.sftp.session.SftpSession;

/**
 * Created by obbakker on 20-4-16.
 */
public class PeregrineSftpService extends SftpService {

    private final Log logger = LogFactory.getLog(this.getClass());


    /**
     * Instantiates a new Peregrine sftp service.
     */
    public PeregrineSftpService() {
    }

    /**
     * Open.
     *
     * @param jschSession the jsch session
     * @throws JSchException the j sch exception
     */
    public void open(Session jschSession) throws JSchException {
        ConnectableSftpSession connectableSftpSession = new ConnectableSftpSession(jschSession);
        connectableSftpSession.connect();
        this.setFtpSession(connectableSftpSession);
        this.open();
    }

    /**
     * Open.
     *
     * @param sftpSession the sftp session
     */
    public void open(SftpSession sftpSession) {
        this.setFtpSession(sftpSession);
        this.open();
    }

    @Override
    public void open() {
        logger.info("Pergerine session: " + this.getFtpSession().isOpen());
    }



}
