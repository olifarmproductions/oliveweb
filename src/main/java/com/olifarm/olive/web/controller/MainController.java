/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.Utils;
import com.olifarm.olive.web.job.JobService;
import com.olifarm.olive.web.job.application.CliApplicationService;
import com.olifarm.olive.web.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

/**
 * Controller class for resolving the main views.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Controller
public class MainController {

    @Autowired
    private CliApplicationService applicationService;

    @Autowired
    private JobService jobService;

    @Autowired
    private UserService userService;

    /**
     * Resolves the main page view.
     *
     * @param model     the model
     * @param principal the principal
     * @return the view
     * @throws Exception the exception
     */
    @RequestMapping("/olive")
    @PreAuthorize("authenticated")
    public String showOliveInterface(Model model, Principal principal) throws Exception {

        User user = userService.getUser(principal.getName());
        List<Job> jobs = jobService.getJobs(user);

        model.addAttribute("apps", applicationService.getApps());
        model.addAttribute("jobs", jobs);
        model.addAttribute("jobBatchInfo", Utils.getJobInfo(jobs));

        return "authenticated/olive_main";
    }

    /**
     * Resolves the profile view.
     *
     * @param model the model
     * @return the view
     */
    @RequestMapping("/profile")
    @PreAuthorize("authenticated")
    public String showProfile(Model model) {
        model.addAttribute("apps", applicationService.getApps());
        return "authenticated/profile";
    }

    /**
     * Resolves the notification view.
     *
     * @param model the model
     * @return the view
     */
    @RequestMapping("/notifications")
    @PreAuthorize("authenticated")
    public String showNotifications(Model model) {
        model.addAttribute("apps", applicationService.getApps());

        return "authenticated/notifications";
    }
}
