/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.olifarm.olive.web.job.application.CliApplicationService;
import com.olifarm.olive.web.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Controller that handles requests relating to email.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Controller
public class MailController {


    @Autowired
    private MailService mailService;

    @Autowired
    private CliApplicationService applicationService;

    /**
     * Report issue post string.
     *
     * @param principal the principal
     * @param type      the type
     * @param title     the title
     * @param content   the content
     * @return the string
     */
    @RequestMapping(value = "/report-issue", method = RequestMethod.POST)
    @PreAuthorize("authenticated")
    public String reportIssuePost(Principal principal, String type, String title, String content) {

        mailService.reportIssue(principal.getName(), type, title, content);

        return "redirect:/olive";
    }

    /**
     * Report issue get string.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/report-issue", method = RequestMethod.GET)
    @PreAuthorize("authenticated")
    public String reportIssueGet(Model model) {

        model.addAttribute("apps", applicationService.getApps());

        return "authenticated/report_issue";
    }
}