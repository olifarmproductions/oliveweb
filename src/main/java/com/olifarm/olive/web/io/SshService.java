/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.io;

import com.jcraft.jsch.*;
import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobDao;
import com.olifarm.olive.job.JobStatusPeregrine;
import com.olifarm.olive.job.cliapplication.CliApplicationDao;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.Utils;
import com.olifarm.olive.web.job.application.FileParameter;
import com.olifarm.olive.web.job.peregrine.JobFormPeregrine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.ConnectionClosedException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Ssh service.
 */
@Service
public class SshService {


    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * The hostname of peregrine.
     */
    @Value("${peregrine.ssh.host}")
    private String host;

    /**
     * The ssh port of peregrine.
     */
    @Value("${peregrine.ssh.port}")
    private int port;

    @Value("${peregrine.custom.tools.home}")
    private String customToolsHome;

    @Value("#{'${peregrine.custom.tools}'.split(',')}")
    private List<String> customTools;

    /**
     * The connection timeout in miliseconds.
     */
    @Value("${peregrine.ssh.timeout}")
    private int timeout;

    @Autowired
    @Qualifier("nameServicePeregrine")
    private PeregrineNameService nameService;

    @Autowired
    private CliApplicationDao applicationDao;

    @Autowired
    private JobDao jobDao;

    /**
     * Get a new ssh session for a user
     *
     * @param user the user
     * @return session session
     * @throws JSchException the j sch exception
     */
    public Session getSession(User user) throws JSchException {

        JSch jsch = new JSch();

        Session session = jsch.getSession(user.getPeregrineUsername(), host, port);
        session.setPassword(user.getPeregrinePassword());
        user.setPeregrinePassword(null);

        // TODO: FOR TESTING ONLY, NEEDS TO ADDED TO HOSTS FILE
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);

        return session;
    }

    /**
     * Execute a job script on peregrine
     *
     * @param job     the job
     * @param session the session
     * @return the peregrine id
     * @throws JSchException         the j sch exception
     * @throws IOException           the io exception
     * @throws SftpException         the sftp exception
     * @throws IllegalStateException the illegal state exception
     */
    public int executePeregrineJob(JobFormPeregrine job, Session session)
            throws JSchException, IOException, SftpException, IllegalStateException{

        job.setModules(applicationDao.getPeregrineModulesByAppId(job.getApplicationId()));

        PeregrineSftpService peregrineSftpService = new PeregrineSftpService();
        peregrineSftpService.open(session);

        if (job.getApp().getParameters() == null) {
            job.getApp().setParameters(new ArrayList<Parameter>());
        }

        String remoteInputDir = nameService.makeRemoteInputDir(job);
        peregrineSftpService.mkdir(remoteInputDir);

        int fileNr = 0;


        for (FileParameter fileParameter : job.getApp().getFileParameters()) {

            boolean fileSet = false;
            String fileName = null;
            String filePath = null;
            MultipartFile file = fileParameter.getFile();

            if (file != null && file.getSize() != 0) {
                fileName = nameService.createFilename(job, fileParameter.getExtensionFromFilename(), fileNr);
                filePath = remoteInputDir + "/" + fileName;
                peregrineSftpService.setFile(file.getInputStream(), filePath);
                fileSet = true;

            } else if (fileParameter.getArguments() != null) {
                fileName = nameService.createFilename(job, Utils.getExtensionFromFilename(fileParameter.getFileNameFromPath()), fileNr);
                filePath = remoteInputDir + "/" + fileName;
                peregrineSftpService.copy(fileParameter.getArguments().get(0), filePath);
                fileSet = true;
            }

            if (fileSet) {

                fileParameter.setArguments(Collections.singletonList(filePath));
                fileParameter.setRemoteFilePath(filePath);
                fileParameter.setFilename(fileName);

                Parameter parameter = new Parameter();
                BeanUtils.copyProperties(fileParameter, parameter);

                // Add the file parameter to parameters
                job.getApp().getParameters().add(parameter);
                fileNr++;
            }
        }

        String outputFolder = nameService.makeRemoteOutputDir(job);

        peregrineSftpService.mkdir(outputFolder);
        peregrineSftpService.close();

        // Build the command
        String filename = outputFolder + "/slurm-" + job.getId() + ".sh";


        String command = "echo \"" + job.createJobScript(customTools, customToolsHome) + "\" > " + filename
                + " && cd " + outputFolder
                + " && sbatch --partition=" + job.getPartition().getCommand() + " " + filename;

        String result = executeSshCommand(session, command);

        try {
            String number = result.substring(20);
            number = number.substring(0, number.length() - 1);
            return Integer.parseInt(number);

        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            logger.warn(result);
            return 0;
        }

    }

    /**
     * Get the status of a job on peregrine using jobinfo.
     *
     * @param session        the session
     * @param peregrineJobId the peregrine job id
     * @return the status
     * @throws IOException           the io exception
     * @throws JSchException         the j sch exception
     * @throws IllegalStateException the illegal state exception
     */
    public JobStatusPeregrine getStatus(Session session, int peregrineJobId) throws IOException, JSchException, IllegalStateException {

        String command = "jobinfo " + peregrineJobId;
        String result = executeSshCommand(session, command);
        Pattern pattern = Pattern.compile(".*State\\s*:[\\s,]{0,2}([A-Z_]*).*", Pattern.DOTALL);

        Matcher m = pattern.matcher(result);
        if (m.find() && !m.group(1).equals("") && m.group(1) != null) {
            return JobStatusPeregrine.valueOf(m.group(1));
        } else {
            return JobStatusPeregrine.UNKNOWN;
        }

//        throw new IllegalStateException("Peregrine status could not be resolved");
    }

    /**
     * Cancel a job on peregrine using scancel.
     *
     * @param session        the session
     * @param peregrineJobId the peregrine job id
     * @return the boolean
     * @throws IOException           the io exception
     * @throws JSchException         the j sch exception
     * @throws IllegalStateException the illegal state exception
     */
    public boolean cancelJob(Session session, int peregrineJobId) throws IOException, JSchException, IllegalStateException {

        String command = "scancel " + peregrineJobId;
        String result = executeSshCommand(session, command);

        return true;
    }

    private String executeSshCommand(Session session, String command) throws JSchException, IOException , IllegalStateException {

        // Open an execute channel
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);
        InputStream commandOutput = channel.getInputStream();

        if (!channel.isClosed()) {
            // TODO: look into error with opening channel. Possible solution catch and force peregrine reconnect.
            // Connect to the channel
            channel.connect();

            // Build a buffer to catch the output
            StringBuilder outputBuffer = new StringBuilder();
            int readByte = commandOutput.read();

            while (readByte != 0xffffffff) {
                outputBuffer.append((char) readByte);
                readByte = commandOutput.read();
            }

            // Disconnect the channel and the session
            channel.disconnect();

            // Return the output from the ssh
            return outputBuffer.toString();
        } else {
            throw new IllegalStateException("Execute channel is closed");
        }

    }

    /**
     * Zip output folder.
     *
     * @param job     the job
     * @param session the session
     * @throws JSchException         the j sch exception
     * @throws IOException           the io exception
     * @throws IllegalStateException the illegal state exception
     */
    public void zipOutputFolder(Job job, Session session) throws JSchException, IOException, IllegalStateException {

        User user = job.getUser();
        user.setPeregrineUsername(session.getUserName());
        job.setUser(user);

        String command = "cd " + nameService.makeRemoteOutputDir(job)
                + " && zip -r "
                + nameService.createFilename(job, "out.zip", 0)
                + " *";

        executeSshCommand(session, command);
    }

    /**
     * Make ssh session session.
     *
     * @param peregrineSession the peregrine session
     * @return the session
     * @throws PeregrineSessionAbsentException the peregrine session absent exception
     * @throws JSchException                   the j sch exception
     */
    public static Session makeSshSession(PeregrineSession peregrineSession) throws PeregrineSessionAbsentException, JSchException {
        Session session = peregrineSession.getSession();

        if (session != null) {

            if (!session.isConnected()) {
                session.connect();
            }
            return session;

        } else {
            throw new PeregrineSessionAbsentException("Peregrine session is not set");
        }
    }
}
