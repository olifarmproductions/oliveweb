/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.olifarm.olive.user.UserDao;
import com.olifarm.olive.web.notifications.notification.Notification;
import com.olifarm.olive.web.notifications.notification.NotificationDao;
import com.olifarm.olive.web.notifications.notification.TimedNotificationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.text.ParseException;
import java.util.List;

/**
 * REST controller that handles the retrieving of notification objects.
 * They are retreived form the database and sent to the user using http.
 *
 * @author Olivier
 */
@RestController
public class NotificationController {

    /**
     * Object implementing {@link NotificationDao} autowired by spring.
     */
    @Autowired
    private NotificationDao notificationDao;

    /**
     * Object implementing {@link UserDao} autowired by spring.
     */
    @Autowired
    private UserDao userDao;

    /**
     * Retrieve notifications from the database using an {@link TimedNotificationRequest} object.
     *
     * @param notificationRequest the {@link TimedNotificationRequest}.
     * @param bindingResult       the result of the validation of the {@link TimedNotificationRequest}.
     * @param principal           {@link Principal} object used for getting user data.
     * @return A list of {@link Notification} objects
     * @throws ParseException the parse exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/data/get-notifications", method = RequestMethod.POST)
    public List<Notification> getNotifications(@Valid TimedNotificationRequest notificationRequest, BindingResult bindingResult, Principal principal) throws ParseException {

        notificationRequest.setUserId(userDao.getId(principal.getName()));
        return notificationDao.getMessages(notificationRequest);
    }
}
