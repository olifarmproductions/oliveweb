/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.peregrine;

/**
 * Enum with partition types for Peregrine
 *
 * @author Harm Brugge
 */
public enum Partition {

    /**
     * Standard 24 core, 128 GB memory nodes, time limit: 10 days.
     */
    NODES("nodes"),

    /**
     * Standard nodes, but only short jobs, time limit: 30 minutes.
     */
    SHORT("short"),

    /**
     * Nvidia GPU nodes, time limit: 3 days.
     */
    GPU("gpu"),

    /**
     * Intel Xeon Phi nodes, time limit: 3 days.
     */
    PHI("phi"),

    /**
     * Big memory nodes, time limit: 10 days.
     */
    HIMEM("himem");

    private String command;

    private Partition(String command) {
        this.command = command;
    }

    /**
     * Gets command.
     *
     * @return the command
     */
    public String getCommand() {
        return this.command;
    }

}
