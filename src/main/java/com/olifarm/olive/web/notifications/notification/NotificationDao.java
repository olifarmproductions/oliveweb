/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.notifications.notification;

import java.text.ParseException;
import java.util.List;

/**
 * Interface detailling the methods of an NotificationDao.
 *
 * @author Olivier
 */
public interface NotificationDao {

    /**
     * Get notification from the database.
     *
     * @param nfg {@link TimedNotificationRequest} to select the notifications
     * @return {@link List} of {@link Notification} objects in JSON format
     * @throws ParseException if the date cant be parsed
     */
    List<Notification> getMessages(TimedNotificationRequest nfg) throws ParseException;

    /**
     * Add a {@link Notification} to the database.
     *
     * @param notification the {@link Notification} object to add.
     */
    void setNotification(Notification notification);


}
