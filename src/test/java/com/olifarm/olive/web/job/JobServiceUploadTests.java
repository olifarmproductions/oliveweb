/*
 *  OliveWeb is the webinterface for the OliveProject.
 *  Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 */

package com.olifarm.olive.web.job;

import com.olifarm.olive.io.RemoteFileService;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.TestBase;
import com.olifarm.olive.web.job.application.CliApplicationForm;
import com.olifarm.olive.web.job.application.FileParameter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * The type Job service upload tests.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
public class JobServiceUploadTests extends TestBase {

    @Autowired
    private JobService jobService;

    @Autowired
    private RemoteFileService remoteFileService;

    private MultipartFile validMultiPartFile;


    /**
     * Sets .
     */
    @Before
    public void setup() {
        byte[] bytes = new byte[20];
        new Random().nextBytes(bytes);

        validMultiPartFile = new MockMultipartFile("Test", "test.random", "text", bytes);
    }

    /**
     * Upload files test sunny.
     *
     * @throws IOException the io exception
     */
    @Test
    public void uploadFilesTestSunny() throws IOException {

        JobForm job = new JobForm();
        CliApplicationForm application = new CliApplicationForm();
        application.setParameters(new ArrayList());
        application.setName("testApplication");
        job.setApp(application);
        User user = new User();
        user.setUsername("testUser");
        job.setUser(user);

        FileParameter parameter = new FileParameter();
        parameter.setFile(validMultiPartFile);

        job.getApp().setFileParameters(Collections.singletonList(parameter));
        jobService.uploadFiles(job);

        FileParameter uploadedFileParam = job.getApp().getFileParameters().get(0);

        ByteArrayOutputStream resultOutputStream = new ByteArrayOutputStream();

        remoteFileService.open();
        remoteFileService.read(uploadedFileParam.getRemoteFilePath(), resultOutputStream);
        remoteFileService.close();

        Assert.assertArrayEquals(resultOutputStream.toByteArray(), validMultiPartFile.getBytes());

    }

    /**
     * Upload empty file.
     */
    public void uploadEmptyFile() {

    }

}
