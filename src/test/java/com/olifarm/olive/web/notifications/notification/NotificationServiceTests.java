package com.olifarm.olive.web.notifications.notification;


import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobStatus;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.TestBase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * The type Notification service tests.
 */
public class NotificationServiceTests extends TestBase {

    private Notification validNotification;

    private Job validJob;

    private String validEvent;

    /**
     * Sets .
     *
     * @throws ParseException the parse exception
     */
    @Before
    public void setup() throws ParseException {

        validJob = new Job();
        validJob.setId(1);
        validJob.setStatus(JobStatus.PROCESSING);
        validJob.setUser(new User(){{setId(2);}});
        validJob.setUsersJobId(1);


        validEvent = "test event";

        validNotification = new Notification();
        validNotification.setUserId(validJob.getUserId());
        validNotification.setTitle("Job " + validJob.getUsersJobId() + " " + validEvent);
        validNotification.setNotification("Job name: " + validJob.getName() +
                " with id: " + validJob.getId() +
                " had status: " + validJob.getStatus().toString() +
                " when " +
                validEvent);
        validNotification.setType(NotificationType.INFO);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        validNotification.setDateTime(formatter.parse("12/12/1212"));

    }

    /**
     * Create job event notification test sunny.
     */
    @Test
    public void createJobEventNotificationTestSunny(){

        Notification result = NotificationService.createJobEventNotification(validJob, validEvent);

        Assert.assertEquals(result, validNotification);
    }

    /**
     * Create job event notification test event empty.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createJobEventNotificationTestEventEmpty(){

        Notification result = NotificationService.createJobEventNotification(validJob, "");
    }

    /**
     * Create job event notification test event null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createJobEventNotificationTestEventNull(){

        Notification result = NotificationService.createJobEventNotification(validJob, null);
    }


    /**
     * Create job event notification test job null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createJobEventNotificationTestJobNull(){

        Notification result = NotificationService.createJobEventNotification(null, validEvent);
    }

    /**
     * Create job event notification job test invalid.
     */
    @Test(expected = IllegalArgumentException.class)
    public void createJobEventNotificationJobTestInvalid(){

        Job invalidJob = validJob;
        invalidJob.setId(Integer.MIN_VALUE);
        invalidJob.setUser(null);
        Notification result = NotificationService.createJobEventNotification(null, validEvent);
    }
}
