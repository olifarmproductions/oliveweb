/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.notifications.notification;

import com.olifarm.olive.job.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Service used for creating notifications.
 *
 * @author Olivier Bakker
 * @author Harm Brugge
 */
@Service
public class NotificationService {

    /**
     * NotificationDao used for adding notifications to the database.
     */
    @Autowired
    private NotificationDao notificationDao;

    /**
     * Add a notification to the database.
     *
     * @param notification the notification.
     */
    public void setNotification(Notification notification){
        notificationDao.setNotification(notification);
    }

    /**
     * Create a new notification.
     *
     * @param job   the job
     * @param event a string containing the message
     * @return the notification
     */
    public static Notification createJobEventNotification(Job job, String event) {

        if (event == null || event.equals("")) {
            throw new IllegalArgumentException("Event may no be null or empty");
        }
        if (job == null || job.getUser() == null || job.getId() <= 0 || job.getUsersJobId() <= 0 || job.getStatus() == null) {
            throw new IllegalArgumentException("Job is invalid");
        }

        Notification notification = new Notification();
        notification.setUserId(job.getUserId());
        notification.setTitle("Job " + job.getUsersJobId() + " " + event);
        notification.setNotification("Job name: " + job.getName()
                + " with id: " + job.getId()
                + " had status: " + job.getStatus().toString()
                + " when "
                + event);
        notification.setType(NotificationType.INFO);
        notification.setDateTime(new Date());

        return notification;

    }

    /**
     * Create job error notification notification.
     *
     * @param job   the job
     * @param event the event
     * @return the notification
     */
    public static Notification createJobErrorNotification(Job job, String event) {

        Notification notification = new Notification();
        notification.setUserId(job.getUserId());
        notification.setTitle("Job " + job.getUsersJobId() + " " + event);
        notification.setNotification("Job name: " + job.getName()
                + " with id: " + job.getId()
                + " had status: " + job.getStatus().toString()
                + " when "
                + event);
        notification.setType(NotificationType.ERROR);
        notification.setDateTime(new Date());

        return notification;

    }
}

