/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.peregrine;

import com.olifarm.olive.job.cliapplication.Module;
import com.olifarm.olive.web.job.JobForm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for mapping to form data for a Peregrine job.
 *
 * @author Harm Brugge
 */
public class JobFormPeregrine extends JobForm {

    /**
     * Modules required for a job to run on Peregrine.
     * String must include version number like expected after the "module load" command
     * in a bash run script (sbatch)
     */
    private List<Module> modules;

    /**
     * The wall clock time requirement for a job can be specified using the --time parameter.
     * The time can be specified in several formats: "minutes", "minutes:seconds",
     * "hours:minutes:seconds", "days-hours", "days-hours:minutes" and "days-hours:minutes:seconds".
     * A job is allowed to run only for the time that has been requested. After this time has passed
     * the job will be stopped, regardless of its state. Therefore one has to make sure that the job
     * will finish within this time.
     *
     * --time is corresponding flag
     *
     */
    private String wallClockTime;

    /**
     * Number of nodes to use for the job.
     *
     * --node is corresponding flag
     */
    private int node;

    /**
     * Total number of tasks that the job will start.
     *
     * --ntasks is corresponding flag
     * only useful if --ntasks-per-node is not used
     */
    private int numberOfTasks;

    /**
     * Number of tasks to start per node.
     *
     * --ntasks-per-node is corresponding flag
     * only useful if --ntasks is not used
     */
    private int numberOfTasksPerNode;

    /**
     * Number of threads per task.
     *
     * --cpus-per-task is corresponding flag
     */
    private int cpusPerTask;

    /**
     * The amount of memory needed for a job per node in megabytes.
     * You can include a suffix to use a different unit: K or KB for kilobytes,
     * M or MB for megabytes, G or GB for gigabytes, and T or TB for terabytes.
     * Make sure to not include a space between the number and the suffix
     *
     * --mem is corresponding flag
     */
    private int memory;

    /**
     * The amount of memory needed for a job per physical cpu core in megabytes.
     * You can include a suffix to use a different unit: K or KB for kilobytes,
     * M or MB for megabytes, G or GB for gigabytes, and T or TB for terabytes.
     * Make sure to not include a space between the number and the suffix
     *
     * --mep-per-cpu is corresponding flag
     */
    private int memoryPerCpu;

    /**
     * Partition to be used. determines on which node type the job will run.
     */
    private Partition partition;

    /**
     * List with mail notification types requested fo the Peregrine job.
     *
     * @see MailType
     */
    private List<MailType> mailTypes;

    /**
     * Instantiates a new Job form peregrine.
     */
    public JobFormPeregrine() {
        modules = new ArrayList<>();
    }


    /**
     * Creates a complete bash job script which can be run started on Peregrine
     *
     * @param customTools     the custom tools
     * @param customToolsHome the custom tools home
     * @return sbtach job script
     */
    public String createJobScript(List<String> customTools, String customToolsHome) {
        String script = "#!/bin/bash\n";

        if (node != 0) {
            script += "#SBATCH --nodes=" + node + "\n";
        }

        // tasksPerNode is only useful when numberOfTasks is not set
        if (numberOfTasks != 0) {
            script += "#SBATCH --ntasks=" + numberOfTasks + "\n";
        } else if (numberOfTasksPerNode != 0) {
            script += "#SBATCH --ntasks-per-node=" + numberOfTasksPerNode + "\n";
        }

        if (cpusPerTask != 0) {
            script += "#SBATCH --cpus-per-task=" + cpusPerTask + "\n";
        }

        if (super.getName() != null) {
            script += "#SBATCH --job-name='" + super.getName() + "'\n";
            script += "#SBATCH --output=" + super.getApp().getName().toLowerCase() + "_job-" + super.getId() + ".stdout.log" + "\n";
        }

        if (wallClockTime != null && wallClockTime.equals("")) {
            script += "#SBATCH --time=" + wallClockTime + "\n";
        }

        if (memory != 0) {
            script += "#SBATCH --mem=" + memory + "\n";
        }

        if (modules != null && !modules.isEmpty()) {
            for (Module module :modules) {
                String line = "module load " + module.getVersion() + "\n";
//                String line = "module load " + module.getName() + "/" + module.getVersion();
                script += line;
            }
        }

        script += "\n";

        StringBuilder sb = new StringBuilder();
        boolean isMartinitool = false;

        for (String tool: customTools) {
            if (super.getApp().getName().toLowerCase().equals(tool.split("\\.")[0])) {
                sb.append(customToolsHome).append("/").append(tool).append(" ");
                isMartinitool = true;
                break;
            }
        }

        if (!isMartinitool) {
            sb.append(super.getApp().getName()).append(" ");
        }

        for (String param :super.getApp().paramsToClString()) {
            sb.append(param).append(" ");
        }

        script += sb.toString();

        return script;
    }

    /**
     * Gets modules.
     *
     * @return the modules
     */
    public List<Module> getModules() {
        return modules;
    }

    /**
     * Sets modules.
     *
     * @param modules the modules
     */
    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    /**
     * Gets wall clock time.
     *
     * @return the wall clock time
     */
    public String getWallClockTime() {
        return wallClockTime;
    }

    /**
     * Sets wall clock time.
     *
     * @param wallClockTime the wall clock time
     */
    public void setWallClockTime(String wallClockTime) {
        this.wallClockTime = wallClockTime;
    }

    /**
     * Gets node.
     *
     * @return the node
     */
    public int getNode() {
        return node;
    }

    /**
     * Sets node.
     *
     * @param node the node
     */
    public void setNode(int node) {
        this.node = node;
    }

    /**
     * Gets number of tasks.
     *
     * @return the number of tasks
     */
    public int getNumberOfTasks() {
        return numberOfTasks;
    }

    /**
     * Sets number of tasks.
     *
     * @param numberOfTasks the number of tasks
     */
    public void setNumberOfTasks(int numberOfTasks) {
        this.numberOfTasks = numberOfTasks;
    }

    /**
     * Gets number of tasks per node.
     *
     * @return the number of tasks per node
     */
    public int getNumberOfTasksPerNode() {
        return numberOfTasksPerNode;
    }

    /**
     * Sets number of tasks per node.
     *
     * @param numberOfTasksPerNode the number of tasks per node
     */
    public void setNumberOfTasksPerNode(int numberOfTasksPerNode) {
        this.numberOfTasksPerNode = numberOfTasksPerNode;
    }

    /**
     * Gets cpus per task.
     *
     * @return the cpus per task
     */
    public int getCpusPerTask() {
        return cpusPerTask;
    }

    /**
     * Sets cpus per task.
     *
     * @param cpusPerTask the cpus per task
     */
    public void setCpusPerTask(int cpusPerTask) {
        this.cpusPerTask = cpusPerTask;
    }

    /**
     * Gets memory.
     *
     * @return the memory
     */
    public int getMemory() {
        return memory;
    }

    /**
     * Sets memory.
     *
     * @param memory the memory
     */
    public void setMemory(int memory) {
        this.memory = memory;
    }

    /**
     * Gets memory per cpu.
     *
     * @return the memory per cpu
     */
    public int getMemoryPerCpu() {
        return memoryPerCpu;
    }

    /**
     * Sets memory per cpu.
     *
     * @param memoryPerCpu the memory per cpu
     */
    public void setMemoryPerCpu(int memoryPerCpu) {
        this.memoryPerCpu = memoryPerCpu;
    }

    /**
     * Gets mail types.
     *
     * @return the mail types
     */
    public List<MailType> getMailTypes() {
        return mailTypes;
    }

    /**
     * Sets mail types.
     *
     * @param mailTypes the mail types
     */
    public void setMailTypes(List<MailType> mailTypes) {
        this.mailTypes = mailTypes;
    }

    /**
     * Gets partition.
     *
     * @return the partition
     */
    public Partition getPartition() {
        return partition;
    }

    /**
     * Sets partition.
     *
     * @param partition the partition
     */
    public void setPartition(Partition partition) {
        this.partition = partition;
    }

    @Override
    public String toString() {
        return "JobFormPeregrine{" +
                "wallClockTime='" + wallClockTime + '\'' +
                ", node=" + node +
                ", numberOfTasks=" + numberOfTasks +
                ", numberOfTasksPerNode=" + numberOfTasksPerNode +
                ", cpusPerTask=" + cpusPerTask +
                ", memory=" + memory +
                ", memoryPerCpu=" + memoryPerCpu +
                ", partition=" + partition +
                ", mailTypes=" + mailTypes +
                '}';
    }
}
