/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.application;

import com.olifarm.olive.job.cliapplication.Parameter;
import org.springframework.web.multipart.MultipartFile;


/**
 * The type File parameter.
 */
public class FileParameter extends Parameter {

    private MultipartFile file;

    /**
     * Instantiates a new File parameter.
     */
    public FileParameter() {

    }

    /**
     * Instantiates a new File parameter.
     *
     * @param parameter the parameter
     */
    public FileParameter(Parameter parameter) {
        this.setFilename(parameter.getFilename());
        this.setId(parameter.getId());
        this.setName(parameter.getName());
        this.setFlag(parameter.getFlag());
        this.setLongFlag(parameter.isLongFlag());
        this.setType(parameter.getType());
        this.setLevel(parameter.getLevel());
        this.setArgNum(parameter.getArgNum());
        this.setArguments(parameter.getArguments());
        this.setDescription(parameter.getDescription());
        this.setExtension(parameter.getDescription());
        this.setLocalFilePath(parameter.getLocalFilePath());
        this.setRemoteFilePath(parameter.getRemoteFilePath());
    }

    /**
     * Empty arguments.
     */
    public void emptyArguments() {
        setArguments(null);
        setRemoteFilePath(null);
        setLocalFilePath(null);
        setFilename(null);
        setExtension(null);
        setFile(null);
    }

    /**
     * Gets file.
     *
     * @return the file
     */
    public MultipartFile getFile() {
        return file;
    }

    /**
     * Sets file.
     *
     * @param file the file
     */
    public void setFile(MultipartFile file) {
        this.file = file;
    }

    /**
     * Gets extension from filename.
     *
     * @return the extension from filename
     */
    public String getExtensionFromFilename() {
        String extension = "";
        int i = file.getOriginalFilename().lastIndexOf('.');
        if (i > 0) {
            extension = file.getOriginalFilename().substring(i + 1);
        }

        return extension;
    }
}
