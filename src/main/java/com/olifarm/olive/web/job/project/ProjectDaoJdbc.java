/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

package com.olifarm.olive.web.job.project;

import com.olifarm.olive.job.Project;
import com.olifarm.olive.job.ProjectDao;
import com.olifarm.olive.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * The type Project dao jdbc.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Component
public class ProjectDaoJdbc implements ProjectDao {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Override
    public List<Project> getProjects(User user) {

        return jdbc.query("SELECT * FROM projects WHERE user_id = :userId",
                new MapSqlParameterSource("userId", user.getId()),
                BeanPropertyRowMapper.newInstance(Project.class));
    }

    @Override
    public Project getProject(User user, int i) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", user.getId());
        params.addValue("id", i);

        return jdbc.queryForObject("SELECT * FROM projects WHERE user_id = :userId AND  id = :id",
                params,
                BeanPropertyRowMapper.newInstance(Project.class));
    }

    @Override
    public int addProject(Project project) {
        GeneratedKeyHolder key = new GeneratedKeyHolder();
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(project);

        jdbc.update("INSERT INTO projects (user_id, name, description) VALUES (:userId, :name, :description)",
                params,
                key);

        return key.getKey().intValue();
    }

    @Override
    public void updateProject(Project project) {
        jdbc.update("UPDATE projects SET user_id =:userId, name =:name, description = :description "
                + "WHERE id =:id AND user_id = :userId",
                new BeanPropertySqlParameterSource(project));

    }

    @Override
    public void removeProject(Project project) {
        jdbc.update("DELETE FROM projects WHERE id = :id AND user_id = :userId", new BeanPropertySqlParameterSource(project));
    }


}
