/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */
package com.olifarm.olive.web.user;

import com.olifarm.olive.user.User;
import com.olifarm.olive.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Implementation of the user data access object for JDBC.
 *
 * @author Harm Brugge
 * @version 0.0.1
 */
@Component
public class UserDaoJdbc implements UserDao {

    /**
     * Template for querying the database
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Override
    public List<User> getUsers() {
        return null;
    }

    @Override
    public User getUser(String username) {
        return jdbc.queryForObject("SELECT * FROM users WHERE username =:username",
                new MapSqlParameterSource("username", username),
                BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public User getUserById(int i) {
        return jdbc.queryForObject("SELECT * FROM users WHERE id =:id",
                new MapSqlParameterSource("id", i),
                BeanPropertyRowMapper.newInstance(User.class));    }

    @Override
    public User getUserByEmail(String email) {
        return jdbc.queryForObject("SELECT * FROM users WHERE email =:email",
                new MapSqlParameterSource("email", email),
                BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public int getId(String username) {
        return jdbc.queryForObject("SELECT id FROM users WHERE username =:username",
                new MapSqlParameterSource("username", username), Integer.class);
    }

    @Override
    public boolean exists(String username) {
        return jdbc.queryForObject(
                "SELECT count(*) FROM users WHERE username=:username",
                new MapSqlParameterSource("username", username), Integer.class) > 0;
    }

    @Override
    public boolean existsEmail(String email) {
        return jdbc.queryForObject(
                "SELECT count(*) FROM users WHERE email=:email",
                new MapSqlParameterSource("email", email), Integer.class) > 0;
    }

    @Override
    public boolean create(User user) {
        return jdbc.update("INSERT INTO users (username, authority, name, surname, email, password, enabled) "
                + "VALUES (:username, :authority, :name, :surname, :email, :password, :enabled)",
                new BeanPropertySqlParameterSource(user)) == 1;
    }

    @Override
    public boolean updateUser(User user) {
        return jdbc.update("UPDATE users SET name =:name, surname =:surname, email =:email WHERE id =:id",
                new BeanPropertySqlParameterSource(user)) == 1;
    }

    @Override
    public boolean changePassword(User user) {
        return jdbc.update("UPDATE users SET password =:password WHERE id =:id",
                new BeanPropertySqlParameterSource(user)) == 1;
    }

    @Override
    public boolean changePeregrineUsername(User user) {
        return jdbc.update("UPDATE users SET peregrine_username =:peregrineUsername WHERE id =:id",
                new BeanPropertySqlParameterSource(user)) == 1;
    }

    @Override
    public boolean setToken(User user) {
        return jdbc.update("UPDATE users SET passwordtoken =:passwordToken WHERE id =:id",
                new BeanPropertySqlParameterSource(user)) == 1;
    }

    @Override
    public boolean setTimestamp(User user) {
        return jdbc.update("UPDATE users SET timestamp =:timestamp WHERE id =:id",
                new BeanPropertySqlParameterSource(user)) == 1;    }

}
