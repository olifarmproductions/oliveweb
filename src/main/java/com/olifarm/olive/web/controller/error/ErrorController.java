/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

package com.olifarm.olive.web.controller.error;

import com.olifarm.olive.web.io.PeregrineSessionAbsentException;
import org.springframework.amqp.AmqpConnectException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.ui.Model;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * The type Error controller.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@ControllerAdvice
public class ErrorController {

    /**
     * Empty result data access exception string.
     *
     * @param model the model
     * @return the string
     */
    @ExceptionHandler(EmptyResultDataAccessException.class)
    public String EmptyResultDataAccessException(Model model) {
        model.addAttribute("message", "Looks like the resource you tried to access does not exist. Sorry :(");
        return "errors/basic_error";
    }

    /**
     * Peregrine session absent exception string.
     *
     * @param model the model
     * @return the string
     */
    @ExceptionHandler(PeregrineSessionAbsentException.class)
    public String PeregrineSessionAbsentException(Model model) {
        model.addAttribute("message", "A valid peregrine session could not be found. Please log in to peregrine.");
        return "errors/basic_error";
    }

    /**
     * Amqp connect exception string.
     *
     * @param model the model
     * @return the string
     */
    @ExceptionHandler(AmqpConnectException.class)
    public String AmqpConnectException(Model model) {
        model.addAttribute("message", "A connection to the amqp broker could not be made. Please contact your system admin");
        return "errors/basic_error";
    }

    /**
     * Missing servlet request parameter exception string.
     *
     * @param model the model
     * @return the string
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public String MissingServletRequestParameterException(Model model) {
        model.addAttribute("message", "Looks like the URL you tried to access has an invalid parameter");
        return "errors/basic_error";
    }
}
