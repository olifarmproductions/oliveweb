/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

package com.olifarm.olive.web.job.project;

import com.olifarm.olive.job.Project;
import com.olifarm.olive.job.ProjectDao;
import com.olifarm.olive.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The type Project service.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Service
public class ProjectService {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private JobGroupService jobGroupService;


    /**
     * Gets all projects for user.
     *
     * @param user the user
     * @return the all projects for user
     */
    public List<Project> getAllProjectsForUser(User user) {
        List<Project> projects = projectDao.getProjects(user);

        for (Project project: projects) {
            project.setJobGroups(jobGroupService.getJobGroupsForProject(user, project));
        }

        return projects;
    }

    /**
     * Gets project.
     *
     * @param user the user
     * @param id   the id
     * @return the project
     */
    public Project getProject(User user, int id) {
        Project project = projectDao.getProject(user, id);
        project.setJobGroups(jobGroupService.getJobGroupsForProject(user, project));

        return project;
    }

    /**
     * Add project.
     *
     * @param user    the user
     * @param project the project
     */
    public void addProject(User user, Project project) {
        project.setUserId(user.getId());
        projectDao.addProject(project);
    }

    /**
     * Update project.
     *
     * @param user    the user
     * @param project the project
     */
    public void updateProject(User user, Project project) {
        project.setUserId(user.getId());
        projectDao.updateProject(project);
    }

    /**
     * Remove project.
     *
     * @param user    the user
     * @param project the project
     */
    public void removeProject(User user, Project project) {
        project.setUserId(user.getId());
        projectDao.removeProject(project);
    }

}
