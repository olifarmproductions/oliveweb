/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.olifarm.olive.job.JobGroup;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.job.project.JobGroupService;
import com.olifarm.olive.web.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * The type Job group controller.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@RestController
public class JobGroupController {

    @Autowired
    private UserService userService;

    @Autowired
    private JobGroupService jobGroupService;

    /**
     * Gets job groups.
     *
     * @param principal the principal
     * @return the job groups
     */
    @RequestMapping("/data/job-groups")
    @PreAuthorize("authenticated")
    public List<JobGroup> getJobGroups(Principal principal) {
        User user = userService.getUser(principal.getName());
        return jobGroupService.getAllJobGroupsForUser(user);
    }

    /**
     * Gets job group.
     *
     * @param jobGroupId the job group id
     * @param principal  the principal
     * @return the job group
     */
    @RequestMapping("/data/job-group")
    @PreAuthorize("authenticated")
    public JobGroup getJobGroup(int jobGroupId, Principal principal) {
        User user = userService.getUser(principal.getName());
        return jobGroupService.getJobGroup(user, jobGroupId);
    }

    /**
     * Add job group boolean.
     *
     * @param jobGroup      the job group
     * @param bindingResult the binding result
     * @param principal     the principal
     * @return the boolean
     */
    @RequestMapping(value = "/data/add-job-group", method = RequestMethod.POST)
    @PreAuthorize("authenticated")
    public boolean addJobGroup(@Valid JobGroup jobGroup, BindingResult bindingResult, Principal principal) {

        if (bindingResult.hasErrors()) {
            return false;
        }
        User user = userService.getUser(principal.getName());
        jobGroupService.addJobGroup(user, jobGroup);
        return true;
    }

    /**
     * Edit job group boolean.
     *
     * @param jobGroup      the job group
     * @param bindingResult the binding result
     * @param principal     the principal
     * @return the boolean
     */
    @RequestMapping(value = "/data/update-job-group", method = RequestMethod.POST)
    @PreAuthorize("authenticated")
    public boolean editJobGroup(@Valid JobGroup jobGroup, BindingResult bindingResult, Principal principal) {

        if (bindingResult.hasErrors()) {
            return false;
        }
        User user = userService.getUser(principal.getName());
        jobGroupService.updateJobGroup(user, jobGroup);
        return true;
    }

    /**
     * Remove job group boolean.
     *
     * @param jobGroup      the job group
     * @param bindingResult the binding result
     * @param principal     the principal
     * @return the boolean
     */
    @RequestMapping(value = "/data/remove-job-group", method = RequestMethod.POST)
    @PreAuthorize("authenticated")
    public boolean removeJobGroup(@Valid JobGroup jobGroup, BindingResult bindingResult, Principal principal) {

        if (bindingResult.hasErrors()) {
            return false;
        }
        User user = userService.getUser(principal.getName());
        jobGroupService.removeJobGroup(user, jobGroup);
        return true;
    }
}
