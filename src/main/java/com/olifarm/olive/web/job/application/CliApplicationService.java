/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.application;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobDao;
import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.CliApplicationDao;
import com.olifarm.olive.job.cliapplication.Module;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Application Service.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
@Service
public class CliApplicationService {

    /**
     * Data access object.
     */
    @Autowired
    private CliApplicationDao cliApplicationDao;

    @Autowired
    private JobDao jobDao;

    /**
     * Get a application (+ input parameters) by id.
     *
     * @param id id in the datasource
     * @return the application + input parameters
     */
    public CliApplication getApp(int id) {

        CliApplication app = cliApplicationDao.getApplication(id);
//        List<Parameter> parameters = cliApplicationDao.getInputFileParameters(app);
//        parameters.addAll(cliApplicationDao.getInputParameters(app));

        if (app.isPeregrineEnabled()) {
            app.setModules(cliApplicationDao.getPeregrineModulesByAppId(id));
        }

        List<Parameter> parameters = new ArrayList<>();
        parameters.addAll(cliApplicationDao.getInputFileParameters(app));
        parameters.addAll(cliApplicationDao.getInputParameters(app));
        parameters.addAll(cliApplicationDao.getOutputParams(app));

        app.setParameters(parameters);

        return app;
    }

    /**
     * Gets app form.
     *
     * @param id the id
     * @return the app form
     */
    public CliApplicationForm getAppForm(int id) {
        CliApplicationForm app = new CliApplicationForm(cliApplicationDao.getApplication(id));

        app.setParameters(cliApplicationDao.getInputParameters(app));
        app.setFileParameters(getInputFileParameters(app));

        return app;
    }

    /**
     * Gets all the apps.
     *
     * @return the apps
     */
    public List<CliApplication> getApps() {
        List<CliApplication> apps = cliApplicationDao.getApplications();

        for (CliApplication app :apps) {

            List<Parameter> params = new ArrayList<>();
            params.addAll(cliApplicationDao.getInputParameters(app));
            params.addAll(cliApplicationDao.getOutputParams(app));
            app.setParameters(params);
        }

        return apps;
    }

    /**
     * Gets all the apps without params
     *
     * @return the apps
     */
    public List<CliApplication> getAppsNoParams() {
        return cliApplicationDao.getApplications();
    }

    /**
     * Sets the app in the datasource.
     *
     * @param app the application
     */
    public void setApp(CliApplication app) {
        if (app.getId() == 0) {
            this.addApp(app);
        } else {
            this.updateApp(app);
        }
    }

    /**
     * Gets peregrine modules.
     *
     * @return the peregrine modules
     */
    public List<Module> getPeregrineModules() {
        return cliApplicationDao.getPeregrineModules();
    }

    /**
     * Sets peregrine module.
     *
     * @param module the module
     */
    public void setPeregrineModule(Module module) {
        cliApplicationDao.setPeregrineModule(module);
    }

    /**
     * Removes peregrine module and its references.
     *
     * @param module the module
     */
    public void removePeregrineModule(Module module) {
        cliApplicationDao.removePeregrineModule(module);
    }

    /**
     * Gets peregrine modulesby id.
     *
     * @param id the id
     * @return the peregrine modulesby id
     */
    public List<Module> getPeregrineModulesbyId(int id) {
        return cliApplicationDao.getPeregrineModulesByAppId(id);
    }

    /**
     * Update an application + parameters
     *
     * @param app the application
     */
    private void updateApp(CliApplication app) {
        cliApplicationDao.updateApp(app);
        this.updateModules(app);

        for (Parameter parameter :app.getParameters()) {
            if (cliApplicationDao.existsParameter(parameter)) {
                if (parameter.isRemove()) {
                    cliApplicationDao.removeParameter(parameter);
                } else {
                    cliApplicationDao.updateParameter(parameter);
                }
            } else {
                cliApplicationDao.addParameter(app, parameter);
            }
        }
    }

    private void updateModules(CliApplication app) {

        List<Module> original = cliApplicationDao.getPeregrineModulesByAppId(app.getId());
        List<Module> selected = app.getModules();

        if (selected == null) {
            selected = new ArrayList<>();
        }
        ArrayList<Module> add = new ArrayList<>(selected);
        add.removeAll(original);
        cliApplicationDao.addModules(app, add);

        ArrayList<Module> remove = new ArrayList<>(original);
        remove.removeAll(selected);
        cliApplicationDao.removeModules(app, remove);

    }

    private List<FileParameter> getInputFileParameters(CliApplication app) {
        List<FileParameter> fileParameters = new ArrayList<>();

        for (Parameter parameter :cliApplicationDao.getInputFileParameters(app)) {
            fileParameters.add(new FileParameter(parameter));
        }

        return fileParameters;
    }

    /**
     * Adds a new application to the datasource
     *
     * @param app the application
     */
    private void addApp(CliApplication app) {
        int appId = cliApplicationDao.setApp(app);
        app.setId(appId);
        cliApplicationDao.setAppParams(app);
        cliApplicationDao.setStdOut(app);
        cliApplicationDao.setStdErr(app);
    }


}
