DROP TABLE IF EXISTS job_parameters;
DROP TABLE IF EXISTS jobs;
DROP TABLE IF EXISTS job_group;
DROP TABLE IF EXISTS projects;
DROP TABLE IF EXISTS parameters;
DROP TABLE IF EXISTS application_modules;
DROP TABLE IF EXISTS applications;
DROP TABLE IF EXISTS peregrine_modules;
DROP TABLE IF EXISTS notifications;
DROP TABLE IF EXISTS users;

CREATE TABLE IF NOT EXISTS users (
  id                 INT AUTO_INCREMENT,
  username           VARCHAR(40) UNIQUE,
  authority          VARCHAR(45),
  name               VARCHAR(45),
  surname            VARCHAR(45),
  email              VARCHAR(45) UNIQUE,
  password           VARCHAR(80),
  enabled            TINYINT,
  peregrine_username VARCHAR(40),
  passwordtoken      VARCHAR(200),
  timestamp          DATETIME,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS applications (
  id                INT     AUTO_INCREMENT,
  name              VARCHAR(40),
  zip_output        BOOLEAN DEFAULT FALSE,
  list_separator    VARCHAR(5),
  custom_view       BOOLEAN DEFAULT FALSE,
  peregrine_enabled BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS peregrine_modules (
  id      INT AUTO_INCREMENT,
  name    VARCHAR(40),
  version VARCHAR(50),
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS application_modules (
  id                  INT AUTO_INCREMENT,
  application_id      INT NOT NULL,
  peregrine_module_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (application_id) REFERENCES applications (id),
  FOREIGN KEY (peregrine_module_id) REFERENCES peregrine_modules (id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS parameters (
  id             INT     AUTO_INCREMENT,
  name           VARCHAR(45)  NOT NULL,
  application_id INT          NOT NULL,
  flag           VARCHAR(200) NULL,
  long_flag      TINYINT,
  type           VARCHAR(15),
  arg_num        INT,
  arg_sep        VARCHAR(5),
  description    VARCHAR(100),
  level          INT,
  extension      VARCHAR(10),
  addition       VARCHAR(50),
  form_group     VARCHAR(200),
  value          TEXT,
  allow_multiple BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (id),
  FOREIGN KEY (application_id) REFERENCES applications (id)
);

CREATE TABLE IF NOT EXISTS projects (
  id          INT AUTO_INCREMENT,
  user_id     INT NOT NULL,
  name        VARCHAR(200),
  description TEXT,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS job_group (
  id          INT AUTO_INCREMENT,
  name        VARCHAR(200),
  parent_id   INT,
  user_id     INT NOT NULL,
  project_id  INT,
  description TEXT,
  PRIMARY KEY (id),
  FOREIGN KEY (parent_id) REFERENCES job_group (id)
    ON DELETE CASCADE,
  FOREIGN KEY (project_id) REFERENCES projects (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS jobs (
  id                   INT AUTO_INCREMENT,
  peregrine_job_id     INT,
  peregrine_job_status VARCHAR(45),
  job_group_id         INT,
  users_job_id         INT DEFAULT 0,
  application_id       INT NOT NULL,
  user_id              INT NOT NULL,
  name                 VARCHAR(100),
  start_time           DATETIME,
  stop_time            DATETIME,
  status               VARCHAR(20),
  input_folder         VARCHAR(200),
  output_folder        VARCHAR(200),
  peregrine_enabled    BOOLEAN,
  PRIMARY KEY (id),
  FOREIGN KEY (job_group_id) REFERENCES job_group (id),
  FOREIGN KEY (application_id) REFERENCES applications (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS job_parameters (
  id           INT AUTO_INCREMENT,
  parameter_id INT NOT NULL,
  job_id       INT NOT NULL,
  value        VARCHAR(100),
  path         VARCHAR(200),
  PRIMARY KEY (id),
  FOREIGN KEY (parameter_id) REFERENCES parameters (id),
  FOREIGN KEY (job_id) REFERENCES jobs (id)
);

CREATE TABLE IF NOT EXISTS notifications (
  id           INT         NOT NULL AUTO_INCREMENT,
  title        VARCHAR(45) NOT NULL,
  type         VARCHAR(45) NOT NULL DEFAULT 'INFO',
  notification TEXT        NOT NULL,
  user_id      INT         NOT NULL,
  date_time    DATETIME    NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
);