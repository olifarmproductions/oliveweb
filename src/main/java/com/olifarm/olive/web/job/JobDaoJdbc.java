/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.JobDao;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.List;

/**
 * Implementation of the Job data access object interface for JDBC.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Component
public class JobDaoJdbc implements JobDao {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Override
    public boolean create(Job job) {

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(job);
        // Needed for ENUM
        params.registerSqlType("status", Types.VARCHAR);

        boolean inserted;

        // Is true if the Job is inserted in db
        if (job.getJobGroupId() == 0) {
            inserted = jdbc.update(
                    "INSERT INTO jobs (application_id, user_id, users_job_id, name, start_time, stop_time, status, job_group_id, peregrine_enabled) "
                            + "VALUES (:applicationId, :userId, :usersJobId, :name, :startTime, :stopTime, :status, NULL, :peregrineEnabled)",
                    params, keyHolder) == 1;
        } else {
            inserted = jdbc.update(
                    "INSERT INTO jobs (application_id, user_id, users_job_id, name, start_time, stop_time, status, job_group_id, peregrine_enabled) "
                            + "VALUES (:applicationId, :userId, :usersJobId, :name, :startTime, :stopTime, :status, :jobGroupId, :peregrineEnabled)",
                    params, keyHolder) == 1;
        }
        // Set the generated id on Job object
        job.setId(keyHolder.getKey().intValue());

        return inserted;
    }


    @Override
    public List<Job> getJobs(User user) {
        return jdbc.query("SELECT * FROM jobs WHERE user_id =:id",
                new MapSqlParameterSource("id", user.getId()),
                new JobRowMapper());
    }

    @Override
    public List<Job> getJobsByGroupId(User user, int i) {
        MapSqlParameterSource params = new MapSqlParameterSource("id", user.getId());
        params.addValue("jobGroupId", i);
        return jdbc.query("SELECT * FROM jobs WHERE user_id =:id and job_group_id = :jobGroupId",
                params,
                new JobRowMapper());
    }

    @Override
    public List<Job> getJobsLimit(User user, int limit) {
        return jdbc.query("SELECT * FROM jobs WHERE user_id =:id ORDER BY start_time DESC  LIMIT :limit",
                new MapSqlParameterSource("id", user.getId()).addValue("limit", limit),
                new JobRowMapper());
    }

    @Override
    public int getMaxUserJobId(User user) {

        Integer x =
                jdbc.queryForObject("SELECT max(users_job_id) FROM jobs WHERE user_id =:id",
                        new MapSqlParameterSource("id", user.getId()), Integer.class);

        if (x == null) {
            return 0;
        }
        return x;
    }

    @Override
    public List<Parameter> getParameters(Job job) {
        return jdbc.query(
                "SELECT * FROM job_parameters jp JOIN parameters p ON jp.parameter_id = p.id WHERE job_id =:id",
                new MapSqlParameterSource("id", job.getId()),
                new ParameterRowMapper());
    }

    @Override
    public Job getJob(int id, User user) {

        if (id <= 0) {
            throw new IllegalArgumentException("Job id cannot be negative or zero");
        }
        if (user == null || user.getId() <= 0) {
            throw new IllegalArgumentException("User is null or the user id is not set");
        }

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("id", id);
        params.addValue("userId", user.getId());

        return jdbc.queryForObject("SELECT * FROM jobs WHERE id =:id AND user_id =:userId",
                params,
                new JobRowMapper());
    }

    @Override
    public boolean updateStatus(Job job) {

        boolean returnStatus = false;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", job.getId());

        if (job.getStatus() != null) {
            params.addValue("status", job.getStatus().toString());
            returnStatus = jdbc.update("UPDATE jobs SET status = :status WHERE id = :id",
                    params) == 1;
        }

        if (job.getPeregrineJobStatus() != null) {
            params.addValue("status", job.getPeregrineJobStatus().toString());
            returnStatus = jdbc.update("UPDATE jobs SET peregrine_job_status = :status WHERE id = :id",
                    params) == 1;
        }

        return returnStatus;
    }

    @Override
    public boolean updateStopTime(Job job) {
        MapSqlParameterSource params = new MapSqlParameterSource("jobId", job.getId());
        params.addValue("id", job.getId());
        params.addValue("stopTime", job.getStopTime());
        return jdbc.update("UPDATE jobs SET stop_time = :stopTime WHERE id = :id", params) == 1;

    }

    @Override
    public boolean addParameter(Job job, Parameter parameter) {
        MapSqlParameterSource params = new MapSqlParameterSource("jobId", job.getId());
        params.addValue("id", parameter.getId());

        if (parameter.getArguments() != null) {
            params.addValue("value", parameter.getArguments().toString());
        } else {
            params.addValue("value", null);
        }

        return jdbc.update("INSERT INTO job_parameters (parameter_id, job_id, value) "
                + "VALUES (:id, :jobId, :value)", params) == 1;
    }

    @Override
    public boolean setPeregrineJobId(Job job) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", job.getId());
        params.addValue("peregrineJobId", job.getPeregrineJobId());

        return jdbc.update("UPDATE jobs SET peregrine_job_id = :peregrineJobId WHERE id = :id", params) == 1;
    }

    @Override
    public void setOutputParams(Job job) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("jobId", job.getId());

        for (Parameter parameter : job.getApp().getParameters()) {
            params.addValue("id", parameter.getId());
            params.addValue("value", parameter.getArguments().toString());
            jdbc.update("INSERT INTO job_parameters (parameter_id, job_id, value) VALUES (:id, :jobId, :value)",
                    params);
        }
    }

    @Override
    public Parameter getParameter(User user, int jobId, int paramId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", user.getId());
        params.addValue("jobId", jobId);
        params.addValue("id", paramId);

        return jdbc.queryForObject("SELECT jp.*, p.* FROM job_parameters jp "
                        + "JOIN parameters p ON jp.parameter_id = p.id "
                        + "JOIN jobs j ON jp.job_id = j.id "
                        + "WHERE p.id =:id AND j.id =:jobId AND j.user_id =:userId",
                params,
                new ParameterRowMapper());
    }

    @Override
    public boolean setJobGroup(Job job) {
        return jdbc.update("UPDATE jobs SET job_group_id = :jobGroupId WHERE id=:id", new BeanPropertySqlParameterSource(job)) == 1;

    }

    @Override
    public boolean removeJob(Job job) {
        return jdbc.update("DELETE FROM jobs WHERE id = :id AND user_id = :userId", new BeanPropertySqlParameterSource(job)) == 1;
    }

}
