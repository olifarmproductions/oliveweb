/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.notifications.notification;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Author(s):     Olivier Bakker
 * Student id(s): 330595
 * Email(s):      o.b.bakker@st.hanze.nl
 * Institute:     Hanze University of Applied Sciences, bioinformatics department
 * Project:       olive
 * Package:       com.olifarm.notifications.notification
 * Created on:    26-12-2015
 * All rights reserved.
 */
public class TimedNotificationRequest {

    private int userId;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date stopDate;
    private int limit;

    /**
     * Instantiates a new Timed notification request.
     */
    public TimedNotificationRequest() {
    }

    /**
     * Gets user id.
     *
     * @return user id
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Gets start date.
     *
     * @return start date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Sets start date.
     *
     * @param startDate the start date
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets stop date.
     *
     * @return stop date
     */
    public Date getStopDate() {
        return stopDate;
    }

    /**
     * Sets stop date.
     *
     * @param stopDate the stop date
     */
    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    /**
     * Gets limit.
     *
     * @return limit limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Sets limit.
     *
     * @param limit the limit
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }
}
