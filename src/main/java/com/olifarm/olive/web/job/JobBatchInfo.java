/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job;

/**
 * Created by obbakker on 2-6-16.
 */
public class JobBatchInfo {

    private int numberOfJobs;
    private int numberOfActiveJobs;
    private int numberOfCompletedJobs;

    /**
     * Instantiates a new Job batch info.
     */
    public JobBatchInfo() {
    }

    /**
     * Instantiates a new Job batch info.
     *
     * @param numberOfJobs          the number of jobs
     * @param numberOfActiveJobs    the number of active jobs
     * @param numberOfCompletedJobs the number of completed jobs
     */
    public JobBatchInfo(int numberOfJobs, int numberOfActiveJobs, int numberOfCompletedJobs) {
        this.numberOfJobs = numberOfJobs;
        this.numberOfActiveJobs = numberOfActiveJobs;
        this.numberOfCompletedJobs = numberOfCompletedJobs;
    }

    /**
     * Gets number of jobs.
     *
     * @return the number of jobs
     */
    public int getNumberOfJobs() {
        return numberOfJobs;
    }

    /**
     * Sets number of jobs.
     *
     * @param numberOfJobs the number of jobs
     */
    public void setNumberOfJobs(int numberOfJobs) {
        this.numberOfJobs = numberOfJobs;
    }

    /**
     * Gets number of active jobs.
     *
     * @return the number of active jobs
     */
    public int getNumberOfActiveJobs() {
        return numberOfActiveJobs;
    }

    /**
     * Sets number of active jobs.
     *
     * @param numberOfActiveJobs the number of active jobs
     */
    public void setNumberOfActiveJobs(int numberOfActiveJobs) {
        this.numberOfActiveJobs = numberOfActiveJobs;
    }

    /**
     * Gets number of completed jobs.
     *
     * @return the number of completed jobs
     */
    public int getNumberOfCompletedJobs() {
        return numberOfCompletedJobs;
    }

    /**
     * Sets number of completed jobs.
     *
     * @param numberOfCompletedJobs the number of completed jobs
     */
    public void setNumberOfCompletedJobs(int numberOfCompletedJobs) {
        this.numberOfCompletedJobs = numberOfCompletedJobs;
    }

    /**
     * Add job.
     */
    public void addJob() {
        numberOfJobs += 1;
    }

    /**
     * Remove job.
     */
    public void removeJob() {
        numberOfJobs -= 1;
    }

    /**
     * Add active job.
     */
    public void addActiveJob() {
        numberOfActiveJobs += 1;
    }

    /**
     * Remove active job.
     */
    public void removeActiveJob() {
        numberOfActiveJobs -= 1;
    }

    /**
     * Add completed job.
     */
    public void addCompletedJob() {
        numberOfCompletedJobs += 1;
    }

    /**
     * Remove completed job.
     */
    public void removeCompletedJob() {
        numberOfCompletedJobs -= 1;
    }

}
