/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.controller;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.olifarm.olive.io.RemoteFileService;
import com.olifarm.olive.job.Job;
import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.job.cliapplication.ParameterType;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.Utils;
import com.olifarm.olive.web.io.PeregrineSession;
import com.olifarm.olive.web.io.PeregrineSessionAbsentException;
import com.olifarm.olive.web.io.PeregrineSftpService;
import com.olifarm.olive.web.io.SshService;
import com.olifarm.olive.web.job.JobService;
import com.olifarm.olive.web.job.application.CliApplicationService;
import com.olifarm.olive.web.job.application.CliApplicationForm;
import com.olifarm.olive.web.job.peregrine.JobFormPeregrine;
import com.olifarm.olive.web.job.project.JobGroupService;
import com.olifarm.olive.web.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;

/**
 * Controller that handles requests relating to jobs.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 */
@Controller
@SessionAttributes("peregrineSession")
public class JobController {

    /**
     * Logger class used for logging.
     */
    private final Log logger = LogFactory.getLog(this.getClass());

    /**
     * {@link JobService} used for creating and interacting with jobs.
     */
    @Autowired
    private JobService jobService;

    /**
     * {@link CliApplicationService} used for interacting with an {@link CliApplication} object.
     */
    @Autowired
    private CliApplicationService applicationService;

    @Autowired
    private JobGroupService jobGroupService;

    /**
     * {@link UserService} used for interaction with {@link com.olifarm.olive.user.User} objects.
     */
    @Autowired
    private UserService userService;

    /**
     * {@link RemoteFileService} used for interacting with the ftp server.
     */
    @Autowired
    private RemoteFileService remoteFileService;

    @Autowired
    private SshService sshService;

    /**
     * Set an empty peregrineSession object on session.
     *
     * @return new peregrineSession object
     */
    @ModelAttribute("peregrineSession")
    public PeregrineSession createSession() {
        return new PeregrineSession();
    }

    /**
     * Resolves the new_job view.
     *
     * @param principal the principal
     * @param model     the model
     * @param job       the job for the view
     * @param id        the id of the application to render the view for
     * @return the view
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/new-job", method = RequestMethod.GET)
    public String getJobForm(Principal principal, Model model, JobFormPeregrine job,
                             @RequestParam(value = "id", defaultValue = "1") int id) {

        User user = userService.getUser(principal.getName());
        CliApplicationForm app = applicationService.getAppForm(id);
        job.setApp(app);
        setPeregrineDefaultSettings(job);

        model.addAttribute("job", job);
        model.addAttribute("jobGroups", jobGroupService.getAllJobGroupsForUserAsList(user));
        model.addAttribute("apps", applicationService.getApps());

        return "authenticated/new_job";
    }

    /**
     * Cancels a (peregrine) job.
     *
     * @param id        the id
     * @param principal the principal
     * @param session   the session
     * @param response  the response
     * @return status boolean
     * @throws JSchException the j sch exception
     * @throws IOException   the io exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/cancel-job", method = RequestMethod.GET)
    @ResponseBody
    public boolean cancelJob(@RequestParam(value = "id") int id,
                             Principal principal,
                             HttpSession session,
                             HttpServletResponse response) throws JSchException, IOException {

        User user = userService.getUser(principal.getName());
        Job job = jobService.getJob(id, user);
        PeregrineSession peregrineSession = (PeregrineSession) session.getAttribute("peregrineSession");

        if (!Utils.jobHasCompleted(job) && job.isPeregrineEnabled()) {
            try {
                sshService.cancelJob(SshService.makeSshSession(peregrineSession), job.getPeregrineJobId());
                return true;
            } catch (PeregrineSessionAbsentException | IllegalStateException e) {
                response.sendError(HttpStatus.UNAUTHORIZED.value(), "please log in to peregrine");
                return false;
            }
        } else {
            response.sendError(HttpStatus.BAD_REQUEST.value(), "Canceling not supported for basic nodes");
        }
        return false;
    }

    /**
     * Creates a new job and redirect to the job status page.
     *
     * @param job              the job
     * @param peregrineSession the peregrine session
     * @param principal        springs principal object used for storing user data
     * @param model            the model
     * @return the view
     * @throws IOException                     the io exception
     * @throws JSchException                   the j sch exception
     * @throws SftpException                   the sftp exception
     * @throws PeregrineSessionAbsentException the peregrine session absent exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/new-job", method = RequestMethod.POST)
    public String doJob(@ModelAttribute JobFormPeregrine job,
                        @ModelAttribute("peregrineSession") PeregrineSession peregrineSession,
                        Principal principal,
                        Model model) throws IOException, JSchException, SftpException, PeregrineSessionAbsentException {

        User user = userService.getUser(principal.getName());
        job.setUser(user);

        logger.info("Processing job:  id: " + job.getId() + ", name: " + job.getName());

        if (jobService.createNewJob(job, peregrineSession)) {
            logger.info("Processed job:  id: " + job.getId() + ", name: " + job.getName());
        } else {
            logger.warn("Error processing job:  id: " + job.getId() + ", name: " + job.getName());
            model.addAttribute("message", "Error starting job. Please contact system admin");
            return "errors/basic_error";
        }

        return "redirect:/job?id=" + job.getId();
    }

    /**
     * Resolves the rerun job view.
     *
     * @param model     the model
     * @param jobForm   the job form
     * @param principal springs principal object used for storing user data
     * @param id        the jobs id
     * @return the view
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/rerun-job", method = RequestMethod.GET)
    public String rerunJob(Model model, JobFormPeregrine jobForm, Principal principal,
                           @RequestParam(value = "id") int id) {

        User user = userService.getUser(principal.getName());

        Job job = jobService.getJobWithEmptyParams(id, user);
        BeanUtils.copyProperties(job, jobForm);

        setPeregrineDefaultSettings(jobForm);
        model.addAttribute("job", jobForm);
        model.addAttribute("jobGroups", jobGroupService.getAllJobGroupsForUserAsList(user));
        model.addAttribute("apps", applicationService.getApps());

        return "authenticated/new_job";
    }

    /**
     * Creates a job object with an existing file and returns the new_job view with that file.
     *
     * @param model     the model
     * @param principal springs principal object used for storing user data
     * @param paramId   the parameters id
     * @param appId     the apps id
     * @param jobId     the jobs id
     * @return the view
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/job-file", method = RequestMethod.GET)
    public String jobFile(Model model, Principal principal,
                          @RequestParam(value = "param") int paramId,
                          @RequestParam(value = "app") int appId,
                          @RequestParam(value = "job") int jobId) {

        User user = userService.getUser(principal.getName());

        Job job = jobService.createJobWithFile(user, appId, jobId, paramId);

        model.addAttribute("job", job);
        model.addAttribute("apps", applicationService.getApps());

        return "authenticated/new_job";
    }

    /**
     * Resolves the job status view.
     *
     * @param id        the jobs id
     * @param principal springs principal object used for storing user data
     * @param model     the model
     * @return the view
     * @throws IOException   the io exception
     * @throws JSchException the j sch exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "/job", method = RequestMethod.GET)
    public String getJob(@RequestParam(value = "id") int id, Principal principal, Model model) throws IOException, JSchException {

        User user = userService.getUser(principal.getName());

        Job job = jobService.getJob(id, user);
        model.addAttribute("job", job);

        logger.info("Got new request for job id: " + id);
        model.addAttribute("id", id);
        model.addAttribute("apps", applicationService.getApps());

        if (job.getApp().isCustomView()) {
            return "authenticated/fragments/app_views/" + job.getApp().getName().toLowerCase();
        } else {
            return "authenticated/project/view_job";
        }
    }

    /**
     * API used for downloading a file.
     *
     * @param principal        springs principal object used for storing user data
     * @param jobId            the jobs id
     * @param paramId          the parameters id
     * @param peregrineSession the peregrine session
     * @param response         response for writing the file output to
     * @throws IOException   the io exception
     * @throws JSchException the j sch exception
     */
    @PreAuthorize("authenticated")
    @RequestMapping("/download")
    @ResponseStatus(value = HttpStatus.OK)
    public void downloadJobFiles(Principal principal,
                                 @RequestParam(value = "job-id") int jobId,
                                 @RequestParam(value = "param-id") int paramId,
                                 @ModelAttribute("peregrineSession") PeregrineSession peregrineSession,
                                 HttpServletResponse response) throws IOException, JSchException {

        User user = userService.getUser(principal.getName());

        Job job = jobService.getJob(jobId, user);

        for (Parameter parameter : job.getApp().getParameters()) {
            if (parameter.getId() == paramId) {

                String path = parameter.getArguments().get(0);
                String fileName = parameter.getFileNameFromPath();

                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", fileName);
                response.setHeader(headerKey, headerValue);
                // set content attributes for the response
                response.setContentType("application/octet-stream");

                try {
                    // Execute if the job was run using peregrine
                    if (job.isPeregrineEnabled()) {
                        // Zip the output folder if the requested file type is a zip archive
                        if (parameter.getType() == ParameterType.OUTPUT_ZIP_FILE) {
                            this.makeZipFilePeregrine(job, peregrineSession);
                        }
                        getFilesPeregrine(path, response, peregrineSession);
                    } else {
                        getFilesBasic(path, response);
                    }
                    break;
                } catch (PeregrineSessionAbsentException | IllegalStateException e) {
                    response.sendError(HttpStatus.UNAUTHORIZED.value(), "please log in to peregrine");
                } catch (IOException e) {
                    response.sendError(HttpStatus.NOT_FOUND.value(), "The requested file could not be found");
                } catch (JSchException e) {
                    response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something went wrong with the connection");
                }
            }
        }
    }

    private void getFilesBasic(String path, HttpServletResponse response) throws IOException {

        remoteFileService.open();
        remoteFileService.read(path, response.getOutputStream());
        remoteFileService.close();

    }

    private void getFilesPeregrine(String path, HttpServletResponse response, PeregrineSession peregrineSession) throws IOException, PeregrineSessionAbsentException, JSchException {

        PeregrineSftpService ftpservice = new PeregrineSftpService();
        ftpservice.open(SshService.makeSshSession(peregrineSession));
        ftpservice.read(path, response.getOutputStream());
        ftpservice.close();

    }

    private void makeZipFilePeregrine(Job job, PeregrineSession peregrineSession) throws PeregrineSessionAbsentException, IOException, JSchException {
        sshService.zipOutputFolder(job, SshService.makeSshSession(peregrineSession));
    }

    /**
     * Set the default configuration for a peregrine job.
     * @param job the job to set the config on
     */
    private void setPeregrineDefaultSettings(JobFormPeregrine job) {
        job.setWallClockTime("1");
        job.setNode(1);
        job.setNumberOfTasks(1);
        job.setNumberOfTasksPerNode(1);
        job.setCpusPerTask(1);
    }
}
