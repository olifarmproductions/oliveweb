package com.olifarm.olive.web.controller;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.io.PeregrineSession;
import com.olifarm.olive.web.io.SshService;
import com.olifarm.olive.web.user.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * The type Peregrine authenticator.
 */
@RestController
@SessionAttributes("peregrineSession")
public class PeregrineAuthenticator {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private SshService sshService;

    @Autowired
    private UserService userService;

    /**
     * Set an empty peregrineSession object on session.
     *
     * @return new peregrineSession object
     */
    @ModelAttribute("peregrineSession")
    public PeregrineSession createSession()  {
        return new PeregrineSession();
    }

    /**
     * API for checking if there is an active ssh peregrine session on the servers session.
     *
     * @param peregrineSession peregrineSession object stored on session
     * @return HttpStatus.ACCEPTED on success, .UNAUTHORIZED on fail
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "check-session", method = RequestMethod.GET)
    public ResponseEntity<String> checkAuthentication(@ModelAttribute("peregrineSession") PeregrineSession peregrineSession) {

        Session session = peregrineSession.getSession();

        if (session == null) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if (!session.isConnected()) {
            try {
                session.connect();
            } catch (JSchException e) {
                logger.warn("Could not get a connection on session, error: " + e.getMessage());
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        }

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    /**
     * API for creating a SSH session to the peregrine server
     *
     * @param peregrineSession PeregrineSession object on session
     * @param principal        User data on session
     * @return HttpStatus.ACCEPTED on success, .UNAUTHORIZED on fail
     * @throws JSchException if creating session object fails.
     */
    @PreAuthorize("authenticated")
    @RequestMapping(value = "do-auth", method = RequestMethod.POST)
    public ResponseEntity<String> doAuthentication(@ModelAttribute("peregrineSession") PeregrineSession peregrineSession,
                                                   Principal principal) throws JSchException {

        User user = userService.getUser(principal.getName());
        user.setPeregrinePassword(peregrineSession.getPassword());

        Session session = sshService.getSession(user);
        peregrineSession.setPassword(null); // remove password after connecting

        // Test the session
        try {
            session.connect();
        } catch (JSchException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        // On success store session
        peregrineSession.setSession(session);

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

//    @ExceptionHandler(IllegalArgumentException.class)
//    public Object handleNullSession() {
//        return null;
//    }

}
