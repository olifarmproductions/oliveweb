/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */
package com.olifarm.olive.web.mail;

import com.olifarm.olive.job.Job;
import com.olifarm.olive.user.User;
import com.olifarm.olive.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * Class for sending email.
 *
 * @author Harm Brugge
 * @author Olivier Bakker
 * @version 0.0.1
 */
@Service
public class MailService {

    /**
     * Mail sender object.
     */
    @Autowired
    private MailSender mailSender;

    /**
     * Handle for message source.
     */
    @Autowired
    private MessageSource messageSource;

    /**
     * User DAO to get user information.
     */
    @Autowired
    private UserDao userDao;

    /**
     * Configured mail address
     */
    @Value("${mail.address}")
    private String MAIL_ADDRESS;

    /**
     * Sends a mail when a job is finished.
     *
     * @param job the finished job
     * @return true if succeeded
     */
    public boolean jobFinished(Job job) {
        User user = userDao.getUser(job.getUsername());

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom(MAIL_ADDRESS);
        mail.setTo(user.getEmail());
        mail.setSubject(messageSource.getMessage("MailService.jobFinished.title", null, null));
        mail.setText(messageSource.getMessage("MailService.jobFinished.content", new Object[]{ job.getName() }, null));
        mailSender.send(mail);

        return true;
    }

    /**
     * Report issue boolean.
     *
     * @param username the username
     * @param type     the type
     * @param title    the title
     * @param content  the content
     * @return the boolean
     */
    public boolean reportIssue(String username, String type, String title, String content) {


        User user = userDao.getUser(username);

        String mailContent = content + "\n\n Email: " + user.getEmail();

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom(MAIL_ADDRESS);
        mail.setTo(MAIL_ADDRESS);
        mail.setSubject(messageSource.getMessage("MailService.reportIssue.title", new Object[]{type, title}, null));
        mail.setText(messageSource.getMessage("MailService.reportIssue.content", new Object[]{username, type, mailContent }, null));
        mailSender.send(mail);


        return true;
    }

    /**
     * Send password reset link boolean.
     *
     * @param user the user
     * @param link the link
     * @return the boolean
     */
    public boolean sendPasswordResetLink(User user, String link) {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom(MAIL_ADDRESS);
        mail.setTo(user.getEmail());
        mail.setSubject(messageSource.getMessage("MailService.resetPassword.title", new Object[]{}, null));
        mail.setText(messageSource.getMessage("MailService.resetPassword.content", new Object[]{link}, null));
        mailSender.send(mail);
        return true;
    }

}