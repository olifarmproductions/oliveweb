/*
 *
 * OliveWeb is the webinterface for the OliveProject.
 * Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Olivier Bakker: o.b.bakker@st.hanze.nl
 *  Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 *
 */

/**
 * Created by Olivier on 27-5-2016.
 */
var appModule = (function () {
    
    var module = {};

    var size = $("meta[name='params_size']").attr("content");
    var paramId = 0;
    if (size != null) {
        paramId = size
    }

    
    module.addParameter = function () {
        var tmplHtml = $("#parameter-form-template").html();
        var tmpl = Handlebars.compile(tmplHtml);
        var model = {"paramId": paramId};
        $("#param-settings").append(tmpl(model));
        paramId++;
    };

    module.collapseAllParameter = function () {
        $(".param-hidden").addClass("hidden");
    };

    module.toggleShowParameter = function (id) {
        if ($("#param-hidden-" + id).hasClass("hidden")) {
            $("#param-hidden-" + id).removeClass("hidden");
        } else {
            $("#param-hidden-" + id).addClass("hidden");
        }
    };

    module.removeParameter = function (id) {
        $("#param-" + id).remove();
    };

    module.showOrHideModule = function () {
        if ($("#is-peregrine-enabled").is(":checked")) {
            $("#module-settings").removeClass("hidden");
        } else {
            $("#module-settings").addClass("hidden");
        }
    };

    return module;
})();