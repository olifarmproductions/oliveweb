-- noinspection SqlNoDataSourceInspectionForFile
-- noinspection SqlDialectInspectionForFile
INSERT INTO users (username, authority, name, surname, email, password, enabled, peregrine_username) VALUES
  ('harmbrugge', 'ROLE_ADMIN', 'Harm', 'Brugge', 'harmbrugge@gmail.com',
   '$2a$10$8BuqmvWDYoyuhgss2qq/gOQ2dqjwMUi8fiB6nhrN463aNEXkxXF2u', 1, 'f111537');

INSERT INTO users (username, authority, name, surname, email, password, enabled, peregrine_username) VALUES
  ('finch', 'ROLE_ADMIN', 'Harold', 'Finch', 'o.b.bakker@st.hanze.nl',
   '$2a$10$lyrHAl2AOKugpvkyMvgKBu.Kpp47.koyOtWOTdVRc3N1NvH.ngJJy', 1, 'f111537');

INSERT INTO applications (id, name, zip_output, list_separator, custom_view, peregrine_enabled)
  VALUES (1, 'Martinate', 1, ' ', 1, true);

INSERT INTO parameters (name, application_id, flag, long_flag, type, arg_num, description, level, extension) VALUES
  ('Input PDB file', 1, 'f', 0, 'INPUT_FILE', 1, 'The input PDB file', 1, 'pdb'),
  ('Input topology file', 1, 'top', 0, 'INPUT_FILE', 1, 'The input toplogy file', 1, 'top'),
  ('Coarse grained force field', 1, 'cp', 0, 'TEXT', 1, 'Coarse grained force field', 1, ''),
  ('Index file', 1, 'mdx', 0, 'TEXT', 1, 'Index file', 1, ''),
  ('Multiscaling', 1, 'm', 0, 'TEXT', 1, 'Chains of multiscaling. Other chains will be coarse grained.', 2, ''),
  ('Multiscale all chains', 1, 'M', 0, 'BOOLEAN', 1, 'Multiscale all chains, except those specified with -m', 2, ''),
  ('Atomistic force fields', 1, 'ff', 0, 'TEXT', 1, 'Atomistic force field for multiscaling', 2, ''),
  ('Solvent', 1, 'sol', 0, 'TEXT', 1, 'Solvent to use', 1, ''),
  ('Dielectric constant', 1, 'epsr', 0, 'TEXT', 1, 'Dielectric constant of the medium', 2, ''),
  ('Temperature', 1, 'T', 0, 'TEXT', 1, 'Temperature in kelvin', 2, ''),
  ('Pressure', 1, 'P', 0, 'TEXT', 1, 'Pressure in bar', 2, ''),
  ('Salt', 1, 'salt', 0, 'TEXT', 1, 'NaCl concentration in M', 2, ''),
  ('Simulation time', 1, 'time', 0, 'TEXT', 1, 'Simulation length in ns', 2, ''),
  ('Resolution', 1, 'at', 0, 'TEXT', 1, 'Output resolution in ns', 2, ''),
  ('Number of cores', 1, 'np', 0, 'TEXT', 1, 'Number of cores to run with', 3, ''),
  ('Virstual sites', 1, 'vsite', 0, 'BOOLEAN', 1, 'Use virtual sites', 3, ''),
  ('Step', 1, 'step', 0, 'TEXT', 1, 'Step where to start or pauze execution.', 3, ''),
  ('Stop', 1, 'stop', 0, 'BOOLEAN', 1, 'Step at which to stop execution', 3, ''),
  ('Overwrite files', 1, 'force', 0, 'BOOLEAN', 1, 'Force overwriting existing files', 2, ''),
  ('No exec', 1, 'noexec', 0, 'BOOLEAN', 1, 'Don''t actually simulate', 3, ''),
  ('No cleanup', 1, 'keep', 0, 'BOOLEAN', 1, 'No cleanup, keep all the rubbish', 3, ''),
  ('Daft', 1, 'daft', 0, 'INPUT_FILE', 1, 'Index file with daft energy groups', 2, 'idx'),
  ('Dry', 1, 'dry', 0, 'TEXT', 1, 'Use dry martini', 3, '');

INSERT INTO parameters (name, application_id, flag, long_flag, type, arg_num, arg_sep, description, level, extension, allow_multiple)
VALUES
  ('insane-l', 1, 'insane-l', 1, 'TEXT', 1, '=', 'insane l', 3, '', true),
  ('insane-d', 1, 'insane-d', 1, 'TEXT', 1, '=', 'insane d', 3, '', true),
  ('insane-dm', 1, 'insane-dm', 1, 'TEXT', 1, '=', 'insane dm', 3, '', false),
  ('insane-dz', 1, 'insane-dz', 1,'TEXT',1, '=', 'insane-dz', 3, '', false),
  ('insane-sol', 1, 'insane-sol', 1,'TEXT',1, '=', 'insane-sol', 3, '', false),
  ('insane-fudge', 1, 'insane-fudge', 1,'TEXT',1, '=', 'insane-fudge', 3, '', false),
  ('mdrun-rdd', 1, 'mdrun-rdd', 1, 'TEXT', 1, '=', 'Md run rdd', 3, '', false),
  ('martinize-cys', 1, 'maritze-cys', 1,'TEXT',1, '=', 'Maritze-cys', 3, '', false);

INSERT INTO parameters (application_id, long_flag, flag, type, arg_num, description, level, name, extension) VALUES
  (1, FALSE, '', 'STD_OUT', 1, 'The standard out output', 0, 'stdout', 'stdout.log'),
  (1, FALSE, '', 'STD_ERR', 1, 'The standard error output', 0, 'stderr', 'stderr.log'),
  (1, FALSE, '', 'OUTPUT_ZIP_FILE', 1, 'Zipped output', 0, 'zip', 'out.zip'),
  (1, FALSE, 'dir', 'OUTPUT_DIR', 1, 'Output dir', 0, 'output dir', '');

INSERT INTO parameters (name, application_id, flag, long_flag, type, arg_num, arg_sep, description, level, extension, addition, form_group, allow_multiple)
VALUES
  ('cg.gro', 1, '', 0, 'OUTPUT_FILE', 0, '', '?', 0, 'gro', '-cg', '', 0),
  ('cg.ndx', 1, '', 0, 'OUTPUT_FILE', 0, '', '?', 0, 'ndx', '-cg', '', 0),
  ('end-state', 1, '', 0, 'OUTPUT_FILE', 0, '', 'End-state of simulation', 0, 'pdb', '-centered_filtered', '', 0),
  ('cg.top', 1, '', 0, 'OUTPUT_FILE', 0, '', '?', 0, 'top', '-cg', '', 0);

INSERT INTO applications (id, name, list_separator, zip_output, peregrine_enabled) VALUES (2, 'Insane', ' ', FALSE, true);

INSERT INTO parameters (application_id, long_flag, flag, type, arg_num, description, level, name, form_group) VALUES
  (2, FALSE, 'pbc', 'TEXT', 1, 'PBC type: hexagonal, rectangular, square, cubic, optimal or keep', 2, 'PBC type',
   'Periodic boundary conditions'),
  (2, FALSE, 'd', 'TEXT', 1, 'Distance between periodic images (nm)', 1, 'Distance', 'Periodic boundary conditions'),
  (2, FALSE, 'dz', 'TEXT', 1, 'Z distance between periodic images (nm)', 2, 'Z distance', 'Periodic boundary conditions'),
  (2, FALSE, 'x', 'TEXT', 1, 'X dimension or first lattice vector of system (nm)', 2, 'X dim', 'Periodic boundary conditions'),
  (2, FALSE, 'y', 'TEXT', 1, 'Y dimension or first lattice vector of system (nm)', 2, 'Y dim', 'Periodic boundary conditions'),
  (2, FALSE, 'z', 'TEXT', 1, 'Z dimension or first lattice vector of system (nm)', 2, 'Z dim', 'Periodic boundary conditions'),
  (2, FALSE, 'box', 'TEXT', 9, 'Box in GRO (3 or 9 floats) or PDB (6 floats) format, comma separated', 2, 'Box', 'Periodic boundary conditions'),
  (2, FALSE, 'l', 'TEXT', 1, 'Lipid type and relative abundance (NAME[:#])', 1, 'Lipid (L)', 'Membrane/lipid'),
  (2, FALSE, 'u', 'TEXT', 1, 'Lipid type and relative abundance (NAME[:#])', 1, 'Lipid (u)', 'Membrane/lipid'),
  (2, FALSE, 'a', 'TEXT', 1, 'Area per lipid (nm*nm)', 2, 'Area', 'Membrane/lipid'),
  (2, FALSE, 'au', 'TEXT', 1, 'Area per lipid (nm*nm) for upper layer', 2, 'Area (top layer)', 'Membrane/lipid'),
  (2, FALSE, 'asym', 'TEXT', 1, 'Membrane asymmetry (number of lipids)', 3, 'Membrane asymmetry', 'Membrane/lipid'),
  (2, FALSE, 'hole', 'TEXT', 1, 'Make a hole in the membrane with specified radius', 2, 'Hole', 'Membrane/lipid'),
  (2, FALSE, 'disc', 'TEXT', 1, 'Make a membrane disc with specified radius', 2, 'Disc', 'Membrane/lipid'),
  (2, FALSE, 'rand', 'TEXT', 1, 'Random kick size (maximum atom displacement)', 3, 'Random kick', 'Membrane/lipid'),
  (2, FALSE, 'bd', 'TEXT', 1, 'Bead distance unit for scaling z-coordinates (nm)', 3, 'Bead distance', 'Membrane/lipid'),
  (2, FALSE, 'center', 'BOOLEAN', 1, 'Center the protein on z', 1, 'Center', 'Protein'),
  (2, FALSE, 'orient', 'BOOLEAN', 1, 'Orient protein in membrane', 1, 'Orient', 'Protein'),
  (2, FALSE, 'rotate', 'TEXT', 1, 'Rotate protein (random|princ|angle(float))', 2, 'Rotate', 'Protein'),
  (2, FALSE, 'od', 'TEXT', 1, 'Grid spacing for determining orientation', 3, 'Grid spacing', 'Protein'),
  (2, FALSE, 'op', 'TEXT', 1, 'Hydrophobic ratio power for determining orientation', 3, 'Hydrophobic ratio power', 'Protein'),
  (2, FALSE, 'fudge', 'TEXT', 1, 'Fudge factor for allowing lipid-protein overlap', 3, 'Fudge', 'Protein'),
  (2, FALSE, 'ring', 'TEXT', 1, 'Put lipids inside the protein', 2, 'Ring', 'Protein'),
  (2, FALSE, 'dm', 'TEXT', 1, 'Shift protein with respect to membrane', 1, 'Shift protein', 'Protein'),
  (2, FALSE, 'sol', 'TEXT', 1, 'Solvent type and relative abundance (NAME[:#])', 1, 'Solvent', 'Solvent'),
  (2, FALSE, 'sold', 'TEXT', 1, 'Solvent diameter', 1, 'Solvent diameter', 'Solvent'),
  (2, FALSE, 'solr', 'TEXT', 1, 'Solvent random kick', 3, 'Solvent random kick', 'Solvent'),
  (2, FALSE, 'excl', 'TEXT', 1, 'Exclusion range (nm) for solvent addition relative to membrane center', 3,
   'Exclusion range', 'Solvent'),
  (2, FALSE, 'salt', 'TEXT', 1, 'Salt concentration', 1, 'Salt', 'Salt'),
  (2, FALSE, 'charge', 'TEXT', 1, 'Charge of system. Set to auto to infer from residue names', 2, 'Charge', 'Salt');

INSERT INTO parameters (application_id, long_flag, flag, type, arg_num, description, level, name, form_group, allow_multiple)
VALUES
  (2, FALSE, 'alhead', 'TEXT', 2, 'Additional lipid head specification string', 1, 'Alhead', 'Aditonal lipid types',
   TRUE),
  (2, FALSE, 'allink', 'TEXT', 2, 'Additional lipid linker specification string', 1, 'Allink', 'Aditonal lipid types',
   TRUE),
  (2, FALSE, 'altail', 'TEXT', 2, 'Additional lipid tail specification string', 1, 'Altail', 'Aditonal lipid types',
   TRUE),
  (2, FALSE, 'alname', 'TEXT', 1, 'Additional lipid name, x4 letter', 1, 'Alname', 'Aditonal lipid types', TRUE),
  (2, FALSE, 'alcharge', 'TEXT', 1, 'Additional lipid charge', 1, 'Alcharge', 'Aditonal lipid types', TRUE);


INSERT INTO parameters (application_id, long_flag, flag, type, arg_num, description, level, name, extension, form_group)
VALUES
  (2, FALSE, 'o', 'OUTPUT_FILE', 1, 'Output GRO file: Membrane with Protein', 0, 'Output file', 'pdb', 'Files'),
  (2, FALSE, 'f', 'INPUT_FILE', 1, 'Input GRO or PDB file 1: Protein', 1, 'Input GRO or PDB file', 'pdb', 'Files'),
  (2, FALSE, 'p', 'INPUT_FILE', 1, 'Optional rudimentary topology file', 2, 'rudimentary topology', 'top', 'Files'),
  (2, FALSE, '', 'STD_OUT', 1, 'The standard out output', 0, 'stdout', 'stdout.log', 'Files'),
  (2, FALSE, '', 'STD_ERR', 1, 'The standard error output', 0, 'stderr', 'stderr.log', 'Files');

INSERT INTO applications (id, name, zip_output, list_separator, custom_view, peregrine_enabled)
VALUES (3, 'Rscript', 0, '', 0, true);

INSERT INTO parameters (name, application_id, flag, long_flag, type, arg_num, arg_sep, description, level, extension, form_group, allow_multiple)
VALUES
   ('R script', 3, '', 0, 'INPUT_FILE', 1, '', 'R script', 1, 'R', '', 0),
   ('stdout', 3, '', 0, 'STD_OUT', 1, NULL, 'The standard output', 0, 'stdout.log', NULL, 0),
   ('stderr', 3, '', 0, 'STD_ERR', 1, NULL, 'The standard error output', 0, 'stderr.log', NULL, 0),
   ('test', 3, '', 0, 'OUTPUT_FILE', 1, NULL, 'The standard error output', 0, 'txt', NULL, 0);

INSERT INTO peregrine_modules (id, name, version) VALUES (4, 'R', '3.2.1-goolfc-2.7.11-default-X11');
INSERT INTO peregrine_modules (id, name, version) VALUES (5, 'gromacs 5 avx2 mt', 'GROMACS/5.1.2-intel-2016a-avx2-mt');
INSERT INTO peregrine_modules (id, name, version) VALUES (6, 'gromacs 5 hybrid', 'GROMACS/5.1.2-intel-2016a-hybrid');
INSERT INTO peregrine_modules (id, name, version) VALUES (7, 'gromacs 5 mt', 'GROMACS/5.1.2-intel-2016a-mt');
INSERT INTO peregrine_modules (id, name, version) VALUES (8, 'python 3', 'Python/3.5.1-intel-2016a');
INSERT INTO peregrine_modules (id, name, version) VALUES (9, 'python 2', 'Python/Python/2.7.11-intel-2016a');

INSERT INTO application_modules (application_id, peregrine_module_id) VALUES (1, 5);
INSERT INTO application_modules (application_id, peregrine_module_id) VALUES (1, 9);
INSERT INTO application_modules (application_id, peregrine_module_id) VALUES (2, 9);
INSERT INTO application_modules (application_id, peregrine_module_id) VALUES (3, 4);

-- # INSERT INTO projects (id, user_id, name) VALUES (1, 2, 'Test project 1');
-- # INSERT INTO projects (id, user_id, name) VALUES (2, 2, 'Test project 2');
-- # INSERT INTO projects (id, user_id, name) VALUES (3, 2, 'Test project 3');
-- #
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (1, 'Group yo', NULL, 2, 1);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (2, 'Group 2', 1, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (3, 'Group 3', 2, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (4, 'Group 4', 3, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (5, 'Group 5', 3, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (6, 'Group 6', 3, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (7, 'Group 7', 3, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (8, 'Group 8', 3, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (9, 'Group 9', 4, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (10, 'Group 10', 4, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (11, 'Group 11', 4, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (12, 'Group 12', 4, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (13, 'Group 13', 3, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (14, 'Group 14', 2, 2, NULL);
-- #
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (15, 'Test yo 2', NULL, 2, 2);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (16, 'Test 16', 15, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (17, 'Test 17', 15, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (18, 'Test 18', 15, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (19, 'Test 19', 18, 2, NULL);
-- # INSERT INTO job_group (id, name, parent_id, user_id, project_id) VALUES (20, 'Test 20', 18, 2, NULL);

-- # INSERT INTO jobs (id, users_job_id, application_id, user_id, name, start_time, stop_time, status, input_folder, output_folder)
-- # VALUES (1, 1, 1, 2, '', '2016-03-22 11:03:33', NULL, 'PROCESSING', NULL, NULL);
-- # INSERT INTO jobs (id, users_job_id, application_id, user_id, name, start_time, stop_time, status, input_folder, output_folder)
-- # VALUES (2, 2, 2, 2, '', '2016-03-22 11:03:57', NULL, 'PROCESSING', NULL, NULL);
-- #
-- # INSERT INTO job_parameters (id, parameter_id, job_id, value, path)
-- # VALUES (1, 27, 1, '[/homes/obbakker/OLIVE_FTP/olive/ftp/finch/1/output/martinate_job-1.stdout.log]', NULL);
-- # INSERT INTO job_parameters (id, parameter_id, job_id, value, path)
-- # VALUES (2, 28, 1, '[/homes/obbakker/OLIVE_FTP/olive/ftp/finch/1/output/martinate_job-1.stderr.log]', NULL);
-- # INSERT INTO job_parameters (id, parameter_id, job_id, value, path)
-- -- # VALUES (3, 29, 1, '[/homes/obbakker/OLIVE_FTP/olive/ftp/finch/1/output/martinate_job-1.out.zip]', NULL);
-- # INSERT INTO job_parameters (id, parameter_id, job_id, value, path) VALUES (4, 30, 1, '[]', NULL);
-- # INSERT INTO job_parameters (id, parameter_id, job_id, value, path)
-- # VALUES (5, 67, 2, '[/homes/obbakker/OLIVE_FTP/olive/ftp/finch/2/input/insane_job-2.rtf]', NULL);
-- # INSERT INTO job_parameters (id, parameter_id, job_id, value, path)
-- # VALUES (6, 66, 2, '[/homes/obbakker/OLIVE_FTP/olive/ftp/finch/2/output/insane_job-2.pdb]', NULL);
-- # INSERT INTO job_parameters (id, parameter_id, job_id, value, path)
-- # VALUES (7, 69, 2, '[/homes/obbakker/OLIVE_FTP/olive/ftp/finch/2/output/insane_job-2.stdout.log]', NULL);
-- # INSERT INTO job_parameters (id, parameter_id, job_id, value, path) VALUES (8, 70, 2, '[/homes/obbakker/OLIVE_FTP/olive/ftp/finch/2/output/insane_job-2.stderr.log]', null);