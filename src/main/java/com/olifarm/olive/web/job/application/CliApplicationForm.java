/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.application;

import com.olifarm.olive.job.cliapplication.CliApplication;
import com.olifarm.olive.job.cliapplication.Parameter;
import com.olifarm.olive.job.cliapplication.ParameterType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbrugge on 24-2-16.
 */
public class CliApplicationForm extends CliApplication {

    private List<FileParameter> fileParameters;

    /**
     * Instantiates a new Cli application form.
     */
    public CliApplicationForm() {
        this.fileParameters = new ArrayList<>();
    }

    /**
     * Instantiates a new Cli application form.
     *
     * @param app the app
     */
    public CliApplicationForm(CliApplication app) {
        this.setZipOutput(app.isZipOutput());
        this.setId(app.getId());
        this.setName(app.getName());
        this.setParameters(app.getParameters());
        this.setListSeparator(app.getListSeparator());
        this.setCustomView(app.isCustomView());
        this.setPeregrineEnabled(app.isPeregrineEnabled());
        this.setModules(app.getModules());

        this.fileParameters = new ArrayList<>();
    }

    /**
     * Move input file parameters.
     */
    public void moveInputFileParameters() {
        List<Parameter> normalParams = new ArrayList<>();

        for (Parameter parameter :this.getParameters()) {
            if (parameter.getType() == ParameterType.INPUT_FILE) {
                fileParameters.add(new FileParameter(parameter));
            } else {
                normalParams.add(parameter);
            }
        }

        this.setParameters(normalParams);
    }

    /**
     * Empty file parameters.
     */
    public void emptyFileParameters() {
        for (FileParameter parameter :this.getFileParameters()) {
            parameter.emptyArguments();
        }
    }

    /**
     * Gets file parameters.
     *
     * @return the file parameters
     */
    public List<FileParameter> getFileParameters() {
        return fileParameters;
    }

    /**
     * Sets file parameters.
     *
     * @param fileParameters the file parameters
     */
    public void setFileParameters(List<FileParameter> fileParameters) {
        this.fileParameters = fileParameters;
    }
}
