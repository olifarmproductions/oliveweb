/*
 *
 *     OliveWeb is the webinterface for the OliveProject.
 *     Copyright (C) 2016  Olivier Bakker & Harm Brugge
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Olivier Bakker: o.b.bakker@st.hanze.nl
 *     Harm Brugge: h.brugge@st.hanze.nl
 *
 *
 */

package com.olifarm.olive.web.job.project;

import com.olifarm.olive.job.JobGroup;
import com.olifarm.olive.user.User;
import com.olifarm.olive.web.TestBase;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 * Created by obbakker on 26-4-16.
 */
public class JobGroupDaoTests extends TestBase {


    @Autowired
    private JobGroupDaoJdbc jobGroupDaoJdbc;

    private JobGroup validJobGroupNoProject;

    private User user;

    /**
     * Sets .
     */
    @Before
    public void setup() {

        validJobGroupNoProject = new JobGroup();
        validJobGroupNoProject.setId(1);
        validJobGroupNoProject.setName("Test yo");
        validJobGroupNoProject.setUserId(2);

        JobGroup one = new JobGroup();
        one.setName("Test 1");
        one.setUserId(2);

        JobGroup two = new JobGroup();
        two.setName("Test 2");
        two.setUserId(2);

        JobGroup three = new JobGroup();
        three.setName("Test 3");
        three.setUserId(2);

        JobGroup four = new JobGroup();
        four.setName("Test 4");
        four.setUserId(2);

        JobGroup five = new JobGroup();
        five.setName("Test 5");
        five.setUserId(2);

        three.addChild(four);
        three.addChild(five);
        one.addChild(three);

        validJobGroupNoProject.addChild(one);
        validJobGroupNoProject.addChild(two);

        user = new User();
        user.setId(2);
    }

    /**
     * Job group tests.
     */
    @Test
    public void jobGroupTests() {
        int id = addJobGroupTestSunny();
        getJobGroupTestSunny(id);
        removeJobTest(id);
    }


    /**
     * Add job group test sunny int.
     *
     * @return the int
     */
    public int addJobGroupTestSunny() {
        return jobGroupDaoJdbc.addJobGroup(validJobGroupNoProject);
    }

    /**
     * Gets job group test sunny.
     *
     * @param id the id
     */
    public void getJobGroupTestSunny(int id) {
        jobGroupDaoJdbc.getJobGroup(user, id);
    }

    /**
     * Remove job test.
     *
     * @param id the id
     */
    public void removeJobTest(int id) {
        JobGroup jobGroup = jobGroupDaoJdbc.getJobGroup(user, id);
        jobGroupDaoJdbc.removeJobGroup(jobGroup);
    }



}
